package com.darwingtechnologies.app.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.darwingtechnologies.app.R;
import com.darwingtechnologies.app.business.models.userLogin.UserLoginResponse;
import com.darwingtechnologies.app.fragments.BrokerProfileFragment;
import com.darwingtechnologies.app.fragments.BrokersFragment;
import com.darwingtechnologies.app.fragments.HomeScreenFragment;
import com.darwingtechnologies.app.fragments.ProfileFragment;
import com.darwingtechnologies.app.fragments.ProfileFragmentNew;
import com.darwingtechnologies.app.fragments.SplashFragment;
import com.darwingtechnologies.app.utilities.CommonMethods;
import com.darwingtechnologies.app.utilities.CommonObjects;
import com.darwingtechnologies.app.utilities.GPSTracker;
import com.darwingtechnologies.app.utilities.LocaleHelper;
import com.darwingtechnologies.app.utilities.PreferencesUtils;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity  {
    public static LatLng latLng;

    private static MainActivity instance;
    public SpinKitView spin_kit;
    private LinearLayout llTopBar;
    private BottomNavigationView bnvBottomNavigation;
    private DrawerLayout drawer_layout;
    private NavigationView nvLeft;
    public static FragmentActivity currentFragment;
    private ImageButton ibMenu;
    private Switch sbChangeLanguage;
    private TextView tvLogout;

    public static UserLoginResponse userLoginResponse=null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main_drawer);
        spin_kit = findViewById(R.id.spin_kit);

        instance = this;

        sbChangeLanguage = findViewById(R.id.sbChangeLanguage);
        if(LocaleHelper.isDefaultLanguage(MainActivity.instance())){
            sbChangeLanguage.setChecked(false);
            addSwitchListener();
            LocaleHelper.setLocale(MainActivity.this, "en");

        }else{
            sbChangeLanguage.setChecked(true);
            addSwitchListener();
            LocaleHelper.setLocale(MainActivity.this, "ar");
        }


        CommonObjects.setContext(this);
        goForNextScreen();
        initTopBar();
        inItBottomNavigation();
        topBar();

    }
    private void initTopBar() {
        llTopBar = findViewById(R.id.llTopBar);

    }
    private void topBar(){
        ibMenu = findViewById(R.id.ibMenu);
        tvLogout = findViewById(R.id.tvLogout);
        nvLeft = findViewById(R.id.nvLeft);

        drawer_layout = findViewById(R.id.drawer_layout);
        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

        ibMenu.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        drawer_layout.openDrawer(nvLeft);
                    }
                });





//        llProfile.setOnClickListener(
//                new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        CommonMethods.callFragment(ProfileFragment.newInstance(), R.id.flFragmentOverlay, R.anim.fade_in, R.anim.fade_out, CommonObjects.getContext(), true);
//                        drawer.closeDrawer(nvLeft);
//                    }
//                });
        tvLogout.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PreferencesUtils.getInstance(MainActivity.this).clearPreferences();
                        CommonMethods.clearPreferences((FragmentActivity)CommonObjects.getContext(),getString(R.string.app_name));
                        startActivity(new Intent(CommonObjects.getContext(), MainActivity.class));
                        ((FragmentActivity) CommonObjects.getContext()).finishAffinity();
                        drawer_layout.closeDrawer(nvLeft);
                    }
                });
    }

    void addSwitchListener(){
        sbChangeLanguage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if(b){
                    LocaleHelper.setLocale(MainActivity.this, "ar");
                    CommonMethods.showToast("Language Updated to Arabic", Toast.LENGTH_LONG);
                }else{
                    LocaleHelper.setLocale(MainActivity.this, "en");
                    CommonMethods.showToast("Language Updated to English", Toast.LENGTH_LONG);
                }


                drawer_layout.closeDrawer(nvLeft);

                    startActivity(new Intent(CommonObjects.getContext(), MainActivity.class));
                    ((FragmentActivity) CommonObjects.getContext()).finishAffinity();


            }
        });
    }
    private void inItBottomNavigation() {
        bnvBottomNavigation = findViewById(R.id.bnvBottomNavigation);

        bnvBottomNavigation.getMenu().findItem(R.id.navigation_home).setChecked(true);
        bnvBottomNavigation.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.navigation_home:
                                if (bnvBottomNavigation.getSelectedItemId() != R.id.navigation_home) {
                                    CommonMethods.callFragment(HomeScreenFragment.newInstance(), R.id.flFragmentContainer, R.anim.fade_in, R.anim.fade_out, CommonObjects.getContext(), false);

                                }
                                return true;
                            case R.id.navigation_broker:
                                if (bnvBottomNavigation.getSelectedItemId() != R.id.navigation_broker) {
                                    CommonMethods.callFragment(BrokersFragment.newInstance(null,true), R.id.flFragmentContainer, R.anim.fade_in, R.anim.fade_out, CommonObjects.getContext(), false);

                                }
                                return true;
                            case R.id.navigation_profile:
                                if (bnvBottomNavigation.getSelectedItemId() != R.id.navigation_profile) {
                                    if(userLoginResponse!=null && userLoginResponse.getData()!=null){
                                        if(userLoginResponse.getData().getBrokerInfo()==null || userLoginResponse.getData().getBrokerInfo().size()<=0){
                                            CommonMethods.callFragment(ProfileFragment.newInstance(), R.id.flFragmentContainer, R.anim.fade_in, R.anim.fade_out, CommonObjects.getContext(), false);
                                        }else{
                                            CommonMethods.callFragment(ProfileFragmentNew.newInstance(userLoginResponse.getData().getBrokerInfo().get(0)), R.id.flFragmentContainer, R.anim.fade_in, R.anim.fade_out, CommonObjects.getContext(), false);
                                        }

                                    }else{
                                        CommonMethods.showMessage(MainActivity.instance(),"Please login to continue.");
                                    }
                                }
                                return true;
//                                if (getIsGuest()) {
//                                    CommonMethods.signupToContinueDialog(MainActivity.this);
//                                    return false;
//                                } else {
//                                    return true;
//                                }
                        }
                        return false;
                    }
                });
    }
    private void goForNextScreen(){
        CommonMethods.callFragment(SplashFragment.newInstance(), R.id.flFragmentContainer, R.anim.fade_in, R.anim.fade_out,this, false);
    }
    public static final boolean isInstanciated() {
        return instance != null;
    }

    public static final MainActivity instance() {
        if (instance != null) return instance;
        throw new RuntimeException("MainActivity not instantiated yet");
    }
    public boolean popBackStackMytm() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStackImmediate();
//            currentFragment=getCurrentFragment();
            return true;
        }
        return false;
    }
    public void isShowSpinKit(Boolean isShow) {
        if (isShow) {
            spin_kit.setVisibility(View.VISIBLE);
        } else {
            spin_kit.setVisibility(View.GONE);
        }
    }
    public void checkAndRequestPermissionsToSendImage() {
        ArrayList<String> permissionsList = new ArrayList<>();

        int readExternalStorage =
                getPackageManager()
                        .checkPermission(
                                Manifest.permission.READ_EXTERNAL_STORAGE, getPackageName());
//        org.linphone.mediastream.Log.i(
//                "[Permission] Read external storage permission is "
//                        + (readExternalStorage == PackageManager.PERMISSION_GRANTED
//                                ? "granted"
//                                : "denied"));
        int camera =
                getPackageManager().checkPermission(Manifest.permission.CAMERA, getPackageName());
//        org.linphone.mediastream.Log.i(
//                "[Permission] Camera permission is "
//                        + (camera == PackageManager.PERMISSION_GRANTED ? "granted" : "denied"));

        int writeExternalStorage =
                getPackageManager()
                        .checkPermission(
                                Manifest.permission.WRITE_EXTERNAL_STORAGE, getPackageName());
//        org.linphone.mediastream.Log.i(
//                "[Permission] Camera permission is "
//                        + (camera == PackageManager.PERMISSION_GRANTED ? "granted" : "denied"));

        if (readExternalStorage != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.shouldShowRequestPermissionRationale(
                    this, Manifest.permission.READ_EXTERNAL_STORAGE);
//            org.linphone.mediastream.Log.i("[Permission] Asking for read external storage");
            permissionsList.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (camera != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA);
//            org.linphone.mediastream.Log.i("[Permission] Asking for camera");
            permissionsList.add(Manifest.permission.CAMERA);
        }
        if (writeExternalStorage != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.shouldShowRequestPermissionRationale(
                    this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
//            org.linphone.mediastream.Log.i("[Permission] Asking for write external storage");
            permissionsList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionsList.size() > 0) {
            String[] permissions = new String[permissionsList.size()];
            permissions = permissionsList.toArray(permissions);
            ActivityCompat.requestPermissions(this, permissions, 0);
        }
    }

    public void hideTopBar() {
        llTopBar.setVisibility(View.GONE);
    }

    public void showTopBar() {
        llTopBar.setVisibility(View.VISIBLE);
    }

    public void hideBottomBar() {
        bnvBottomNavigation.setVisibility(View.GONE);
    }

    public void showBottomBar() {
        bnvBottomNavigation.setVisibility(View.VISIBLE);
    }

    public boolean checkForGPSOn() {

        if(!GPSTracker.isGpsEnabled(this)){

            CommonMethods.displayMessagesDialog( "Turn on GPS first ",this).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    GPSTracker.askForOnGps(MainActivity.this);
                }
            }).setCancelable(false).create().show();
            // Toast.makeText(mActivity, "Turn on GPS first", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            GPSTracker.getInstance(MainActivity.this);
            return true;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        getLoginDetails();
    }

    public void getLoginDetails(){
        String response= CommonMethods.getStringPreference(MainActivity.instance(), getString(R.string.app_name), UserLoginResponse.class.getSimpleName(),null);

        if(response!=null && !response.isEmpty()){
            userLoginResponse=new Gson().fromJson(response,UserLoginResponse.class);
        }
    }


}
