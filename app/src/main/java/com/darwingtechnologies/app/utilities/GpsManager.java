package com.darwingtechnologies.app.utilities;

import android.content.Context;

/**
 * Created by Sabir on 1/6/2018.
 */

public class GpsManager {
   private static  GPSTracker gpsTracker;

    public static double getLongitude(Context context) {
        if(gpsTracker==null)
         gpsTracker = GPSTracker.getInstance(context);
       return gpsTracker.getLongitude();
    }

    public static double getLatitude(Context context) {

        if(gpsTracker==null){

            gpsTracker=  GPSTracker.getInstance(context);
        }


        return gpsTracker.getLatitude();

    }

}
