package com.darwingtechnologies.app.utilities;

import android.Manifest;
import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.Spanned;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.darwingtechnologies.app.R;
import com.google.gson.Gson;


import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.TELEPHONY_SERVICE;

public class Utils {
    private static ProgressDialog pgDialog;
    public static final Integer MINIMUM_RADIUS_IN_METER = 16;

    public static void showToast(Context context, String msg) {
        if (msg == null) return;
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void showSnackBarSocket(FragmentActivity mActivity, View coordinatorLayout) {
        try {
//            Snackbar snackbar = Snackbar
//                    .make(coordinatorLayout, mActivity.getString(R.string.txt_poor_connection), Snackbar.LENGTH_INDEFINITE)
//                    .setAction("Okay", new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                        }
//                    });
//
//            snackbar.setActionTextColor(Color.RED);
//            View sbView = snackbar.getView();
//            TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
//            textView.setTextColor(getColor(mActivity,R.color.colorPrimary));
//            snackbar.show();
        } catch (Exception e) {
            Utils.showLoger(e.getMessage());
        }

    }

    public static void showLoger(String msg) {
        //Log.d("NARSUN_KEY_1", msg);
        Log.e("Log:",msg);
    }

    public static void showLoger_api(String msg) {
        Log.d("API_KEY_1", msg);
    }

    public static void showLogerException(String msg) {
        Log.d("NARSUN_KEY_2", msg);
    }

    public static void showLogerProduction(String msg) {
        Log.d("NARSUN_KEY", msg);
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = null;
        if (connectivityManager != null) {
            activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        }
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

//    public static void loadImage(final Context mActivity, String url, ImageView myImageView) {
//        myImageView.setVisibility(View.VISIBLE);
//        Glide.with(mActivity)
//                .load(BuildConfig.BASE_URL + url)
//                .centerCrop()
//                .placeholder(R.mipmap.bg_splash_new)
//                .into(myImageView);
//    }
//
//    public static void loadBigImage(final FragmentActivity mActivity, String url, ImageView myImageView) {
//        if (mActivity.isDestroyed()) return;
//        myImageView.setVisibility(View.VISIBLE);
//        Glide.with(mActivity)
//                .load(BuildConfig.BASE_URL + url)
//                .centerCrop()
//                .placeholder(R.drawable.yems_big)
//                .into(myImageView);
//    }

//    public static void loadWinsImage(final FragmentActivity mActivity, String url, ImageView myImageView) {
//        if (mActivity.isDestroyed()) return;
//        myImageView.setVisibility(View.VISIBLE);
//        Glide.with(mActivity)
//                .load(BuildConfig.BASE_URL + url)
//                .centerCrop()
//                .placeholder(R.drawable.giftbox)
//                .into(myImageView);
//    }

    public static int pxToDp(Context context, int px) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }

    public static void hideKeyboard(FragmentActivity activity) {
        try {
            InputMethodManager imm =
                    (InputMethodManager)
                            activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            View view = activity.getCurrentFocus();
            if (imm != null && view != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static Boolean isKeyboardShowing(FragmentActivity mActivity) {
        InputMethodManager imm =
                (InputMethodManager)
                        mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        //            writeToLog("Software Keyboard was shown");
        //            writeToLog("Software Keyboard was not shown");
        return imm.isAcceptingText();
    }

    public static void showFragmentDialog(FragmentActivity mActivity, DialogFragment newFragment, String key) {
        FragmentTransaction ft = mActivity.getSupportFragmentManager().beginTransaction();
        newFragment.show(ft, "dialog");
    }

    public static void loadFragment(FragmentActivity mActivity, Fragment newInstance) {
        Utils.switchToFragment(mActivity, newInstance, true);
    }

    public static void switchToFragment(FragmentActivity mActivity, Fragment toFragment, boolean b) {
        showLoger("Previous stack Count " + mActivity.getSupportFragmentManager().getBackStackEntryCount());
        FragmentTransaction ft = mActivity.getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);

        if (!isFragmentOnTop(toFragment, mActivity)) {
            ft.addToBackStack(toFragment.getClass().getName()).replace(R.id.container, toFragment).commit();
        }
    }

    public static boolean isFragmentOnTop(Fragment toFragment, FragmentActivity fragmentActivity) {
        int count = fragmentActivity.getSupportFragmentManager().getBackStackEntryCount();
        if (count > 0) {
            FragmentManager.BackStackEntry entry =
                    fragmentActivity.getSupportFragmentManager().getBackStackEntryAt(count - 1);

            return entry != null && toFragment.getClass().getName().equals(entry.getName());
        }
        return false;
    }

    public static void clearFragmentStack(FragmentActivity mActivity, boolean isAll) {
        FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
        showLoger(" stack Count " + fragmentManager.getBackStackEntryCount());
        if (isAll) {
            for (int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
                fragmentManager.popBackStackImmediate();//.popBackStack();
            }
        } else {
            fragmentManager.popBackStack();
        }
    }

    public static final int getColor(FragmentActivity context, int id) {
        final int version = Build.VERSION.SDK_INT;
        if (version >= 23) {
            return ContextCompat.getColor(context, id);
        } else {
            return context.getResources().getColor(id);
        }
    }

//    public static boolean isUrduLanguage(FragmentActivity mActivity) {
//        return PreferencesUtils.getInstance(mActivity).getString(Constants.UserLanguage, "").equals(Constants.LanguageUrdu);
//    }

    public static void putStringPreference(FragmentActivity mActivity, String dataKey, String data) {
        PreferencesUtils.getInstance(mActivity).putString(dataKey, data);
    }

    public static String getStringPreference(FragmentActivity mActivity, String dataKey, String defaultValue) {
        return PreferencesUtils.getInstance(mActivity).getString(dataKey, defaultValue);
    }
    public static void setTermsAndCondition(FragmentActivity mActivity, String termsText) {
        PreferencesUtils.getInstance(mActivity).putString("terms", termsText);
    }

    public static String getTermsAndCondition(FragmentActivity mActivity) {
        return PreferencesUtils.getInstance(mActivity).getString("terms", null);
    }

    public static void setPrivacyPolicy(FragmentActivity mActivity, String privacyText) {
        PreferencesUtils.getInstance(mActivity).putString("privacy", privacyText);
    }

    public static String getPrivacyPolicy(FragmentActivity mActivity) {
        return PreferencesUtils.getInstance(mActivity).getString("privacy", null);
    }

    public static void setAboutApp(FragmentActivity mActivity, String aboutText) {
        PreferencesUtils.getInstance(mActivity).putString("aboutApp", aboutText);
    }

    public static String getAboutApp(FragmentActivity mActivity) {
        return PreferencesUtils.getInstance(mActivity).getString("aboutApp", null);
    }
    public static void setHelp(FragmentActivity mActivity, String aboutText) {
        PreferencesUtils.getInstance(mActivity).putString("help", aboutText);
    }

    public static String getHelp(FragmentActivity mActivity) {
        return PreferencesUtils.getInstance(mActivity).getString("help", null);
    }


//    public static void setHashSignatureForOTP(Context mActivity,String signature) {
//        PreferencesUtils.getInstance(mActivity).putString(CqOTPSignature, signature);
//    }

//    public static String getHashSignatureForOTP(FragmentActivity mActivity) {
//        String signature =PreferencesUtils.getInstance(mActivity).getString(CqOTPSignature, null);
//        if(signature==null){
//            signature="1GxsCk1bgkX";
//        }
//
//        return signature;
//    }



//    public static void goToPrivacyPolicyOrForOtherTextActivity(FragmentActivity mActivity, String title, String bodyText, String imageUrl){
//        Intent intent = new Intent(mActivity, PrivacyPolicyActivity.class);
//        intent.putExtra("title",title);
//        intent.putExtra("bodyText",bodyText);
//        if(imageUrl!=null && !imageUrl.isEmpty() && !imageUrl.equalsIgnoreCase("false") && imageUrl.startsWith("http")){
//                intent.putExtra("imageUrl",imageUrl);
//        }
//        mActivity.startActivity(intent);
//    }
    public static void setCheckInCount(FragmentActivity mActivity) {
        PreferencesUtils.getInstance(mActivity).putInt("CheckInCount", getCheckInCount(mActivity)+1);
    }

    public static int getCheckInCount(FragmentActivity mActivity) {
        return PreferencesUtils.getInstance(mActivity).getInt("CheckInCount", 0);
    }

//    public static void showRatingDialog(final FragmentActivity mActivity) {
//        new Handler().post(new Runnable() {
//            @Override
//            public void run() {
////                    .setTriggerCount(10)
////                        .setRepeatCount(10)
//           final AppRatingDialog appRatingDialog = new AppRatingDialog.Builder(mActivity)
//                .setLayoutBackgroundColor(R.color.colorWhite)
//                .setIconDrawable(true, ContextCompat.getDrawable(mActivity, R.drawable.rate_app))
//                .setTitleText("Enjoy " + mActivity.getString(R.string.app_name) + " ?")
//                .setTitleTextColor(R.color.black)
//                .setMessageText("Tap a Rate App to rate it on the Play Store.")
//                .setMessageTextColor(R.color.black)
//                .setRateButtonText("Rate App", new OnRatingDialogListener() {
//                    @Override
//                    public void onClick() {
//                        goToPlayStoreForReview(mActivity);
//                    }
//                }).setRateLaterButtonText("Not Now", new OnRatingDialogListener() {
//                       @Override
//                       public void onClick() {
//
//                       }
//                   })
//                .setRateButtonBackground(R.color.colorSkyBlueMate)
//                .build();
//
//        appRatingDialog.show();
//            }
//        });
//    }


//    public static void goToPlayStoreForReview(FragmentActivity fragmentActivity){
//        Intent browserIntent =
//                new Intent(
//                        Intent.ACTION_VIEW,
//                        Uri.parse("https://play.google.com/store/apps/details?id="+APPLICATION_ID+"&hl=en"));
//        browserIntent.addCategory(Intent.CATEGORY_BROWSABLE);
//        fragmentActivity.startActivity(browserIntent);
//    }

//    public static void saveUserData(FragmentActivity mActivity, UserData data) {
//        if (data != null) {
//            PreferencesUtils.getInstance(mActivity).putString(Constants.UserIdKey, ""+data.getId());
//            PreferencesUtils.getInstance(mActivity).putString(Constants.UserName, data.getName());
//            PreferencesUtils.getInstance(mActivity).putString(Constants.UserPhoneNo, data.getPhone());
//            PreferencesUtils.getInstance(mActivity).putString(Constants.UserEmail, data.getEmail());
//            PreferencesUtils.getInstance(mActivity).putString(Constants.UserAvatar, data.getAvatar(false));
////            PreferencesUtils.getInstance(mActivity).putString(Constants.UserDob, data.getDateOfBirth());
////            PreferencesUtils.getInstance(mActivity).putString(Constants.UserGender, data.getGender());
//        }
//    }

//    public static UserData getUserData(FragmentActivity mActivity) {
//        UserData userData=new UserData();
//
//        try {
//            userData.setId(Integer.parseInt(PreferencesUtils.getInstance(mActivity).getString(Constants.UserIdKey, "")));
//            userData.setName(PreferencesUtils.getInstance(mActivity).getString(Constants.UserName, ""));
//            userData.setPhone(PreferencesUtils.getInstance(mActivity).getString(Constants.UserPhoneNo, ""));
//            userData.setEmail(PreferencesUtils.getInstance(mActivity).getString(Constants.UserEmail, ""));
//            userData.setAvatar(PreferencesUtils.getInstance(mActivity).getString(Constants.UserAvatar, ""));
////            userData.setDateOfBirth(PreferencesUtils.getInstance(mActivity).getString(Constants.UserDob, ""));
////            userData.setGender(PreferencesUtils.getInstance(mActivity).getString(Constants.UserGender, ""));
//
//            return userData;
//        }catch (Exception e){
//            e.printStackTrace();
//            return null;
//        }
//    }


    public static String SERVER_TIME_STAMP_FORMAT ="yyyy-mm-dd HH:mm:ss";// "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static String SERVER_TIME_STAMP_DATE_ONLY_FORMAT ="yyyy-MM-dd";// "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    public static long getLocalMilisecond() {
        Date today = new Date();
        String dateToStr = getGMTDatTime(today);
        Date date = parserTimeStamp(dateToStr, "yyyy-MM-dd'T'HH:mm:ss");
        return date.getTime();
    }

    public static long getBroadCastToMilisecond(Date today) {
        String dateToStr = getGMTDatTime(today);
        Date date = parserTimeStamp(dateToStr, "yyyy-MM-dd'T'HH:mm:ss");
        return date.getTime();
    }

    public static long getSerTimeMilisecond(String server_time) {
        // 2019-04-18T07:47:51.049+02:00
        Date date = parserTimeStamp(server_time, SERVER_TIME_STAMP_FORMAT);
        //String dateToStr = getGMTDatTime(date);
        //Date dateGmt = parserTimeStamp(dateToStr, "yyyy-MM-dd'T'HH:mm:ss");
        return date.getTime();
    }

    public static Date parserTimeStamp(String timeStampStr, String pattern) {
        Utils.showLoger("parserTimeStamp " + timeStampStr + " pattern " + pattern);
        SimpleDateFormat sourceFormat = new SimpleDateFormat(pattern);
        Date parsedDate = new Date();
        try {
            if (timeStampStr == null) return parsedDate;
            parsedDate = sourceFormat.parse(timeStampStr);
            Utils.showLoger("parserTimeStamp " + parsedDate);
        } catch (ParseException e) {
            e.printStackTrace();
            showLoger(" ParseException " + e.getMessage());
        }
        return parsedDate;

    }

    public static Date parserTimeStamp(String timeStampStr) {
        Utils.showLoger("parserTimeStamp2 " + timeStampStr);
        SimpleDateFormat sourceFormat = new SimpleDateFormat(SERVER_TIME_STAMP_FORMAT);
        TimeZone utcTime = TimeZone.getTimeZone("UTC");
        sourceFormat.setTimeZone(utcTime);
        Date parsedDate = new Date();
        try {
            if (timeStampStr == null) return parsedDate;
            parsedDate = sourceFormat.parse(timeStampStr);
            Utils.showLoger("parserTimeStamp2 " + parsedDate);
        } catch (ParseException e) {
            e.printStackTrace();
            showLoger(" ParseException2 " + e.getMessage());
        }
          Utils.showLoger("parserTimeStamp parsedDate " + parsedDate);
        return parsedDate;

    }
    public static Date parserTimeStampDateOnly(String timeStampStr) {
        Utils.showLoger("parserTimeStamp2 " + timeStampStr);
        SimpleDateFormat sourceFormat = new SimpleDateFormat(SERVER_TIME_STAMP_DATE_ONLY_FORMAT);
        TimeZone utcTime = TimeZone.getTimeZone("UTC");
        sourceFormat.setTimeZone(utcTime);
        Date parsedDate = new Date();
        try {
            if (timeStampStr == null) return parsedDate;
            parsedDate = sourceFormat.parse(timeStampStr);
            Utils.showLoger("parserTimeStamp2 " + parsedDate);
        } catch (ParseException e) {
            e.printStackTrace();
            showLoger(" ParseException2 " + e.getMessage());
        }
        Utils.showLoger("parserTimeStamp parsedDate " + parsedDate);
        return parsedDate;

    }

    public static Date getDateOnlyInDefaultFormat(String timeStamp){
        Date date=new Date();
        try {
            SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
            date = DATE_FORMAT.parse(timeStamp);
        }catch (Exception e){
            e.printStackTrace();
        }

        return date;
    }

    public static String getTimeFromDate(Date date){
        if(date==null){
            return "--";
        }
        SimpleDateFormat localDateFormat = new SimpleDateFormat("hh:mm:ss a");
        String time = localDateFormat.format(date);

        return time;

    }

    public static String getGMTDatTime(Date date) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        df.setTimeZone(tz);
        return df.format(date);
    }

    public static String getLocatDateFromSerevrDateTime(String server_time) {
        String notificationDate = "";
        try {

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = df.parse(server_time);


            DateFormat df2 = new SimpleDateFormat("dd MMM yyyy hh:mm a");
            df2.setTimeZone(TimeZone.getDefault());
            notificationDate = df2.format(date);


        } catch (Exception e) {
            showLoger("getLocatDateFromSerevrDateTime " + e.getMessage());
        }
        return notificationDate;
    }

    public static String getDateFormatted(String dateInput) {
        if (dateInput != null) {
            SimpleDateFormat sdf1 = new SimpleDateFormat("MM-dd-yyyy");
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = sdf1.parse(dateInput);
                return sdf2.format(date.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return dateInput;
    }


    public static String toJson(Object jsonObject) {
        return new Gson().toJson(jsonObject);
    }

    public static Object fromJson(String jsonString, Type type) {
        return new Gson().fromJson(jsonString, type);
    }

//    public static void showPopup(FragmentActivity mActivity, String title, String msg) {
//        if (mActivity.isDestroyed() || mActivity.isFinishing()) return;
//        AppDialogs cdd = new AppDialogs(mActivity, title, msg, false);
//        if (cdd.getWindow() != null) {
//            cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        }
//        cdd.show();
//    }

//    public static void getPopup(FragmentActivity mActivity, String title, String msg, boolean goToHome) {
//        if (mActivity.isDestroyed() || mActivity.isFinishing()) ;
//        AppDialogs cdd = new AppDialogs(mActivity, title, msg, goToHome);
//        if (cdd.getWindow() != null) {
//            cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        }
//        cdd.show();
//    }

//    public static void showProgressDialog(Context nContext) {
//
//        try {
//            if (pgDialog != null) {
//                pgDialog.hide();
//            }
//            pgDialog = new ProgressDialog(nContext);
//            if (pgDialog.getWindow() != null)
//                pgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            pgDialog.setIndeterminate(true);
//            pgDialog.setCancelable(false);
//            //            if (MainActivity.isInstanciated()) {
//            //                MainActivity.instance().isShowSpinKit(true);
//            //            } else {
//            pgDialog.show();
//            pgDialog.setContentView(R.layout.app_progress_bar);
//            //            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    public static void hideProgressDialog() {
        try {
            if (pgDialog != null) {
                pgDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static android.app.AlertDialog.Builder displayMessagesDialog(
            final String message, final Context context) {

        final android.app.AlertDialog.Builder builder =
                new android.app.AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        builder.create().hide();
                    }
                });

        // builder.stickers_list_menu().show();

        return builder;
    }

    public static void composeEmail(Context context, String email, String subject, Spanned body, String filePath) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        intent.putExtra(Intent.EXTRA_TEXT, body);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

    public static boolean isEmailValid(String email) {
        try {
            Pattern pattern = Pattern.compile(".+@.+\\.[a-z]+");
            Matcher matcher = pattern.matcher(email);
            return matcher.matches();
        } catch (Exception e) {
            return false;
        }
    }
//    public static String getStaticMapAsImageFromLatLng(LatLng latLng, Context context) {
//
//        //    String
//        // urlTest="https://maps.googleapis.com/maps/api/staticmap?center=Brooklyn+Bridge,New+York,NY&zoom=13&size=600x300&maptype=roadmap" +
//        //
//        // "&markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green%7Clabel:G%7C40.711614,-74.012318" +
//        //            "&markers=color:red%7Clabel:C%7C40.718217,-73.998284" +
//        //            "&key="+API_Key;
//
//        String url =
//                "https://maps.googleapis.com/maps/api/staticmap?markers="
//                        + latLng.latitude
//                        + ","
//                        + latLng.longitude
//                        + "&zoom=17&size=1200x600&key="
//                        + context.getString(R.string.api_key_place_picker);
//        //    android.util.Log.e("MapURL",url);
//
//        return url;
//    }

//    public static void getDirectionUsingLatLng(FragmentActivity mActivity, LatLng latLng){
//        String uri =
//                "https://www.google.com/maps/search/?api=1&query="
//                        + latLng.latitude
//                        + ","
//                        + latLng.longitude;
//        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
//        mActivity.startActivity(intent);
//    }


//    public static String getFcmToken(final FragmentActivity mActivity) {
//        FirebaseInstanceId.getInstance().getInstanceId()
//                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
//                        if (!task.isSuccessful()) {
//                            Utils.showLoger("getInstanceId failed " + task.getException());
//                        } else if (task.getResult() != null) {
//                            // Get new Instance ID token
//                            String token = task.getResult().getToken();
//                            PreferencesUtils.getInstance(mActivity).putString(Constants.UserFCMToken, token);
//                            subscribeToTopics(mActivity);
//                        }
//                    }
//                });
//
//        return PreferencesUtils.getInstance(mActivity).getString(Constants.UserFCMToken,"");
//    }

//    public static String getAuthToken(final FragmentActivity mActivity){
//        String authToken=PreferencesUtils.getInstance(mActivity).getString(Constants.UserAuthToken, "");
//        Log.e("authToken",""+authToken);
//        return authToken;
//    }

//    public static void subscribeToTopics(final FragmentActivity mActivity) {
//        FirebaseMessaging.getInstance().subscribeToTopic("androidUpdates")
//                .addOnCompleteListener(new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//                        if (task.isSuccessful()) {
//                            Utils.showLoger("subscribeToTopics ");
//                            PreferencesUtils.getInstance(mActivity).putString(Constants.UserFCMSubscribe, "1");
//                        }
//                    }
//                });
//    }


    public static String getUdid(Context context) {
        TelephonyManager TelephonyMgr = (TelephonyManager) context.getSystemService(TELEPHONY_SERVICE);

        assert TelephonyMgr != null;
        String deviceId = null;
        try {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return UUID.randomUUID().toString();
            }
            deviceId = TelephonyMgr.getDeviceId();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (deviceId == null) {
                String uniqueID = UUID.randomUUID().toString();
                deviceId = uniqueID;
            }
        }
        Log.e("udid", "" + deviceId);

        return deviceId;

    }

//    public static void inviteApp(FragmentActivity mActivity) {
//
//        try {
//            Intent i = new Intent(Intent.ACTION_SEND);
//            i.setType("text/plain");
//            i.putExtra(Intent.EXTRA_SUBJECT, mActivity.getString(R.string.app_name));
//
//            String sAux = "Download "+mActivity.getString(R.string.app_name)+" App for checking In.\n";
//            sAux = sAux + " #"+mActivity.getString(R.string.app_name) + " https://play.google.com/store/apps/details?id="+APPLICATION_ID;
//            i.putExtra(Intent.EXTRA_TEXT, sAux);
//
//
//            mActivity.startActivity(Intent.createChooser(i, "Share via"));
//        } catch(Exception e) {
//            e.printStackTrace();
//        }
//    }

    public static int distanceLatLongInMeter(double lat1, double lon1, double lat2, double lon2) {
        Log.w(""+lat1+","+ lon1,""+lat2+","+lon2);

        int Radius = 6379000;
        //for Meter (6379000;// radius of earth in Meter)
        //for KM (6371;// radius of earth in Km)
//        lat1 =31.4593055; //StartP.latitude;
//        lon1 = 74.2763784;//StartP.longitude;
//
//        lat2 =31.459166;// EndP.latitude;
//        lon2 =74.276382;// EndP.longitude;

        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;
        DecimalFormat newFormat = new DecimalFormat("####");
        int kmInDec = Integer.valueOf(newFormat.format(km));
        double meter = valueResult % 1000;
        int meterInDec = Integer.valueOf(newFormat.format(meter));
//        android.util.Log.e("Radius Value", "" + valueResult + "   KM  " + km
//                + " Meter   " + (Integer.parseInt(newFormat.format(km*1000))) +" || "+(meterInDec));

//        return Radius * c;

        if((Integer.parseInt(newFormat.format(km)))<=MINIMUM_RADIUS_IN_METER){
            Log.e("meter",""+(Integer.parseInt(newFormat.format(km))));
        }
        return (Integer.parseInt(newFormat.format(km)));
    }


    public static String encodeBase64String(String simpleString) {
        String base64 = Base64.encodeToString(simpleString.getBytes(), Base64.DEFAULT);

        return base64;
    }
    public static String decodeBase64String(String encoded) {

            try {
                // Get bytes from string
                byte[] byteArray = Base64.decode(encoded.getBytes(), Base64.DEFAULT);

                // Print the decoded string
                String decodedString = new String(byteArray);
                return decodedString;

            } catch (Exception e) {
                e.printStackTrace();
                return encoded;
            }
        }



    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    private static String createNotificationChannel(Context context,boolean isImportant) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            String channelId = "Channel_id";

            CharSequence channelName = "Application_name";
            String channelDescription = "Application_name Alert";
            int channelImportance;
            boolean channelEnableVibrate = true;

            if(isImportant){
                channelImportance=NotificationManager.IMPORTANCE_DEFAULT;
            }else{
                channelImportance=NotificationManager.IMPORTANCE_LOW;
                channelEnableVibrate=false;
            }

            NotificationChannel notificationChannel =
                    new NotificationChannel(channelId, channelName, channelImportance);
            notificationChannel.setDescription(channelDescription);
            notificationChannel.enableVibration(channelEnableVibrate);
            if(!isImportant) {
                notificationChannel.enableLights(false);
                notificationChannel.setShowBadge(false);
                notificationChannel.setSound(null,null);
            }
            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(notificationChannel);

            return channelId;
        } else {
            return null;
        }
    }


//    private static int getNotificationIcon(NotificationCompat.Builder notificationBuilder) {
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            int color = 0x008000;
//            notificationBuilder.setColor(color);
//            return R.drawable.splash_logo;
//        }
//        return R.drawable.splash_logo;
//    }

//    public static void showNotification(Context context, String title, String messageBody, Bundle bundle, Intent intentFrom) {
//
//        Intent intent = new Intent(context, SplashActivity.class);
//
//        if(bundle==null){
//            bundle=new Bundle();
//        }
////        if (MainActivity.active) {
////            intent = MainActivity.instance().getIntent();
////
////            bundle.putBoolean("isFromMainActivity", true);
//////            intent.putExtras(bundle);
////        } else {
////            intent = new Intent(context, LinphoneLauncherActivity.class);
////            bundle.putBoolean("isFromMainActivity", false);
//////            intent.putExtras(bundle);
////        }
//
//        intent.putExtras(bundle);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        if(intentFrom!=null){
//            intent=intentFrom;
//        }
//        PendingIntent pendingIntent =
//                PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
//
//
//
//
//        String channel_id = createNotificationChannel(context,true);
//
//        NotificationCompat.Builder notificationBuilder =
//                new NotificationCompat.Builder(context, channel_id)
//                        .setContentTitle(title)
//                        .setContentText(messageBody)
//                        .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
//                        /*.setLargeIcon(largeIcon)*/
//                        .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
//                        .setContentIntent(pendingIntent)
//                        .setPriority(android.app.Notification.PRIORITY_HIGH)
//                        .setAutoCancel(true);
//
//        notificationBuilder.setSmallIcon(getNotificationIcon(notificationBuilder));
//
//        NotificationManager notificationManager =
//                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//       if(Utils.isShowNotification(context)) {
//           notificationManager.notify((int) ((new Date(System.currentTimeMillis()).getTime() / 1000L) % Integer.MAX_VALUE), notificationBuilder.build());
//       }
//    }

//    public static boolean isShowNotification(Context context){
//        return PreferencesUtils.getInstance(context).getBoolean(Constants.UserNotification, true);
//
//    }

//    public static boolean isShowPopup(Context context){
//        return PreferencesUtils.getInstance(context).getBoolean(Constants.UserPopup, true);
//    }

//    public static boolean isShowNewsOption(Context mActivity){
//        return PreferencesUtils.getInstance(mActivity).getBoolean(AppNewsShown,false);
//    }
//    public static String getAppDomainName(Context mActivity){
//        return PreferencesUtils.getInstance(mActivity).getString(AppNewsShown,"cq");
//    }
    //"/WhatsApp/Media/WhatsApp Images/Sent/"

//    public static void settingsUpdate(FragmentActivity mActivity, AppSettings settings){
//        PreferencesUtils.getInstance(mActivity).putBoolean(AppNewsShown,settings.isIs_news_shown());
//        PreferencesUtils.getInstance(mActivity).putString(AppDomainName,settings.getDomain_name());
//        Log.e("SettingsObj", "" + new Gson().toJson(settings));
//    }

//    public static void getSettingsDataFromFirebase(FragmentActivity fragmentActivity){
//        FirebaseFirestore firestore = FirebaseFirestore.getInstance();
//
//        DocumentReference docRef = firestore.collection("settings").document("app_settings");
//        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
//            @Override
//            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
//                if (task.isSuccessful()) {
//                    DocumentSnapshot document = task.getResult();
//                    if (document.exists()) {
//                        AppSettings settings = document.toObject(AppSettings.class);
//                        if (settings != null) {
//                            Utils.settingsUpdate(fragmentActivity,settings);
//                        }
//                        Log.d("Settings", "DocumentSnapshot data: " + document.getData());
//                    } else {
//                        Log.d("Settings", "No such document");
//                    }
//                } else {
//                    Log.d("Settings", "get failed with ", task.getException());
//                }
//
//            }
//        });
//        docRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
//            @Override
//            public void onEvent(@Nullable DocumentSnapshot snapshot,
//                                @Nullable FirebaseFirestoreException e) {
//                if (e != null) {
//                    Log.e("SettingsDoc", "Listen failed.", e);
//                    return;
//                }
//
//                if (snapshot != null && snapshot.exists()) {
//                    Log.e("SettingsDoc", "Current data: " + snapshot.getData());
//                    AppSettings settings = snapshot.toObject(AppSettings.class);
//                    if (settings != null) {
//                        Utils.settingsUpdate(fragmentActivity,settings);
//                    }
//
//                } else {
//                    Log.e("SettingsDoc", "Current data: null");
//                }
//            }
//        });
//
//    }


}
