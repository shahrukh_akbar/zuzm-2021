package com.darwingtechnologies.app.utilities;

public class Constants {
    public static String DEVICE_TYPE = "ANDROID";
    public static String AIR = "air";
    public static String BUS = "bus";
    public static String MOVIE = "movie";
    public static String TOUR = "tour";
    public static final String UserAuthToken = "UserAuthToken";
    public static final String UserCountry = "UserCountry";
    public static final String UserCountryId = "UserCountryId";

    public static final String UserCurrentCity = "UserCurrentCity";
    public static final String UserCurrentCountry = "UserCurrentCountry";
    public static final String UserCurrentLat = "UserCurrentLat";
    public static final String UserCurrentLong = "UserCurrentLong";
    public static final String UserCountryCallingCode = "UserCountryCallingCode";


    public static final String UserCurrentLatLongFor5Minutes = "UserCurrentLatLongFor5Minutes";
    public static final String CqOTPSignature="cqOTPSignature";

    public static class Keys {
        public static String LATLNG = "latlng";

    }
        public static class Messages {
        public static String LOCATION_PERMISSION_REQUIRED="Please accept location permission for Nearby Brokers.";

    }

    }
