package com.darwingtechnologies.app.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.darwingtechnologies.app.R;
import com.darwingtechnologies.app.business.models.UserModel;
import com.darwingtechnologies.app.utilities.CommonMethods;
import com.github.siyamed.shapeimageview.CircularImageView;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Rana Zeshan on 27-Jun-19.
 */
public class UsersListAdapter extends RecyclerView.Adapter<UsersListAdapter.ViewHolder> {

    private List<UserModel> listItems;
    private Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private View mView;
        public TextView tvName;
        public CircularImageView civAvatar;

        private ViewHolder(View view) {
            super(view);
            mView = view;
            tvName =mView.findViewById(R.id.tvName);
            civAvatar=mView.findViewById(R.id.civAvatar);
        }
    }

    public UsersListAdapter(List<UserModel> listItems){
        this.listItems = listItems;
        if(this.listItems==null){
            this.listItems=new ArrayList<>();
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(context==null)
            context = parent.getContext();
        View view = CommonMethods.createView(context, R.layout.list_item_user, parent);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final UserModel userModel = listItems.get(position);
        final String name=userModel.getName();
        final String image=userModel.getImage();


        try {
            holder.tvName.setText(name);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }



}

