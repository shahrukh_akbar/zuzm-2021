package com.darwingtechnologies.app.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.darwingtechnologies.app.R;
import com.darwingtechnologies.app.business.models.RegisterdFrenchiseFranchise;
import com.darwingtechnologies.app.utilities.CommonMethods;

import java.util.ArrayList;

public class AgentRegisterdListAdapter extends RecyclerView.Adapter<AgentRegisterdListAdapter.ViewHolder> {


    private Context context;
    private ArrayList<RegisterdFrenchiseFranchise> listItems = new ArrayList<>();
    private ArrayList<RegisterdFrenchiseFranchise> customerBookingListsFiltered = new ArrayList<>();

    public class ViewHolder extends RecyclerView.ViewHolder {
        private View mView;
        private TextView tvAgentName,tvAgentPhone,
                tvFranchiseName,tvFranchiseAdress,
                tvAdminName,tvAdminPhone;
        private View vAgent;

        private ViewHolder(View view) {
            super(view);
            mView = view;
            tvAgentName = mView.findViewById(R.id.tvAgentName);
            tvAgentPhone = mView.findViewById(R.id.tvAgentPhone);
            tvFranchiseName = mView.findViewById(R.id.tvFranchiseName);
            tvFranchiseAdress = mView.findViewById(R.id.tvFranchiseAdress);
            tvAdminName = mView.findViewById(R.id.tvAdminName);
            tvAdminPhone = mView.findViewById(R.id.tvAdminPhone);
            vAgent = mView.findViewById(R.id.vAgent);

        }
    }

    public AgentRegisterdListAdapter(ArrayList<RegisterdFrenchiseFranchise> listItems) {
        this.listItems=listItems;
//        this.customerBookingListsFiltered=customerBookingListsFiltered;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (context == null) context = parent.getContext();
        View view = CommonMethods.createView(context, R.layout.list_item_agent_registerd, parent);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final RegisterdFrenchiseFranchise registerdFrenchiseFranchise = listItems.get(position);


        if (listItems.get(position).getFranchise()!= null &&listItems.get(position).getFranchise().getAdmin()!=null){
            holder.tvAdminName.setText(listItems.get(position).getFranchise().getAdmin().getName());
            holder.tvAdminPhone.setText(listItems.get(position).getFranchise().getAdmin().getPhone());
        }else {
            holder.tvAdminName.setText("NA");
            holder.tvAdminPhone.setText("NA");
        }

//        if (! registerdFrenchiseFranchise.getFranchise().getAdmin().getPhone().equals("null") ){
//            holder.tvAgentPhone.setVisibility(View.VISIBLE);
//            holder.tvAdminPhone.setText(listItems.get(position).getFranchise().getAdmin().getPhone());

//        }else {
//            holder.tvAgentPhone.setVisibility(View.GONE);

//        }


//            holder.tvAgentName.setText(listItems.get(position).getSaleAgent().getName());
//            holder.tvAgentPhone.setText(listItems.get(position).getSaleAgent().getPhone());
            holder.tvFranchiseAdress.setText(listItems.get(position).getFranchise().getFranchiseAddress());
            holder.tvFranchiseName.setText(listItems.get(position).getFranchise().getFranchiseName());





    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

}
