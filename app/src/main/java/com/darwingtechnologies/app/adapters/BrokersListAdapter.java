package com.darwingtechnologies.app.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.darwingtechnologies.app.R;
import com.darwingtechnologies.app.business.models.UserModel;
import com.darwingtechnologies.app.business.models.searchBroker.BrokerResponse;
import com.darwingtechnologies.app.fragments.BrokerProfileFragment;
import com.darwingtechnologies.app.fragments.BrokersFragment;
import com.darwingtechnologies.app.utilities.CommonMethods;
import com.darwingtechnologies.app.utilities.CommonObjects;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Rana Zeshan on 27-Jun-19.
 */
public class BrokersListAdapter extends RecyclerView.Adapter<BrokersListAdapter.ViewHolder> {

    private List<BrokerResponse> listItems;
    private Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private View mView;
        public TextView tvName,tvShortDetail;
        public CircularImageView civAvatar;

        private ViewHolder(View view) {
            super(view);
            mView = view;
            tvName =mView.findViewById(R.id.tvName);
            tvShortDetail=mView.findViewById(R.id.tvShortDetail);
            civAvatar=mView.findViewById(R.id.civAvatar);
        }
    }

    public BrokersListAdapter(List<BrokerResponse> listItems){
        this.listItems = listItems;
        if(this.listItems==null){
            this.listItems=new ArrayList<>();
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(context==null)
            context = parent.getContext();
        View view = CommonMethods.createView(context, R.layout.item_broker_list, parent);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final BrokerResponse brokerResponse = listItems.get(position);
        final String name=brokerResponse.getPostTitle()!=null?brokerResponse.getPostTitle():"";
        final String shortDetail=brokerResponse.getPostContent()!=null?brokerResponse.getPostContent():"";
        String avatar=(brokerResponse.getSearchBrokerListInfo()!=null && brokerResponse.getSearchBrokerListInfo().getJobLogo()!=null && !brokerResponse.getSearchBrokerListInfo().getJobLogo().isEmpty())?brokerResponse.getSearchBrokerListInfo().getJobLogo():"";


        try {
            holder.tvName.setText(name);
            holder.tvShortDetail.setText(shortDetail);
            if(avatar!=null && !avatar.isEmpty()){
                Picasso.with(CommonObjects.getContext()).load(avatar).placeholder(R.drawable.broker_avatar).error(R.drawable.broker_avatar).into(holder.civAvatar);
            }else{
                Picasso.with(CommonObjects.getContext()).load(R.drawable.broker_avatar).into(holder.civAvatar);
            }

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CommonMethods.callFragment(BrokerProfileFragment.newInstance(brokerResponse), R.id.flFragmentOverlay, R.anim.fade_in, R.anim.fade_out, CommonObjects.getContext(), true);
                }
            });

            android.util.Log.e("avatar "+position,""+avatar);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }



}

