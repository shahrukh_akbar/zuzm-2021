package com.darwingtechnologies.app.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.darwingtechnologies.app.R;
import com.darwingtechnologies.app.business.models.Comment.CommentData;
import com.darwingtechnologies.app.utilities.CommonMethods;

import java.util.List;


/**
 * Created by Rana Zeshan on 27-Jun-19.
 */
public class CommentsListAdapter extends RecyclerView.Adapter<CommentsListAdapter.ViewHolder> {

    private List<CommentData> listItems;
    private Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private View mView;
        private TextView tvAuthorName,tvDateTime,tvComment;
        private ViewHolder(View view) {
            super(view);
            mView = view;

            tvAuthorName=mView.findViewById(R.id.tvAuthorName);
            tvDateTime=mView.findViewById(R.id.tvDateTime);
            tvComment=mView.findViewById(R.id.tvComment);


        }
    }

    public CommentsListAdapter(List<CommentData> listItems) {
        this.listItems = listItems;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(context==null)
            context = parent.getContext();
        View view = CommonMethods.createView(context, R.layout.list_item_comment, parent);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final CommentData commentData = listItems.get(position);

        try {

            holder.tvAuthorName.setText(commentData.getCommentAuthor());
            holder.tvDateTime.setText(""+commentData.getCommentDateGmt());//CommonMethods.getDateFormattedWithTimeAccess(commentData.getCommentDateGmt()));
            holder.tvComment.setText(CommonMethods.fromHtml(commentData.getCommentContent()));


        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }



}

