package com.darwingtechnologies.app.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.darwingtechnologies.app.R;
import com.darwingtechnologies.app.utilities.CommonMethods;

import java.util.ArrayList;


/**
 * Created by Rana Zeshan on 27-Jun-19.
 */
public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.ViewHolder> {

    private ArrayList<String> listItems;
    private Context context;
    private int size = 1;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private View mView;
        private ViewHolder(View view) {
            super(view);
            mView = view;


        }
    }

    public ChatListAdapter(ArrayList<String> listItems) {
        this.listItems = listItems;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(context==null)
            context = parent.getContext();
        View view = CommonMethods.createView(context, R.layout.item_chat, parent);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//        final String title = listItems.get(position);

//        Log.e("Title a",title);

        try {


        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return size;
    }



}

