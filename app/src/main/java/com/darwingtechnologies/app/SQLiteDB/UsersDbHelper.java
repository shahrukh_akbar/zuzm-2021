package com.darwingtechnologies.app.SQLiteDB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import com.darwingtechnologies.app.business.models.UserModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rana Zeshi on 28-Jun-19.
 */

public class UsersDbHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "usersdatabase";
    // table name
    private static final String TABLE_USERS = "jobstable";

    //Table Columns names
    private static final String KEY_ID= "id";
    private static final String KEY_USER_NAME= "userName";
    private static final String KEY_USER_IMAGE= "userImage";


    private SQLiteDatabase dbase;
    //public;
    public Context CContext;

    public UsersDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.CContext=context;
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        dbase=db;
        String sql = "CREATE TABLE IF NOT EXISTS " + TABLE_USERS + " ( "
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + KEY_USER_NAME+ " TEXT, "
                + KEY_USER_IMAGE+" TEXT )";

        db.execSQL(sql);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldV, int newV) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
        onCreate(db);
    }

    // Adding new patient
    public boolean addJob(UserModel userModel) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_USER_NAME,userModel.getName());
        values.put(KEY_USER_IMAGE,userModel.getImage());

        // Inserting Row
        long result=db.insert(TABLE_USERS, null, values);

        db.close();

        if(result==-1){
            return false;
        }else {
            return true;
        }
    }

    public List<UserModel> getAllUsers() {
        List<UserModel> jobModelList =null;
// Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_USERS;
        dbase=this.getReadableDatabase();
        Cursor cursor = dbase.rawQuery(selectQuery, null);
// looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            jobModelList= new ArrayList<UserModel>();
            do {
                final UserModel userModel = new UserModel();
                userModel.setId(cursor.getInt(0));
                userModel.setName(cursor.getString(1));
                userModel.setImage(cursor.getString(2));

                jobModelList.add(userModel);

            } while (cursor.moveToNext());

        }

        return jobModelList;
    }

    public int rowcount()
    {
        int row=0;
        String selectQuery = "SELECT  * FROM " + TABLE_USERS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        row=cursor.getCount();
        return row;
    }




}


