package com.darwingtechnologies.app.fragments;

import android.content.DialogInterface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.baoyz.widget.PullRefreshLayout;
import com.darwingtechnologies.app.BuildConfig;
import com.darwingtechnologies.app.R;
import com.darwingtechnologies.app.activities.MainActivity;
import com.darwingtechnologies.app.business.handlers.AppHandler;
import com.darwingtechnologies.app.business.models.getBrokerByCategory.GetBrokerByCategoryResponse;
import com.darwingtechnologies.app.business.models.getBrokerType.BrokerTypeResponse;
import com.darwingtechnologies.app.business.models.register.RegisterInput;
import com.darwingtechnologies.app.business.models.register.RegisterResponse;
import com.darwingtechnologies.app.business.models.register.newR.RegisterAsBrokerResponse;
import com.darwingtechnologies.app.business.models.setBrokerProfile.SetBrokerInput;
import com.darwingtechnologies.app.business.models.setBrokerProfile.SetBrokerProfileResponse;
import com.darwingtechnologies.app.utilities.CommonMethods;
import com.darwingtechnologies.app.utilities.LocaleHelper;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;

import static com.darwingtechnologies.app.activities.MainActivity.latLng;


/**
 * Created by Rana Zeshan on 27-Jun-19.
 */

public class SignUpFragment extends BaseFragment{
    private EditText etFullName,etEmail,etPassword,etCity;
    private CircularProgressButton btnSubmit;
    private LinearLayout llContentForBroker;
    private TextView tvSignUp,tvSignupAs;

    private Boolean isBroker;


    private TextView tvPickLocation;

    private EditText etTagLine,etDescription,etUserName,etPhone,etWhatsappNumber;
    private TextView tvBecomeBroker;

    private Spinner spnCategory, spnType;
    private int typePosition = 0;
    private int categoryPosition = 0;

    private ArrayList<GetBrokerByCategoryResponse> getBrokerByCategoryList = new ArrayList<>();
    private ArrayList<BrokerTypeResponse> getBrokerTypeList = new ArrayList<>();


    private PullRefreshLayout prlLoadData;


    @Override
    protected void initView() {
        setIbBack(mView.findViewById(R.id.ibBack),null);
        tvSignupAs=mView.findViewById(R.id.tvSignupAs);
        etFullName = mView.findViewById(R.id.etFullName);
        etUserName=mView.findViewById(R.id.etUserName);
        etEmail = mView.findViewById(R.id.etEmail);
        etPassword = mView.findViewById(R.id.etPassword);

        prlLoadData=mView.findViewById(R.id.prlLoadData);
        prlLoadData.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(isBroker){
                    getBrokerTypeData();
                }
            }
        });
        llContentForBroker=mView.findViewById(R.id.llContentForBroker);

        etTagLine=mView.findViewById(R.id.etTagLine);
        etDescription=mView.findViewById(R.id.etDescription);
        etPhone=mView.findViewById(R.id.etPhone);
        etWhatsappNumber=mView.findViewById(R.id.etWhatsappNumber);
        tvBecomeBroker=mView.findViewById(R.id.tvBecomeBroker);

        spnCategory = mView.findViewById(R.id.spnCategory);
        spnType = mView.findViewById(R.id.spnType);
        spnCategory.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(
                            AdapterView<?> adapterView, View view, int i, long l) {
                        categoryPosition = i;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {}
                });
        spnType.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(
                            AdapterView<?> adapterView, View view, int i, long l) {
                        typePosition = i;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {}
                });

        etCity=mView.findViewById(R.id.etCity);
        tvPickLocation=mView.findViewById(R.id.tvPickLocation);
        tvPickLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CommonMethods.callFragment(MapViewFragment.newInstance(latLng,true,true), R.id.flFragmentContainer, R.anim.fade_in, R.anim.fade_out, mActivity, true);

//                CommonMethods.callFragment(HomeMapFragment.newInstance(), R.id.flFragmentOverlay, R.anim.fade_in, R.anim.fade_out, mActivity, true);
            }
        });



        tvSignUp = mView.findViewById(R.id.tvSignUp);
        tvSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkValidation()) {
                    if(isBroker){
                        setupForBroker();
                    }else{
                        setupSimpleUser();
                    }
                }
            }
        });

        if(BuildConfig.DEBUG){

            //franchise testing login
//            etEmail.setText("saleagent@gmail.com");
//            etPassword.setText("12345678");

        }

        CommonMethods.setupUI(mView,mActivity);
    }

    @Override
    protected void loadData() {

        tvSignupAs.setText(isBroker?""+getString(R.string.broker):""+getString(R.string.customer));
        if(isBroker){
            llContentForBroker.setVisibility(View.VISIBLE);
            getBrokerTypeData();
        }else{
            llContentForBroker.setVisibility(View.GONE);
        }


try{
    android.util.Log.e("LatLng", ""+latLng.toString());
}catch (Exception e){
    e.printStackTrace();
}
        UpdateUI();

    }

    public static SignUpFragment newInstance(boolean isBroker) {
        SignUpFragment fragment = new SignUpFragment();
        fragment.isBroker=isBroker;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.sign_up_fragment, null);
    }

    @Override
    public String getFragmentTag() {
        return SignUpFragment.class.getSimpleName();
    }



    boolean checkValidation(){
        if(etFullName.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please Enter Your Full Name.");
            return false;
        }else if(etUserName.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please enter your Username to continue");
            return false;
        }else if(etUserName.getText().toString().contains(" ")){
            CommonMethods.showMessage(mActivity,"Please enter username without space to continue");
            return false;
        }else if(etEmail.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please Enter Your Email.");
            return false;
        }else if(etEmail.getText().toString().contains("@") && !CommonMethods.isEmailValid(etEmail.getText().toString())){
            CommonMethods.showMessage(mActivity,"Please enter Valid Email.");
            return false;
        } else if(etPassword.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please Enter Your Password.");
            return false;
        }else if(isBroker){
            return checkBrokerProfileValidation();
        }else{
            return true;
        }


    }


    boolean checkBrokerProfileValidation(){

         if(etPhone.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please enter your Phone to continue");
            return false;
        }else if(etWhatsappNumber.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please enter your Whatsapp Number to continue");
            return false;
        }else if(etTagLine.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please enter your Tagline to continue");
            return false;
        }else if(etDescription.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please enter Description to continue");
            return false;
        }else if(typePosition==0){
            CommonMethods.showMessage(mActivity,"Please select Broker Type to continue");
            return false;
        }else if(categoryPosition==0){
            CommonMethods.showMessage(mActivity,"Please select Broker Category to continue");
            return false;
        }else if(tvPickLocation.getText().toString().isEmpty()){
             CommonMethods.showMessage(mActivity,"Please pick your Location to continue");
             return false;
         }else if(latLng==null){
             CommonMethods.showMessage(mActivity,"Please pick your Location to continue");
             return false;
         }else if(etCity.getText().toString().isEmpty()){
             CommonMethods.showMessage(mActivity,"Please enter City Name to continue");
             return false;
         }else{
            return true;
        }

    }

    public void setupSimpleUser(){
        RegisterInput registerInput=new RegisterInput();
        registerInput.setEmail(etEmail.getText().toString());
        registerInput.setPassword(etPassword.getText().toString());
        registerInput.setUsername(etUserName.getText().toString());
        String fName="";
        String lName="";

        if(etFullName.getText().toString().contains(" ")){
             fName=etFullName.getText().toString().split(" ")[0];
            for (int i = 1; i <etFullName.getText().toString().split(" ").length ; i++) {
                lName+=etFullName.getText().toString().split(" ")[i]+" ";
            }
        }else{
            fName=etFullName.getText().toString();
        }
        registerInput.setFirstName(fName);
        registerInput.setLastName(lName);

        AppHandler.registerUser(registerInput, new AppHandler.RegisterUserListener() {
            @Override
            public void onSuccess(RegisterResponse registerResponse) {
                CommonMethods.hideProgressDialog();

                CommonMethods.callFragment(SignInFragment.newInstance(), R.id.flFragmentContainer, R.anim.fade_in, R.anim.fade_out, mActivity, false);

            }

            @Override
            public void onError(String error) {
                CommonMethods.hideProgressDialog();
                CommonMethods.showMessage(mActivity,error);
            }
        });

    }


    void getBrokerTypeData(){

        AppHandler.getBrokerType(true,new AppHandler.GetBrokerTypeListener() {
            @Override
            public void onSuccess(ArrayList<BrokerTypeResponse> getBrokerTypeResponse) {
                getBrokerTypeList.clear();
                getBrokerTypeList.addAll(getBrokerTypeResponse);
                setUpSpinner(spnType, getBrokerTypeList(getBrokerTypeList));
                spnType.setSelection(typePosition);

                getBrokerCategoryData();

            }

            @Override
            public void onError(String error) {
                prlLoadData.setRefreshing(false);
                CommonMethods.showMessage(mActivity,error);
                CommonMethods.hideProgressDialog();
            }
        });
    }

    private String[] getBrokerTypeList( ArrayList<BrokerTypeResponse> transportServicesArrival){
        int size =
                transportServicesArrival.size();
        String[] serviceList = new String[(size+1)];

        for (int i = 0; i < serviceList.length; i++) {
            if(i==0){
                serviceList[i] = getString(R.string.select_broker_type);//"Select Broker Type";
            }else {
                String en=transportServicesArrival.get((i-1)).getPostTitle();
                String ar=transportServicesArrival.get((i-1)).getPostTitle();
                if(transportServicesArrival.get((i-1)).getPostTitle()!=null && !transportServicesArrival.get((i-1)).getPostTitle().isEmpty() && transportServicesArrival.get((i-1)).getPostTitle().contains("/#")){

                    try {
                        en = transportServicesArrival.get((i-1)).getPostTitle().split("/#")[0];
                        ar = transportServicesArrival.get((i-1)).getPostTitle().split("/#")[1];
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

                android.util.Log.e("BrokerType(" + (i-1)+")", "en:" + en + "|| ar:" + ar);
                serviceList[i] =LocaleHelper.isDefaultLanguage(MainActivity.instance())? ""+en:""+ar;

//                android.util.Log.e("ServiceName() " + i, "" + transportServicesArrival.get((i-1)).getPostTitle());
//                serviceList[i] = String.valueOf(transportServicesArrival.get((i-1)).getPostTitle());
            }
        }
        return serviceList;
    }


    void getBrokerCategoryData(){

        AppHandler.getBrokerByCategory(new AppHandler.GetBrokerByCategoryListener() {
            @Override
            public void onSuccess(ArrayList<GetBrokerByCategoryResponse> getBrokerByCategoryResponses) {
                getBrokerByCategoryList.clear();
                getBrokerByCategoryList.addAll(getBrokerByCategoryResponses);
                setUpSpinner(spnCategory, getBrokerCatList(getBrokerByCategoryList));
                spnCategory.setSelection(categoryPosition);
                prlLoadData.setRefreshing(false);
            }

            @Override
            public void onError(String error) {
                prlLoadData.setRefreshing(false);
                CommonMethods.showMessage(mActivity,error);
                CommonMethods.hideProgressDialog();
            }
        });
    }

    private String[] getBrokerCatList( ArrayList<GetBrokerByCategoryResponse> transportServicesArrival){
        int size =
                transportServicesArrival.size();
        String[] serviceList = new String[(size+1)];

        for (int i = 0; i < serviceList.length; i++) {
            if(i==0){
                serviceList[i] = getString(R.string.select_broker_category);//"Select Broker Category";
            }else {
                String en=transportServicesArrival.get((i-1)).getName();
                String ar=transportServicesArrival.get((i-1)).getName();
                if(transportServicesArrival.get((i-1)).getName()!=null && !transportServicesArrival.get((i-1)).getName().isEmpty() && transportServicesArrival.get((i-1)).getName().contains("/#")){

                    try {
                        en = transportServicesArrival.get((i-1)).getName().split("/#")[0];
                        ar = transportServicesArrival.get((i-1)).getName().split("/#")[1];
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                android.util.Log.e("BrokerCategory(" + (i-1)+")", "en:" + en + "|| ar:" + ar);
                serviceList[i] = LocaleHelper.isDefaultLanguage(MainActivity.instance())? ""+en:""+ar;

//                android.util.Log.e("ServiceName() " + i, "" + transportServicesArrival.get((i-1)).getName());
//                serviceList[i] = String.valueOf(transportServicesArrival.get((i-1)).getName());
            }
        }
        return serviceList;
    }

    private void setUpSpinner(Spinner spinner, String[] items) {
        ArrayAdapter<String> dataAdapter =
                new ArrayAdapter<>(mActivity, R.layout.item_spinner_detailing, items);
        dataAdapter.setDropDownViewResource(R.layout.item_spinner_dropdown_detailing);
        spinner.setAdapter(dataAdapter);
    }

    void UpdateUI(){

        if(latLng!=null){
            try {
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(mActivity, Locale.getDefault());

                addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                String address =addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

                if(etCity.getText().toString().isEmpty()){
                    etCity.setText(""+city);
                }

                tvPickLocation.setText(""+address);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    void setupForBroker(){
        SetBrokerInput setBrokerInput=new SetBrokerInput();
        setBrokerInput.setPost_title(etFullName.getText().toString());
        setBrokerInput.setTagline(etTagLine.getText().toString());
        setBrokerInput.setPost_content(etDescription.getText().toString());
        setBrokerInput.setEmail(etEmail.getText().toString());
        setBrokerInput.setPhone(etPhone.getText().toString());
        setBrokerInput.setWhatsapp_number(etWhatsappNumber.getText().toString());
        setBrokerInput.setBroker_location(etCity.getText().toString());
        setBrokerInput.setGeolocation_lat(""+latLng.latitude);
        setBrokerInput.setGeolocation_long(""+latLng.longitude);
        setBrokerInput.setUsername(etUserName.getText().toString());
        setBrokerInput.setPassword(etPassword.getText().toString());
        //                    _job_cover
//_job_logo


        if(getBrokerTypeList!=null && getBrokerTypeList.size()>0){
            setBrokerInput.setListing_type(""+getBrokerTypeList.get((typePosition-1)).getPostName());
        }

        if(getBrokerByCategoryList!=null && getBrokerByCategoryList.size()>0){
            setBrokerInput.setCat(""+getBrokerByCategoryList.get((categoryPosition-1)).getTermId());
        }

        becomeBroker(setBrokerInput);
    }

    void becomeBroker(SetBrokerInput setBrokerInput){
        AppHandler.registerAsBroker(setBrokerInput, new AppHandler.RegisterAsBrokerListener() {
            @Override
            public void onSuccess(RegisterAsBrokerResponse setBrokerProfileResponse) {
                if(setBrokerProfileResponse!=null){
                    CommonMethods.displayMessagesDialog("You are a Broker now.",mActivity).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
//                            getIbBack().performClick();

                            CommonMethods.callFragment(SignInFragment.newInstance(), R.id.flFragmentContainer, R.anim.fade_in, R.anim.fade_out, mActivity, false);
                        }
                    }).setTitle("Congratulations").setCancelable(false).create().show();
                }
            }

            @Override
            public void onError(String error) {
                CommonMethods.showMessage(mActivity,""+error);

            }
        });
    }

}
