package com.darwingtechnologies.app.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.baoyz.widget.PullRefreshLayout;
import com.darwingtechnologies.app.R;
import com.darwingtechnologies.app.activities.MainActivity;
import com.darwingtechnologies.app.business.handlers.AppHandler;
import com.darwingtechnologies.app.business.models.getBrokerByCategory.GetBrokerByCategoryResponse;
import com.darwingtechnologies.app.business.models.getBrokerType.BrokerTypeResponse;
import com.darwingtechnologies.app.business.models.register.newR.RegisterAsBrokerResponse;
import com.darwingtechnologies.app.business.models.setBrokerProfile.SetBrokerInput;
import com.darwingtechnologies.app.business.models.setBrokerProfile.SetBrokerProfileResponse;
import com.darwingtechnologies.app.business.models.userLogin.UserLoginResponse;
import com.darwingtechnologies.app.utilities.CommonMethods;
import com.darwingtechnologies.app.utilities.CommonObjects;
import com.darwingtechnologies.app.utilities.LocaleHelper;
import com.darwingtechnologies.app.utilities.PreferencesUtils;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.darwingtechnologies.app.activities.MainActivity.latLng;


/**
 * Created by Rana Zeshan on 27-Jun-19.
 */

public class BecomeBrokerFragment extends BaseFragment{

    private TextView tvPickLocation;
    private EditText etFullName,etEmail,etPassword,etUserName;

    private EditText etTagLine,etDescription,etPhone,etWhatsappNumber,etCity;
    private TextView tvSignUp;

    private Spinner spnCategory, spnType;
    private int typePosition = 0;
    private int categoryPosition = 0;

    private ArrayList<GetBrokerByCategoryResponse> getBrokerByCategoryList = new ArrayList<>();
    private ArrayList<BrokerTypeResponse> getBrokerTypeList = new ArrayList<>();


    private UserLoginResponse userLoginResponse;
    private PullRefreshLayout prlLoadData;

    @Override
    protected void initView() {
        setIbBack(mView.findViewById(R.id.ibBack),""+getString(R.string.become_a_broker));

        etFullName = mView.findViewById(R.id.etFullName);
        etUserName=mView.findViewById(R.id.etUserName);
        etEmail = mView.findViewById(R.id.etEmail);
        etPassword = mView.findViewById(R.id.etPassword);

        prlLoadData=mView.findViewById(R.id.prlLoadData);
        prlLoadData.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                    getBrokerTypeData();
            }
        });

        etTagLine=mView.findViewById(R.id.etTagLine);
        etDescription=mView.findViewById(R.id.etDescription);
        etPhone=mView.findViewById(R.id.etPhone);
        etWhatsappNumber=mView.findViewById(R.id.etWhatsappNumber);

        spnCategory = mView.findViewById(R.id.spnCategory);
        spnType = mView.findViewById(R.id.spnType);
        spnCategory.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(
                            AdapterView<?> adapterView, View view, int i, long l) {
                        categoryPosition = i;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {}
                });
        spnType.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(
                            AdapterView<?> adapterView, View view, int i, long l) {
                        typePosition = i;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {}
                });

        etCity=mView.findViewById(R.id.etCity);
        tvPickLocation=mView.findViewById(R.id.tvPickLocation);
        tvPickLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CommonMethods.callFragment(MapViewFragment.newInstance(latLng,true,true), R.id.flFragmentOverlay, R.anim.fade_in, R.anim.fade_out, mActivity, true);

//                CommonMethods.callFragment(HomeMapFragment.newInstance(), R.id.flFragmentOverlay, R.anim.fade_in, R.anim.fade_out, mActivity, true);
            }
        });



        tvSignUp = mView.findViewById(R.id.tvSignUp);
        tvSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkValidation()) {
                        setupForBroker();

                }
            }
        });


        CommonMethods.setupUI(mView,mActivity);
    }

    @Override
    protected void loadData() {

        userLoginResponse=MainActivity.userLoginResponse;
        getBrokerTypeData();

            UpdateUI();


    }

    public static BecomeBrokerFragment newInstance() {
        BecomeBrokerFragment fragment = new BecomeBrokerFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.become_a_broker_fragment, null);
    }

    @Override
    public String getFragmentTag() {
        return BecomeBrokerFragment.class.getSimpleName();
    }


    void UpdateUI(){

        if(latLng!=null){
            try {
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(mActivity, Locale.getDefault());

                addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
                if(etCity.getText().toString().isEmpty()){
                    etCity.setText(""+city);
                }
                tvPickLocation.setText(""+address);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        if(userLoginResponse!=null && userLoginResponse.getData()!=null){
            etFullName.setText(userLoginResponse.getData().getDisplayName());
            etEmail.setText(userLoginResponse.getData().getUserEmail());

            if(userLoginResponse.getData().getUserInfo()!=null){
                etPhone.setText(userLoginResponse.getData().getUserInfo().getPhone());
                etWhatsappNumber.setText(userLoginResponse.getData().getUserInfo().getPhone());
            }
        }

    }


    void getBrokerCategoryData(){

        AppHandler.getBrokerByCategory(new AppHandler.GetBrokerByCategoryListener() {
            @Override
            public void onSuccess(ArrayList<GetBrokerByCategoryResponse> getBrokerByCategoryResponses) {
                prlLoadData.setRefreshing(false);
                getBrokerByCategoryList.clear();
                getBrokerByCategoryList.addAll(getBrokerByCategoryResponses);
                setUpSpinner(spnCategory, getBrokerCatList(getBrokerByCategoryList));
                spnCategory.setSelection(categoryPosition);
            }

            @Override
            public void onError(String error) {
                prlLoadData.setRefreshing(false);
                CommonMethods.showMessage(mActivity,error);
                CommonMethods.hideProgressDialog();
            }
        });
    }

    private String[] getBrokerCatList( ArrayList<GetBrokerByCategoryResponse> transportServicesArrival){
        int size = transportServicesArrival.size();
        String[] serviceList = new String[(size+1)];

        for (int i = 0; i < serviceList.length; i++) {

            if(i==0){
                serviceList[i] =  getString(R.string.select_broker_category);//"Select Broker Category";
            }else {

                String en=transportServicesArrival.get((i-1)).getName();
                String ar=transportServicesArrival.get((i-1)).getName();
                if(transportServicesArrival.get((i-1)).getName()!=null && !transportServicesArrival.get((i-1)).getName().isEmpty() && transportServicesArrival.get((i-1)).getName().contains("/#")){

                    try {
                        en = transportServicesArrival.get((i-1)).getName().split("/#")[0];
                        ar = transportServicesArrival.get((i-1)).getName().split("/#")[1];
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                android.util.Log.e("BrokerCategory(" + (i-1)+")", "en:" + en + "|| ar:" + ar);
                serviceList[i] = LocaleHelper.isDefaultLanguage(MainActivity.instance())? ""+en:""+ar;

//                android.util.Log.e("ServiceName() " + i, "" + transportServicesArrival.get((i-1)).getName());
//                serviceList[i] = String.valueOf(transportServicesArrival.get((i-1)).getName());
            }
        }
        return serviceList;
    }

    private void setUpSpinner(Spinner spinner, String[] items) {
        ArrayAdapter<String> dataAdapter =
                new ArrayAdapter<>(mActivity, R.layout.item_spinner_detailing, items);
        dataAdapter.setDropDownViewResource(R.layout.item_spinner_dropdown_detailing);
        spinner.setAdapter(dataAdapter);
    }


    void getBrokerTypeData(){

        AppHandler.getBrokerType(true,new AppHandler.GetBrokerTypeListener() {
            @Override
            public void onSuccess(ArrayList<BrokerTypeResponse> getBrokerTypeResponse) {
                getBrokerTypeList.clear();
                getBrokerTypeList.addAll(getBrokerTypeResponse);
                setUpSpinner(spnType, getBrokerTypeList(getBrokerTypeList));
                spnType.setSelection(typePosition);
                getBrokerCategoryData();
            }

            @Override
            public void onError(String error) {
                prlLoadData.setRefreshing(false);
                CommonMethods.showMessage(mActivity,error);
                CommonMethods.hideProgressDialog();
            }
        });
    }

    private String[] getBrokerTypeList( ArrayList<BrokerTypeResponse> transportServicesArrival){
        int size =
                transportServicesArrival.size();
        String[] serviceList = new String[(size+1)];

        for (int i = 0; i < serviceList.length; i++) {
            if(i==0){
                serviceList[i] = getString(R.string.select_broker_type);//"Select Broker Type";
            }else {
                String en=transportServicesArrival.get((i-1)).getPostTitle();
                String ar=transportServicesArrival.get((i-1)).getPostTitle();
                if(transportServicesArrival.get((i-1)).getPostTitle()!=null && !transportServicesArrival.get((i-1)).getPostTitle().isEmpty() && transportServicesArrival.get((i-1)).getPostTitle().contains("/#")){

                    try {
                        en = transportServicesArrival.get((i-1)).getPostTitle().split("/#")[0];
                        ar = transportServicesArrival.get((i-1)).getPostTitle().split("/#")[1];
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

                android.util.Log.e("BrokerType(" + (i-1)+")", "en:" + en + "|| ar:" + ar);
                serviceList[i] =LocaleHelper.isDefaultLanguage(MainActivity.instance())? ""+en:""+ar;

//                android.util.Log.e("ServiceName() " + i, "" + transportServicesArrival.get((i-1)).getPostTitle());
//                serviceList[i] = String.valueOf(transportServicesArrival.get((i-1)).getPostTitle());
            }
        }
        return serviceList;
    }



    boolean checkValidation(){
        if(etFullName.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please Enter Your Full Name.");
            return false;
        }else if(etUserName.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please enter your Username to continue");
            return false;
        }else if(etUserName.getText().toString().contains(" ")){
            CommonMethods.showMessage(mActivity,"Please enter username without space to continue");
            return false;
        }else if(etEmail.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please Enter Your Email.");
            return false;
        }else if(etEmail.getText().toString().contains("@") && !CommonMethods.isEmailValid(etEmail.getText().toString())){
            CommonMethods.showMessage(mActivity,"Please enter Valid Email.");
            return false;
        } else if(etPassword.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please Enter Your Password.");
            return false;
        }else{
            return checkBrokerProfileValidation();
        }


    }


    boolean checkBrokerProfileValidation(){

        if(etPhone.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please enter your Phone to continue");
            return false;
        }else if(etWhatsappNumber.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please enter your Whatsapp Number to continue");
            return false;
        }else if(etTagLine.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please enter your Tagline to continue");
            return false;
        }else if(etDescription.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please enter Description to continue");
            return false;
        }else if(typePosition==0){
            CommonMethods.showMessage(mActivity,"Please select Broker Type to continue");
            return false;
        }else if(categoryPosition==0){
            CommonMethods.showMessage(mActivity,"Please select Broker Category to continue");
            return false;
        }else if(tvPickLocation.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please pick your Location to continue");
            return false;
        }else if(latLng==null){
            CommonMethods.showMessage(mActivity,"Please pick your Location to continue");
            return false;
        }else if(etCity.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please enter City Name to continue");
            return false;
        }else{
            return true;
        }

    }

    void setupForBroker(){
        SetBrokerInput setBrokerInput=new SetBrokerInput();
        setBrokerInput.setPost_title(etFullName.getText().toString());
        setBrokerInput.setTagline(etTagLine.getText().toString());
        setBrokerInput.setPost_content(etDescription.getText().toString());
        setBrokerInput.setEmail(etEmail.getText().toString());
        setBrokerInput.setPhone(etPhone.getText().toString());
        setBrokerInput.setWhatsapp_number(etWhatsappNumber.getText().toString());
        setBrokerInput.setBroker_location(etCity.getText().toString());
        setBrokerInput.setGeolocation_lat(""+latLng.latitude);
        setBrokerInput.setGeolocation_long(""+latLng.longitude);
        setBrokerInput.setUsername(etUserName.getText().toString());
        setBrokerInput.setPassword(etPassword.getText().toString());
        //                    _job_cover
//_job_logo


        if(getBrokerTypeList!=null && getBrokerTypeList.size()>0){
            setBrokerInput.setListing_type(""+getBrokerTypeList.get((typePosition-1)).getPostName());
        }

        if(getBrokerByCategoryList!=null && getBrokerByCategoryList.size()>0){
            setBrokerInput.setCat(""+getBrokerByCategoryList.get((categoryPosition-1)).getTermId());
        }

        becomeBroker(setBrokerInput);
    }

    void becomeBroker(SetBrokerInput setBrokerInput){
        AppHandler.registerAsBroker(setBrokerInput, new AppHandler.RegisterAsBrokerListener() {
            @Override
            public void onSuccess(RegisterAsBrokerResponse setBrokerProfileResponse) {
                if(setBrokerProfileResponse!=null){
                    CommonMethods.displayMessagesDialog("You are a Broker now. Please login with mentioned credentials to continue.",mActivity).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();

                            PreferencesUtils.getInstance(mActivity).clearPreferences();
                            CommonMethods.clearPreferences((FragmentActivity) CommonObjects.getContext(),getString(R.string.app_name));
                            startActivity(new Intent(CommonObjects.getContext(), MainActivity.class));
                            ((FragmentActivity) CommonObjects.getContext()).finishAffinity();


                        }
                    }).setTitle("Congratulations").setCancelable(false).create().show();
                }
            }

            @Override
            public void onError(String error) {
                CommonMethods.showMessage(mActivity,""+error);

            }
        });
    }






}
