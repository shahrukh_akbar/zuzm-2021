package com.darwingtechnologies.app.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.darwingtechnologies.app.R;
import com.darwingtechnologies.app.activities.MainActivity;
import com.darwingtechnologies.app.adapters.CommentsListAdapter;
import com.darwingtechnologies.app.business.handlers.AppHandler;
import com.darwingtechnologies.app.business.models.Comment.CommentData;
import com.darwingtechnologies.app.business.models.Comment.addComment.AddCommentData;
import com.darwingtechnologies.app.business.models.Comment.addComment.AddCommentResponse;
import com.darwingtechnologies.app.business.models.getBrokerType.BrokerTypeResponse;
import com.darwingtechnologies.app.business.models.searchBroker.BrokerResponse;
import com.darwingtechnologies.app.business.models.userLogin.UserLoginResponse;
import com.darwingtechnologies.app.utilities.CommonMethods;
import com.darwingtechnologies.app.utilities.CommonObjects;
import com.darwingtechnologies.app.utilities.LocaleHelper;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Rana Zeshan on 27-Jun-19.
 */

public class BrokerProfileFragment extends BaseFragment{
    private TextView tvName,tvAddress,tvShorDescription;
    private CircularImageView civAvatar;
    private ImageView ivCover;
    private LinearLayout llCommentSection,llCategorySection;
    private RecyclerView rvListItems;
    private ProgressBar pbLoading,pbLoadingCategories;

    private LinearLayout llCategory;
    private TextView tvNameOfCategory;

    private BrokerResponse brokerResponse;
    private BrokerTypeResponse brokerTypeResponse;
    private List<CommentData> comments;

    private ArrayList<BrokerTypeResponse> brokerTypeList = new ArrayList<>();


    private LinearLayout llGetDirection,llCall,llEmail,llWhatsapp;
    private TextView tvPostComment;
    private EditText etComment;


    private UserLoginResponse userLoginResponse;




    @Override
    protected void initView() {
        setIbBack((ImageButton)mView.findViewById(R.id.ibBack), "Profile");

        civAvatar = mView.findViewById(R.id.civAvatar);
        ivCover=mView.findViewById(R.id.ivCover);
        tvName=mView.findViewById(R.id.tvName);
        tvAddress=mView.findViewById(R.id.tvAddress);
        tvShorDescription=mView.findViewById(R.id.tvShorDescription);

        llGetDirection=mView.findViewById(R.id.llGetDirection);
        llGetDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(brokerResponse.getSearchBrokerListInfo()!=null && brokerResponse.getSearchBrokerListInfo().getGeolocationLat()!=null && !brokerResponse.getSearchBrokerListInfo().getGeolocationLat().isEmpty() && brokerResponse.getSearchBrokerListInfo().getGeolocationLong()!=null && !brokerResponse.getSearchBrokerListInfo().getGeolocationLong().isEmpty()){

                    CommonMethods.getDirectionFromLatLng(mActivity,new LatLng(Double.parseDouble(brokerResponse.getSearchBrokerListInfo().getGeolocationLat()),Double.parseDouble(brokerResponse.getSearchBrokerListInfo().getGeolocationLong())));


//                    Uri gmmIntentUri = Uri.parse("geo:"+brokerResponse.getSearchBrokerListInfo().getGeolocationLat()+","+brokerResponse.getSearchBrokerListInfo().getGeolocationLong());
//                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//                    mapIntent.setPackage("com.google.android.apps.maps");
//                    if (mapIntent.resolveActivity(mActivity.getPackageManager()) != null) {
//                        startActivity(mapIntent);
//                    }else{
//                        CommonMethods.showMessage(mActivity,"Look like you don't have maps application to view");
//                    }
                }else{
                    CommonMethods.showMessage(mActivity,"We are unable to get direction for selected user.");
                }
            }
        });

        llCall=mView.findViewById(R.id.llCall);
        llCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(brokerResponse.getSearchBrokerListInfo()!=null && brokerResponse.getSearchBrokerListInfo().getJobPhone()!=null && !brokerResponse.getSearchBrokerListInfo().getJobPhone().isEmpty()){
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("tel:" + brokerResponse.getSearchBrokerListInfo().getJobPhone()));
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }else{
                    CommonMethods.showMessage(mActivity,"We are unable to get a call for selected user.");
                }
            }
        });

        llEmail=mView.findViewById(R.id.llEmail);
        llEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(brokerResponse.getSearchBrokerListInfo()!=null && brokerResponse.getSearchBrokerListInfo().getJobEmail()!=null && !brokerResponse.getSearchBrokerListInfo().getJobEmail().isEmpty()){
                   CommonMethods.composeEmail(mActivity,brokerResponse.getSearchBrokerListInfo().getJobEmail(),"Mail from "+getString(R.string.app_name),null,"");
                }else{
                    CommonMethods.showMessage(mActivity,"We are unable to compose email for selected user.");
                }
            }
        });

        llWhatsapp=mView.findViewById(R.id.llWhatsapp);
        llWhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(brokerResponse.getSearchBrokerListInfo()!=null && brokerResponse.getSearchBrokerListInfo().getWhatsappNumber()!=null && !brokerResponse.getSearchBrokerListInfo().getWhatsappNumber().toString().isEmpty()){

                    String url = "https://api.whatsapp.com/send?phone="+brokerResponse.getSearchBrokerListInfo().getWhatsappNumber().toString();
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                }else{
                    CommonMethods.showMessage(mActivity,"We are unable to open whatsapp for selected user.");
                }
            }
        });







        llCommentSection=mView.findViewById(R.id.llCommentSection);
        rvListItems=mView.findViewById(R.id.rvListItems);
        rvListItems.setLayoutManager(new LinearLayoutManager(mActivity,LinearLayoutManager.VERTICAL,false));
        pbLoading=mView.findViewById(R.id.pbLoading);
        showHideCommentSection(false);

        etComment=mView.findViewById(R.id.etComment);
        tvPostComment=mView.findViewById(R.id.tvPostComment);
        tvPostComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkValidationForAddComment()){
                    postComment();
                }
            }
        });


        llCategorySection=mView.findViewById(R.id.llCategorySection);
        pbLoadingCategories=mView.findViewById(R.id.pbLoadingCategories);
        tvNameOfCategory=mView.findViewById(R.id.tvNameOfCategory);
        llCategory=mView.findViewById(R.id.llCategory);
        showHideCategorySection(false);


        CommonMethods.setupUI(mView,mActivity);
    }

    @Override
    protected void loadData() {

        setBrokerData();

        userLoginResponse=CommonMethods.getUserDetail(mActivity);
        if(userLoginResponse!=null){

        }

    }

    public static BrokerProfileFragment newInstance(BrokerResponse brokerResponse) {
        BrokerProfileFragment fragment = new BrokerProfileFragment();
        fragment.brokerResponse=brokerResponse;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.broker_profile_fragment, null);
    }

    @Override
    public String getFragmentTag() {
        return BrokerProfileFragment.class.getSimpleName();
    }

    void setBrokerData(){
        if(brokerResponse!=null){
            String name=brokerResponse.getPostTitle()!=null?brokerResponse.getPostTitle():"";
            String address=(brokerResponse.getSearchBrokerListInfo()!=null && brokerResponse.getSearchBrokerListInfo().getJobLocation()!=null && !brokerResponse.getSearchBrokerListInfo().getJobLocation().isEmpty())? brokerResponse.getSearchBrokerListInfo().getJobLocation():"";
            String shortDescription=(brokerResponse.getPostContent()!=null && !brokerResponse.getPostContent().isEmpty())? brokerResponse.getPostContent():"Not Available";
            String avatar=(brokerResponse.getSearchBrokerListInfo()!=null && brokerResponse.getSearchBrokerListInfo().getJobLogo()!=null && !brokerResponse.getSearchBrokerListInfo().getJobLogo().isEmpty())?brokerResponse.getSearchBrokerListInfo().getJobLogo():"";
            String cover=(brokerResponse.getSearchBrokerListInfo()!=null && brokerResponse.getSearchBrokerListInfo().getJobCover()!=null && !brokerResponse.getSearchBrokerListInfo().getJobCover().isEmpty())?brokerResponse.getSearchBrokerListInfo().getJobCover():"";

            tvName.setText(name);
            tvAddress.setText(address);
            tvShorDescription.setText(shortDescription);
            if(avatar!=null && !avatar.isEmpty()){
                Picasso.with(CommonObjects.getContext()).load(avatar).placeholder(R.drawable.broker_avatar).error(R.drawable.broker_avatar).into(civAvatar);
            }else{
                Picasso.with(CommonObjects.getContext()).load(R.drawable.broker_avatar).into(civAvatar);
            }

            if(cover!=null && !cover.isEmpty()){
                Picasso.with(CommonObjects.getContext()).load(cover).placeholder(R.drawable.placeholder_image_sqr_new).error(R.drawable.placeholder_image_sqr_new).into(ivCover);
            }else{
                Picasso.with(CommonObjects.getContext()).load(R.drawable.placeholder_image_sqr_new).into(ivCover);
            }



            getComments();

            getBrokerType();
        }
    }


    void getComments(){
        showHideCommentSection(false);
        pbLoading.setVisibility(View.VISIBLE);


        AppHandler.getComments(brokerResponse.getId(), new AppHandler.CommentListener() {
            @Override
            public void onSuccess(List<CommentData> commentData) {
                pbLoading.setVisibility(View.GONE);
                if(commentData!=null && commentData.size()>0){
                    comments=new ArrayList<>();
                    comments.clear();
                    comments.addAll(commentData);
                    showHideCommentSection(true);

                    loadCommentsData();
                }else{
                    showHideCommentSection(false);
                }
            }

            @Override
            public void onError(String error) {
                pbLoading.setVisibility(View.GONE);
                showHideCommentSection(false);
                android.util.Log.e("Comments",""+error);
            }
        });

    }

    void showHideCommentSection(boolean isShow){
        if(isShow){
            llCommentSection.setVisibility(View.VISIBLE);
        }else{
            llCommentSection.setVisibility(View.GONE);
        }
    }

    void loadCommentsData(){
        rvListItems.setAdapter(new CommentsListAdapter(comments));
    }


    void getBrokerType(){
        if(brokerResponse.getSearchBrokerListInfo()!=null && brokerResponse.getSearchBrokerListInfo().getCase27ListingType()!=null && !brokerResponse.getSearchBrokerListInfo().getCase27ListingType().isEmpty()){
            showHideCategorySection(false);
            pbLoadingCategories.setVisibility(View.VISIBLE);

        AppHandler.getBrokerType(false,new AppHandler.GetBrokerTypeListener() {
            @Override
            public void onSuccess(ArrayList<BrokerTypeResponse> getBrokerTypeResponse) {
                pbLoadingCategories.setVisibility(View.GONE);

                for (int i = 0; i <getBrokerTypeResponse.size() ; i++) {
                    if(getBrokerTypeResponse.get(i).getPostName().equalsIgnoreCase(brokerResponse.getSearchBrokerListInfo().getCase27ListingType())){
                        brokerTypeResponse=getBrokerTypeResponse.get(i);
                        break;
                    }
                }

                if(brokerTypeResponse!=null){
                    loadBrokerTypeData();
                }else{
                    showHideCategorySection(false);
                }
            }

            @Override
            public void onError(String error) {
                pbLoadingCategories.setVisibility(View.GONE);
                showHideCategorySection(false);
                android.util.Log.e("Category",""+error);



            }
        });

        }else {
            showHideCategorySection(false);
        }
    }
    void loadBrokerTypeData(){
        showHideCategorySection(true);

        String en=brokerTypeResponse.getPostTitle();
        String ar=brokerTypeResponse.getPostTitle();
        if(brokerTypeResponse.getPostTitle()!=null && !brokerTypeResponse.getPostTitle().isEmpty() && brokerTypeResponse.getPostTitle().contains("/#")){

            try {
                en = brokerTypeResponse.getPostTitle().split("/#")[0];
                ar = brokerTypeResponse.getPostTitle().split("/#")[1];
            }catch (Exception e){
                e.printStackTrace();
            }
        }


        tvNameOfCategory.setText( LocaleHelper.isDefaultLanguage(MainActivity.instance())? ""+en:""+ar);//brokerTypeResponse.getPostTitle());

//        tvNameOfCategory.setText(""+brokerTypeResponse.getPostTitle());
    }

    void showHideCategorySection(boolean isShow){
        if(isShow){
            llCategorySection.setVisibility(View.VISIBLE);
        }else{
            llCategorySection.setVisibility(View.GONE);
        }
    }


    boolean checkValidationForAddComment(){
        if(userLoginResponse==null || userLoginResponse.getData()==null || userLoginResponse.getData().getId()==null || userLoginResponse.getData().getId().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please login to Post a Comment");
            return false;
        }else if(brokerResponse.getId()==null){
            CommonMethods.showMessage(mActivity,"Broker not found for Comment.");
            return false;
        }else if(etComment.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please enter text to comment");
            return false;
        }else{
            return true;
        }
    }

    void postComment(){
        AppHandler.addComment(brokerResponse.getId(), userLoginResponse.getId(), etComment.getText().toString(), new AppHandler.AddCommentListener() {
            @Override
            public void onSuccess(AddCommentData addcommentData) {
                if(comments==null){
                    comments=new ArrayList<>();
                }
                CommentData commentData=(new Gson().fromJson(new Gson().toJson(addcommentData),CommentData.class));
                commentData.setCommentDateGmt(CommonMethods.getDateFormattedWithForStandardFormat(addcommentData.getCommentDate()));
                commentData.setCommentDate(CommonMethods.getDateFormattedWithForStandardFormat(addcommentData.getCommentDate()));
                ArrayList<CommentData> commentsNew=new ArrayList<>();
                commentsNew.add(commentData);
                commentsNew.addAll(comments);
                comments.clear();
                comments.addAll(commentsNew);
//                getComments();
                loadCommentsData();
                resetCommentInputs();
            }

            @Override
            public void onError(String error) {
                CommonMethods.showMessage(mActivity,""+error);

            }
        });
    }

    void resetCommentInputs(){
        etComment.setText("");
    }


}
