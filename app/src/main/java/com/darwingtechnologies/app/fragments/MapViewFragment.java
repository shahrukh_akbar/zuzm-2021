package com.darwingtechnologies.app.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.darwingtechnologies.app.R;
import com.darwingtechnologies.app.activities.MainActivity;
import com.darwingtechnologies.app.business.models.searchBroker.BrokerResponse;
import com.darwingtechnologies.app.utilities.CommonMethods;
import com.darwingtechnologies.app.utilities.CommonObjects;
import com.darwingtechnologies.app.utilities.Constants;
import com.darwingtechnologies.app.utilities.DirectionsJSONParser;
import com.darwingtechnologies.app.utilities.GeoLocator;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class MapViewFragment extends BaseFragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private GoogleMap mGoogleMap;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    public Marker mCurrLocationMarker;
    private ArrayList<LatLng> latLngs;
    private LatLng latLng,draggedLatLng;
    private ArrayList<BrokerResponse> listItems;
    private LatLng destination;
    private LatLng driverLocation;
    private LatLng customerLocation;
    private boolean isLocationChangeStop;
    private boolean isDrag;


    private Context context;

    private LinearLayout llbottomArea;



    @Override
    protected void initView() {
        SupportMapFragment mapFragment = SupportMapFragment.newInstance();
        mapFragment.getMapAsync(this);
        getChildFragmentManager().beginTransaction()
                .add(R.id.map, mapFragment)
                .commit();
        context = getContext();

        llbottomArea=mView.findViewById(R.id.llbottomArea);
        llbottomArea.setVisibility(View.GONE);

    }

    @Override
    protected void loadData() {
        if(isDrag){
            llbottomArea.setVisibility(View.VISIBLE);
            mView.findViewById(R.id.btnDone).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((MainActivity) mActivity).checkForGPSOn()) {
                        if (draggedLatLng != null) {

                            MainActivity.latLng = new LatLng(draggedLatLng.latitude, draggedLatLng.longitude);

                            mActivity.getSupportFragmentManager().popBackStackImmediate();
                        }
                    }
                }
            });
            mView.findViewById(R.id.btnGoToCurrentLocation).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mGoogleMap.clear();
                    locationEnable(true);
                }
            });
        }else{
            llbottomArea.setVisibility(View.GONE);
        }
    }

    public static MapViewFragment newInstance(ArrayList<LatLng> latLng, ArrayList<BrokerResponse> listItems, Boolean isLocationChangeContinue, boolean isDrag) {
        MapViewFragment fragment = new MapViewFragment();
        fragment.latLngs = latLng;
        fragment.listItems=listItems;
        fragment.isLocationChangeStop = isLocationChangeContinue;
        fragment.isDrag = isDrag;
        return fragment;
    }

    public static MapViewFragment newInstance(LatLng latLng, Boolean isLocationChangeContinue, boolean isDrag) {
        MapViewFragment fragment = new MapViewFragment();
        fragment.latLng = latLng;
        fragment.isLocationChangeStop = isLocationChangeContinue;
        fragment.isDrag = isDrag;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.fragment_map_view, null);
    }

    @Override
    public String getFragmentTag() {
        return MapViewFragment.class.getSimpleName();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mGoogleMap = googleMap;
        mGoogleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                if(listItems!=null && listItems.size()>0){
                    for (int i = 0; i <listItems.size() ; i++) {
                        if(Double.parseDouble(listItems.get(i).getSearchBrokerListInfo().getGeolocationLat())==marker.getPosition().latitude && Double.parseDouble(listItems.get(i).getSearchBrokerListInfo().getGeolocationLong())==marker.getPosition().longitude){
                            CommonMethods.callFragment(BrokerProfileFragment.newInstance(listItems.get(i)), R.id.flFragmentOverlay, R.anim.fade_in, R.anim.fade_out, CommonObjects.getContext(), true);
                        }
                    }
                }

            }
        });
        mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(20.0f));
//        if (latLng == null) {
//            locationEnable(false);
//        } else {

//            GeoLocator geoLocator = new GeoLocator(mActivity);
//            LatLng latLng = new LatLng(geoLocator.getLattitude(), geoLocator.getLongitude());
//
//            setMarker(latLng, "", -1);
//        }

        if(latLngs!=null && latLngs.size()>0){
            for (int i = 0; i < latLngs.size() ; i++) {
                BrokerResponse brokerResponse=listItems.get(i);
                final String name=brokerResponse.getPostTitle()!=null?brokerResponse.getPostTitle():"";
                final String shortDetail=brokerResponse.getPostContent()!=null?brokerResponse.getPostContent():"";
                String avatar=(brokerResponse.getSearchBrokerListInfo()!=null && brokerResponse.getSearchBrokerListInfo().getJobLogo()!=null && !brokerResponse.getSearchBrokerListInfo().getJobLogo().isEmpty())?brokerResponse.getSearchBrokerListInfo().getJobLogo():"";

                String sourceString = shortDetail;


                setMarker(latLngs.get(i),name,sourceString,false,avatar);
            }
            locationEnable(isLocationChangeStop);

        }else if(latLng!=null){
            setMarker(latLng,"","",true,null);

        }else{
            locationEnable(isLocationChangeStop);

        }


    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
//        startUpdatingLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location location)
    {
        if(isLocationChangeStop)
        {
            if (mGoogleApiClient != null&&mLastLocation!=null&&mCurrLocationMarker!=null) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            }
        }
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

    }

    public static float getKmFromLatLong(float lat1, float lng1, float lat2, float lng2) {
        Location loc1 = new Location("");
        loc1.setLatitude(lat1);
        loc1.setLongitude(lng1);
        Location loc2 = new Location("");
        loc2.setLatitude(lat2);
        loc2.setLongitude(lng2);
        float distanceInMeters = loc1.distanceTo(loc2);
        return distanceInMeters / 1000;
    }


    void setMarker(LatLng latLng,String title,String snippet,boolean isDefault,String avatar){
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Bitmap bmp=null;
                try{
                    if(avatar!=null && !avatar.isEmpty()){
                        URL url = new URL(avatar);
                        bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                        if(bmp!=null){
                            Bitmap.createScaledBitmap(bmp, 25, 25, true);
                        }
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

                MarkerOptions markerOptions = new MarkerOptions();

                markerOptions.position(latLng);
                markerOptions.draggable(isDrag);
                if (!title.equals("")) {
                    markerOptions.title(title);
                }

                if (!snippet.equals("")) {
                    markerOptions.snippet(snippet);
                }

                if(isDefault){
                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                }else{
                    if(avatar!=null && !avatar.isEmpty() && bmp!=null){
                        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(getResizedBitmap(bmp,120,120)));//BitmapDescriptorFactory.fromBitmap(bmp));
                    }else{
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.person_marker));
                    }
                }

                if (mGoogleMap != null) {
                    if(isDrag){

                        mCurrLocationMarker = mGoogleMap.addMarker(markerOptions);
                        draggedLatLng=mCurrLocationMarker.getPosition();
                        mGoogleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                            @Override
                            public void onMarkerDragStart(Marker marker) {

                            }

                            @Override
                            public void onMarkerDrag(Marker marker) {
                                Log.i("System out", "onMarkerDrag...");

                            }

                            @Override
                            public void onMarkerDragEnd(Marker marker) {
                                Log.i("System out", "onMarkerDragEnd..."+marker.getPosition().latitude+"..."+marker.getPosition().longitude);
                                draggedLatLng=new LatLng(marker.getPosition().latitude,marker.getPosition().longitude);

                                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));

                            }
                        });

                    }else{
                        mGoogleMap.addMarker(markerOptions);
                    }

                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 20));
                }
            }
        });
    }


    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.ACCESS_FINE_LOCATION)) {
                CommonMethods.showMessage(mActivity, Constants.Messages.LOCATION_PERMISSION_REQUIRED);
            } else {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
//                    if (ContextCompat.checkSelfPermission(mActivity,
//                            Manifest.permission.ACCESS_FINE_LOCATION)
//                            == PackageManager.PERMISSION_GRANTED) {
//                        buildGoogleApiClient();
//                        mGoogleMap.setMyLocationEnabled(true);
//                        addMyMarker();
//                    }
                    locationEnable(true);

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    CommonMethods.showToast("permission denied", Toast.LENGTH_SHORT);
                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        //stop location updates when Activity is no longer active
         try {
             if (mGoogleApiClient != null) {

                 LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
             }
         }
         catch (Exception e) {

         }

    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            if (mGoogleApiClient != null) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            }
        }
       catch (Exception e){

       }
    }

    private void locationEnable(Boolean check) {

        if(check){
            //Initialize Google Play Services
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(mActivity,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    //Location Permission already granted
                    buildGoogleApiClient();
                    mGoogleMap.setMyLocationEnabled(check);
                    addMyMarker();
                } else {
                    //Request Location Permission
                    checkLocationPermission();
                }
            } else {
                buildGoogleApiClient();
                mGoogleMap.setMyLocationEnabled(check);
            }
        }else{
            buildGoogleApiClient();
            mGoogleMap.setMyLocationEnabled(check);
        }

    }

    public void setEnableLocation(Boolean check) {
        isLocationChangeStop = check;
        startUpdatingLocation();
    }

    public void setDisableLocation(Boolean check) {
        isLocationChangeStop = check;
    }

    private void startUpdatingLocation() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000 * 30);
        mLocationRequest.setFastestInterval(1000 * 2);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (mActivity != null) {
            if (ContextCompat.checkSelfPermission(mActivity,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                if (mGoogleApiClient != null)
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, MapViewFragment.this);
            }
        }
    }


    public void setOnMarkerClickListener(GoogleMap.OnMarkerClickListener onMarkerClickListener) {

    }

    private class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            String data = "";

            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);

        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList();
                lineOptions = new PolylineOptions();

                List<HashMap<String, String>> path = result.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                lineOptions.addAll(points);
                lineOptions.width(12);
                lineOptions.color(Color.RED);
                lineOptions.geodesic(true);

            }
            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null && mGoogleMap != null) {
                mGoogleMap.clear();
//                setMarker(driverLocation, "Driver", -1);
                if (customerLocation != null) {
//                    setMarker(customerLocation, "Pic up point", -1);
                } else if (destination != null) {
//                    setMarker(destination, "Drop off point", -1);
                }
                mGoogleMap.addPolyline(lineOptions);
            }
        }
    }


    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    public void drawRoute() {
        if (driverLocation != null) {
            // Start downloading json data from Google Directions API
//            setMarker(driverLocation, "Driver", -1);
            if (customerLocation != null) {
//                setMarker(customerLocation, "Pic up point", -1);
                new DownloadTask().execute(getDirectionsUrl(driverLocation, customerLocation));
            } else if (destination != null) {
//                setMarker(destination, "Drop off point", -1);
                new DownloadTask().execute(getDirectionsUrl(driverLocation, destination));
            }
        }
    }

    public void clearMap() {
        destination = null;
        customerLocation = null;
        driverLocation = null;
        mCurrLocationMarker=null;
    }


    void addMyMarker(){
        try{
            GeoLocator geoLocator = new GeoLocator(mActivity);
            LatLng latLng = new LatLng(geoLocator.getLattitude(), geoLocator.getLongitude());
            setMarker(latLng,"ME","",true,null);

        }catch (Exception e){
            e.printStackTrace();
        }

    }


    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }





}
