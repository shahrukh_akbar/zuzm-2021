package com.darwingtechnologies.app.fragments;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.darwingtechnologies.app.R;
import com.darwingtechnologies.app.activities.MainActivity;
import com.darwingtechnologies.app.adapters.CommentsListAdapter;
import com.darwingtechnologies.app.business.handlers.AppHandler;
import com.darwingtechnologies.app.business.models.Comment.CommentData;
import com.darwingtechnologies.app.business.models.Comment.addComment.AddCommentData;
import com.darwingtechnologies.app.business.models.getBrokerType.BrokerTypeResponse;
import com.darwingtechnologies.app.business.models.searchBroker.BrokerResponse;
import com.darwingtechnologies.app.business.models.updateBrokerProfile.UpdateBrokerProfileInput;
import com.darwingtechnologies.app.business.models.updateBrokerProfile.UpdateBrokerProfileResponse;
import com.darwingtechnologies.app.business.models.updateBrokerProfileImage.UpdateBrokerProfileImageResponse;
import com.darwingtechnologies.app.business.models.updateUserImage.UpdateUserImageInput;
import com.darwingtechnologies.app.business.models.updateUserImage.UpdateUserImageResponse;
import com.darwingtechnologies.app.business.models.userLogin.UserLoginResponse;
import com.darwingtechnologies.app.utilities.CommonMethods;
import com.darwingtechnologies.app.utilities.CommonObjects;
import com.darwingtechnologies.app.utilities.LocaleHelper;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.darwingtechnologies.app.activities.MainActivity.latLng;


/**
 * Created by Rana Zeshan on 27-Jun-19.
 */

public class ProfileFragmentNew extends BaseFragment{
    private ImageView ivUpdateAvatar,ivUpdateCover;
    private Uri imagesList ;


    private TextView tvName,tvAddress,tvShorDescription;
    private CircularImageView civAvatar;
    private ImageView ivCover;
    private LinearLayout llCommentSection,llCategorySection;
    private RecyclerView rvListItems;
    private ProgressBar pbLoading,pbLoadingCategories;

    private LinearLayout llCategory;
    private TextView tvNameOfCategory;

    private BrokerResponse brokerResponse;
    private BrokerTypeResponse brokerTypeResponse;
    private List<CommentData> comments;

    private ArrayList<BrokerTypeResponse> brokerTypeList = new ArrayList<>();


    private LinearLayout llGetDirection,llCall,llEmail,llWhatsapp;
    private TextView tvPostComment;
    private EditText etComment;


    private UserLoginResponse userLoginResponse;

    private TextView tvEditProfile,tvCancel;


    private Boolean isCover;
    private Boolean isProfile;

    private LinearLayout llEditProfile,llBrokerContent;

    private EditText etFullName,etEmail,etPhone,etWhatsappNumber,etTagLine,etDescription;

    private TextView tvSave;


    @Override
    protected void initView() {
        llEditProfile=mView.findViewById(R.id.llEditProfile);
        llBrokerContent=mView.findViewById(R.id.llBrokerContent);

        etFullName=mView.findViewById(R.id.etFullName);
        etEmail=mView.findViewById(R.id.etEmail);
        etPhone=mView.findViewById(R.id.etPhone);
        etWhatsappNumber=mView.findViewById(R.id.etWhatsappNumber);
        etTagLine=mView.findViewById(R.id.etTagLine);
        etDescription=mView.findViewById(R.id.etDescription);
        tvSave=mView.findViewById(R.id.tvSave);
        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkValidation()){
                    tvSave.setEnabled(false);

                    updateBrokerProfile();
                }
            }
        });
        addListnerTextChange();

        tvCancel=mView.findViewById(R.id.tvCancel);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showHideEditContentData(false);
            }
        });


        ivUpdateAvatar = mView.findViewById(R.id.ivUpdateAvatar);
        ivUpdateCover = mView.findViewById(R.id.ivUpdateCover);
        ivUpdateAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkForStoragePermission();
                isProfile = true;
                isCover = false;

            }
        });
        ivUpdateCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkForStoragePermission();
                isProfile = false;
                isCover = true;

            }
        });
        civAvatar = mView.findViewById(R.id.civAvatar);
        ivCover=mView.findViewById(R.id.ivCover);
        tvName=mView.findViewById(R.id.tvName);
        tvAddress=mView.findViewById(R.id.tvAddress);
        tvShorDescription=mView.findViewById(R.id.tvShorDescription);
        tvEditProfile=mView.findViewById(R.id.tvEditProfile);
        tvEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showHideEditContentData(true);

            }
        });

        llGetDirection=mView.findViewById(R.id.llGetDirection);
        llGetDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(brokerResponse.getSearchBrokerListInfo()!=null && brokerResponse.getSearchBrokerListInfo().getGeolocationLat()!=null && !brokerResponse.getSearchBrokerListInfo().getGeolocationLat().isEmpty() && brokerResponse.getSearchBrokerListInfo().getGeolocationLong()!=null && !brokerResponse.getSearchBrokerListInfo().getGeolocationLong().isEmpty()){

                    CommonMethods.getDirectionFromLatLng(mActivity,new LatLng(Double.parseDouble(brokerResponse.getSearchBrokerListInfo().getGeolocationLat()),Double.parseDouble(brokerResponse.getSearchBrokerListInfo().getGeolocationLong())));


//                    Uri gmmIntentUri = Uri.parse("geo:"+brokerResponse.getSearchBrokerListInfo().getGeolocationLat()+","+brokerResponse.getSearchBrokerListInfo().getGeolocationLong());
//                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//                    mapIntent.setPackage("com.google.android.apps.maps");
//                    if (mapIntent.resolveActivity(mActivity.getPackageManager()) != null) {
//                        startActivity(mapIntent);
//                    }else{
//                        CommonMethods.showMessage(mActivity,"Look like you don't have maps application to view");
//                    }
                }else{
                    CommonMethods.showMessage(mActivity,"We are unable to get direction for selected user.");
                }
            }
        });

        llCall=mView.findViewById(R.id.llCall);
        llCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(brokerResponse.getSearchBrokerListInfo()!=null && brokerResponse.getSearchBrokerListInfo().getJobPhone()!=null && !brokerResponse.getSearchBrokerListInfo().getJobPhone().isEmpty()){
//                    Intent intent = new Intent(Intent.ACTION_VIEW);
//                    intent.setData(Uri.parse("tel:" + brokerResponse.getSearchBrokerListInfo().getJobPhone()));
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent);
                    displayMessageAndEditProfile(brokerResponse.getSearchBrokerListInfo().getJobPhone());
                }else{
                    displayMessageAndEditProfile("You don't have entered any phone number.");
                }
            }
        });

        llEmail=mView.findViewById(R.id.llEmail);
        llEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(brokerResponse.getSearchBrokerListInfo()!=null && brokerResponse.getSearchBrokerListInfo().getJobEmail()!=null && !brokerResponse.getSearchBrokerListInfo().getJobEmail().isEmpty()){

                    displayMessageAndEditProfile(brokerResponse.getSearchBrokerListInfo().getJobEmail());
                }else{
                    displayMessageAndEditProfile("You don't have entered an email address.");
                }
            }
        });

        llWhatsapp=mView.findViewById(R.id.llWhatsapp);
        llWhatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(brokerResponse.getSearchBrokerListInfo()!=null && brokerResponse.getSearchBrokerListInfo().getWhatsappNumber()!=null && !brokerResponse.getSearchBrokerListInfo().getWhatsappNumber().toString().isEmpty()){
                    displayMessageAndEditProfile(brokerResponse.getSearchBrokerListInfo().getWhatsappNumber().toString());

                }else{
                    displayMessageAndEditProfile("You don't have entered any Whatsapp number.");
                }
            }
        });







        llCommentSection=mView.findViewById(R.id.llCommentSection);
        rvListItems=mView.findViewById(R.id.rvListItems);
        rvListItems.setLayoutManager(new LinearLayoutManager(mActivity,LinearLayoutManager.VERTICAL,false));
        pbLoading=mView.findViewById(R.id.pbLoading);
        showHideCommentSection(false);

        etComment=mView.findViewById(R.id.etComment);
        tvPostComment=mView.findViewById(R.id.tvPostComment);
        tvPostComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkValidationForAddComment()){
                    postComment();
                }
            }
        });


        llCategorySection=mView.findViewById(R.id.llCategorySection);
        pbLoadingCategories=mView.findViewById(R.id.pbLoadingCategories);
        tvNameOfCategory=mView.findViewById(R.id.tvNameOfCategory);
        llCategory=mView.findViewById(R.id.llCategory);
        showHideCategorySection(false);


        CommonMethods.setupUI(mView,mActivity);
    }

    @Override
    protected void loadData() {

        setBrokerData();

        userLoginResponse=CommonMethods.getUserDetail(mActivity);
        if(userLoginResponse!=null){

        }


        showHideEditContentData(false);

    }

    public static ProfileFragmentNew newInstance(BrokerResponse brokerResponse) {
        ProfileFragmentNew fragment = new ProfileFragmentNew();
        fragment.brokerResponse=brokerResponse;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.profile_fragment_new, null);
    }

    @Override
    public String getFragmentTag() {
        return BrokerProfileFragment.class.getSimpleName();
    }

    void setBrokerData(){
        if(brokerResponse!=null){
            String name=brokerResponse.getPostTitle()!=null?brokerResponse.getPostTitle():"";
            String address=(brokerResponse.getSearchBrokerListInfo()!=null && brokerResponse.getSearchBrokerListInfo().getJobLocation()!=null && !brokerResponse.getSearchBrokerListInfo().getJobLocation().isEmpty())? brokerResponse.getSearchBrokerListInfo().getJobLocation():"";
            String shortDescription=(brokerResponse.getPostContent()!=null && !brokerResponse.getPostContent().isEmpty())? brokerResponse.getPostContent():"Not Available";
            String avatar=(brokerResponse.getSearchBrokerListInfo()!=null && brokerResponse.getSearchBrokerListInfo().getJobLogo()!=null && !brokerResponse.getSearchBrokerListInfo().getJobLogo().isEmpty())?brokerResponse.getSearchBrokerListInfo().getJobLogo():"";
            String cover=(brokerResponse.getSearchBrokerListInfo()!=null && brokerResponse.getSearchBrokerListInfo().getJobCover()!=null && !brokerResponse.getSearchBrokerListInfo().getJobCover().isEmpty())?brokerResponse.getSearchBrokerListInfo().getJobCover():"";

            tvName.setText(name);
            tvAddress.setText(address);
            tvShorDescription.setText(shortDescription);
            if(avatar!=null && !avatar.isEmpty()){
                Picasso.with(CommonObjects.getContext()).load(avatar).placeholder(R.drawable.broker_avatar).error(R.drawable.broker_avatar).into(civAvatar);
            }else{
                Picasso.with(CommonObjects.getContext()).load(R.drawable.broker_avatar).into(civAvatar);
            }

            if(cover!=null && !cover.isEmpty()){
                Picasso.with(CommonObjects.getContext()).load(cover).placeholder(R.drawable.placeholder_image_sqr_new).error(R.drawable.placeholder_image_sqr_new).into(ivCover);
            }else{
                Picasso.with(CommonObjects.getContext()).load(R.drawable.placeholder_image_sqr_new).into(ivCover);
            }


            etFullName.setText(name);
            etEmail.setText(""+brokerResponse.getSearchBrokerListInfo().getJobEmail());
            etPhone.setText(""+brokerResponse.getSearchBrokerListInfo().getJobPhone());
            etWhatsappNumber.setText(""+brokerResponse.getSearchBrokerListInfo().getWhatsappNumber());
            etTagLine.setText(""+brokerResponse.getSearchBrokerListInfo().getJobTagline());
            etDescription.setText(""+shortDescription);



            getComments();

            getBrokerType();

            tvSave.setEnabled(false);

        }
    }


    void getComments(){
        showHideCommentSection(false);
        pbLoading.setVisibility(View.VISIBLE);


        AppHandler.getComments(brokerResponse.getId(), new AppHandler.CommentListener() {
            @Override
            public void onSuccess(List<CommentData> commentData) {
                pbLoading.setVisibility(View.GONE);
                if(commentData!=null && commentData.size()>0){
                    comments=new ArrayList<>();
                    comments.clear();
                    comments.addAll(commentData);
                    showHideCommentSection(true);

                    loadCommentsData();
                }else{
                    showHideCommentSection(false);
                }
            }

            @Override
            public void onError(String error) {
                pbLoading.setVisibility(View.GONE);
                showHideCommentSection(false);
                android.util.Log.e("Comments",""+error);
            }
        });

    }

    void showHideCommentSection(boolean isShow){
        if(isShow){
            llCommentSection.setVisibility(View.VISIBLE);
        }else{
            llCommentSection.setVisibility(View.GONE);
        }
    }

    void loadCommentsData(){
        rvListItems.setAdapter(new CommentsListAdapter(comments));
    }


    void getBrokerType(){
        if(brokerResponse.getSearchBrokerListInfo()!=null && brokerResponse.getSearchBrokerListInfo().getCase27ListingType()!=null && !brokerResponse.getSearchBrokerListInfo().getCase27ListingType().isEmpty()){
            showHideCategorySection(false);
            pbLoadingCategories.setVisibility(View.VISIBLE);

            AppHandler.getBrokerType(false,new AppHandler.GetBrokerTypeListener() {
                @Override
                public void onSuccess(ArrayList<BrokerTypeResponse> getBrokerTypeResponse) {
                    pbLoadingCategories.setVisibility(View.GONE);

                    for (int i = 0; i <getBrokerTypeResponse.size() ; i++) {
                        if(getBrokerTypeResponse.get(i).getPostName().equalsIgnoreCase(brokerResponse.getSearchBrokerListInfo().getCase27ListingType())){
                            brokerTypeResponse=getBrokerTypeResponse.get(i);
                            break;
                        }
                    }

                    if(brokerTypeResponse!=null){
                        loadBrokerTypeData();
                    }else{
                        showHideCategorySection(false);
                    }
                }

                @Override
                public void onError(String error) {
                    pbLoadingCategories.setVisibility(View.GONE);
                    showHideCategorySection(false);
                    android.util.Log.e("Category",""+error);



                }
            });

        }else {
            showHideCategorySection(false);
        }
    }
    void loadBrokerTypeData(){
        showHideCategorySection(true);

        String en=brokerTypeResponse.getPostTitle();
        String ar=brokerTypeResponse.getPostTitle();
        if(brokerTypeResponse.getPostTitle()!=null && !brokerTypeResponse.getPostTitle().isEmpty() && brokerTypeResponse.getPostTitle().contains("/#")){

            try {
                en = brokerTypeResponse.getPostTitle().split("/#")[0];
                ar = brokerTypeResponse.getPostTitle().split("/#")[1];
            }catch (Exception e){
                e.printStackTrace();
            }
        }


        tvNameOfCategory.setText( LocaleHelper.isDefaultLanguage(MainActivity.instance())? ""+en:""+ar);//brokerTypeResponse.getPostTitle());
    }

    void showHideCategorySection(boolean isShow){
        if(isShow){
            llCategorySection.setVisibility(View.VISIBLE);
        }else{
            llCategorySection.setVisibility(View.GONE);
        }
    }


    boolean checkValidationForAddComment(){
        if(userLoginResponse==null || userLoginResponse.getData()==null || userLoginResponse.getData().getId()==null || userLoginResponse.getData().getId().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please login to Post a Comment");
            return false;
        }else if(brokerResponse.getId()==null){
            CommonMethods.showMessage(mActivity,"Broker not found for Comment.");
            return false;
        }else if(etComment.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please enter text to comment");
            return false;
        }else{
            return true;
        }
    }

    void postComment(){
        AppHandler.addComment(brokerResponse.getId(), userLoginResponse.getId(), etComment.getText().toString(), new AppHandler.AddCommentListener() {
            @Override
            public void onSuccess(AddCommentData addcommentData) {
                if(comments==null){
                    comments=new ArrayList<>();
                }
                comments.add(new Gson().fromJson(new Gson().toJson(addcommentData),CommentData.class));
//                getComments();
                loadCommentsData();
                resetCommentInputs();
            }

            @Override
            public void onError(String error) {
                CommonMethods.showMessage(mActivity,""+error);

            }
        });
    }

    void resetCommentInputs(){
        etComment.setText("");
    }



    void displayMessageAndEditProfile(String message){
        CommonMethods.displayMessagesDialog(""+message,mActivity).setPositiveButton("Edit Profile", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                tvEditProfile.performClick();
            }
        }).setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();

    }

    private void checkForStoragePermission() {
        if(ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){

            String[] permissionsToask=new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA};

            CommonMethods.showPermissionMessage(mActivity,"To Upload Profile Image, allow "+getString(R.string.app_name)+" to access your device Storage & Camera.",permissionsToask);

        }else {
            openGalleryToShare();
        }
    }

    void openGalleryToShare(){
        CropImage.activity().start(mActivity, ProfileFragmentNew.this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                if (isCover) {
                    updateProfileImage(resultUri);
                }

                if (isProfile) {
                    updateProfileImage(resultUri);
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }


    void updateProfileImage(Uri uri) {

        String path = CommonMethods.compressImage(uri.toString(), "Media/Images/Upload"); // uri.getPath();
        if (path.contains("file://")) {
            path = path.split("file:///", 2)[1];
        } else if (path.contains("content://")) {
            path =
                    CommonMethods.getFilePath(
                            CommonObjects.getContext(),
                            Uri.parse(path));
        }
        if (path != null && path.contains("%20")) {
            path = path.replace("%20", "-");
        }



        imagesList=uri;
        if(isCover){
            ivCover.setImageURI(imagesList);
        }else if(isProfile){
            civAvatar.setImageURI(imagesList);
        }

        if (imagesList!=null){
            updateImage();
        }
    }

    void updateImage() {
        ArrayList<File> images = new ArrayList<>();
        if (imagesList!=null){
            Uri uri = imagesList;
            String path =
                    CommonMethods.compressImage(
                            uri.toString(),
                            "Media/Images/Upload"); // uri.getPath();

            if (path.contains("file://")) {
                path = path.split("file:///", 2)[1];
            } else if (path.contains("content://")) {
                path =
                        CommonMethods.getFilePath(
                                CommonObjects.getContext(),
                                Uri.parse(path));
            }
            if (path != null && path.contains("%20")) {
                path = path.replace("%20", "-");
            }

            File file = new File(path);
            images.add(file);


            UpdateUserImageInput updateProfileImageInput = new UpdateUserImageInput();
            if (userLoginResponse!=null){
                updateProfileImageInput.setUserID(String.valueOf(userLoginResponse.getId()));
                updateProfileImageInput.setProfileId(String.valueOf(brokerResponse.getId()));
            }else {
                return;
            }
            if(isProfile){
                AppHandler.updateBrokerProfileImage(images,updateProfileImageInput, new AppHandler.UpdateBrokerImageListener() {
                    @Override
                    public void onSuccess(UpdateBrokerProfileImageResponse updateUserImageResponse) {
                        CommonMethods.showMessage(mActivity,"Image uploaded Successfully");

                        if(updateUserImageResponse.getData()!=null && updateUserImageResponse.getData().getJobLogo()!=null && !updateUserImageResponse.getData().getJobLogo().isEmpty()){
                            userLoginResponse.getData().getBrokerInfo().get(0).getSearchBrokerListInfo().setJobLogo(updateUserImageResponse.getData().getJobLogo());
                            CommonMethods.setStringPreference(
                                    mActivity,
                                    mActivity.getString(R.string.app_name),
                                    UserLoginResponse.class.getSimpleName(),
                                    new Gson().toJson(userLoginResponse));

                            MainActivity.instance().getLoginDetails();
                            Picasso.with(mActivity).load(updateUserImageResponse.getData().getJobLogo()).placeholder(R.drawable.avatar_sqr).error(R.drawable.avatar_sqr).into(civAvatar);
                        }





                        CommonMethods.hideProgressDialog();
                    }

                    @Override
                    public void onError(String error) {
                        CommonMethods.showMessage(mActivity,""+error);
                        CommonMethods.hideProgressDialog();
                    }
                });

            }else if(isCover){
                AppHandler.updateBrokerProfileCoverImage(images,updateProfileImageInput, new AppHandler.UpdateBrokerImageListener() {
                    @Override
                    public void onSuccess(UpdateBrokerProfileImageResponse updateUserImageResponse) {
                        CommonMethods.showMessage(mActivity,"Image uploaded Successfully");

                        if(updateUserImageResponse.getData()!=null && updateUserImageResponse.getData().getJobCover()!=null && !updateUserImageResponse.getData().getJobCover().isEmpty()){
                            userLoginResponse.getData().getBrokerInfo().get(0).getSearchBrokerListInfo().setJobCover(updateUserImageResponse.getData().getJobCover());
                            CommonMethods.setStringPreference(
                                    mActivity,
                                    mActivity.getString(R.string.app_name),
                                    UserLoginResponse.class.getSimpleName(),
                                    new Gson().toJson(userLoginResponse));

                            MainActivity.instance().getLoginDetails();
                            Picasso.with(mActivity).load(updateUserImageResponse.getData().getJobCover()).placeholder(R.drawable.placeholder_image_sqr_new).error(R.drawable.placeholder_image_sqr_new).into(ivCover);
                        }





                        CommonMethods.hideProgressDialog();
                    }

                    @Override
                    public void onError(String error) {
                        CommonMethods.showMessage(mActivity,""+error);
                        CommonMethods.hideProgressDialog();
                    }
                });

            }
        }
    }

    void showHideEditContentData(boolean isEditable){
        if(isEditable){
            llEditProfile.setVisibility(View.VISIBLE);
            llBrokerContent.setVisibility(View.GONE);
            tvEditProfile.setVisibility(View.GONE);
        }else{
            llEditProfile.setVisibility(View.GONE);
            llBrokerContent.setVisibility(View.VISIBLE);
            tvEditProfile.setVisibility(View.VISIBLE);
        }
    }


    boolean checkValidation(){
        if(etFullName.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please Enter Your Full Name.");
            return false;
        }else if(etEmail.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please Enter Your Email.");
            return false;
        }else if(etEmail.getText().toString().contains("@") && !CommonMethods.isEmailValid(etEmail.getText().toString())){
            CommonMethods.showMessage(mActivity,"Please enter Valid Email.");
            return false;
        }else{
            return checkBrokerProfileValidation();
        }


    }


    boolean checkBrokerProfileValidation(){

        if(etPhone.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please enter your Phone to continue");
            return false;
        }else if(etWhatsappNumber.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please enter your Whatsapp Number to continue");
            return false;
        }else if(etTagLine.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please enter your Tagline to continue");
            return false;
        }else if(etDescription.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please enter Description to continue");
            return false;
        }else{
            return true;
        }

    }

    void addListnerTextChange(){
        TextWatcher textWatcher=new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tvSave.setEnabled(true);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };

        etFullName.addTextChangedListener(textWatcher);
        etEmail.addTextChangedListener(textWatcher);
        etPhone.addTextChangedListener(textWatcher);
        etWhatsappNumber.addTextChangedListener(textWatcher);
        etTagLine.addTextChangedListener(textWatcher);
        etDescription.addTextChangedListener(textWatcher);

    }


    void updateBrokerProfile(){

        UpdateBrokerProfileInput updateBrokerProfileInput=new UpdateBrokerProfileInput();
        updateBrokerProfileInput.setUserId(""+userLoginResponse.getId());
        updateBrokerProfileInput.setProfileId(""+userLoginResponse.getData().getBrokerInfo().get(0).getId());
        updateBrokerProfileInput.setPostTitle(""+etFullName.getText().toString());
        updateBrokerProfileInput.setPostContent(""+etDescription.getText().toString());
        updateBrokerProfileInput.setEmail(""+etEmail.getText().toString());
        updateBrokerProfileInput.setPhone(""+etPhone.getText().toString());
        updateBrokerProfileInput.setWhatsappNumber(""+etWhatsappNumber.getText().toString());
        updateBrokerProfileInput.setTagline(""+etTagLine.getText().toString());

        AppHandler.updateBrokerProfile(updateBrokerProfileInput, new AppHandler.UpdateBrokerProfileListener() {
            @Override
            public void onSuccess(UpdateBrokerProfileResponse updateBrokerProfileResponse) {
                CommonMethods.showToast("Profile Updated Successfully...!!!", Toast.LENGTH_LONG);
                userLoginResponse.getData().getBrokerInfo().get(0).setPostTitle(etFullName.getText().toString());
                userLoginResponse.getData().getBrokerInfo().get(0).getSearchBrokerListInfo().setJobEmail(etEmail.getText().toString());
                userLoginResponse.getData().getBrokerInfo().get(0).getSearchBrokerListInfo().setJobPhone(etPhone.getText().toString());
                userLoginResponse.getData().getBrokerInfo().get(0).getSearchBrokerListInfo().setWhatsappNumber(etWhatsappNumber.getText().toString());
                userLoginResponse.getData().getBrokerInfo().get(0).getSearchBrokerListInfo().setJobTagline(etTagLine.getText().toString());
                userLoginResponse.getData().getBrokerInfo().get(0).setPostContent(etDescription.getText().toString());

                tvName.setText(etFullName.getText().toString());

                CommonMethods.setStringPreference(
                        mActivity,
                        mActivity.getString(R.string.app_name),
                        UserLoginResponse.class.getSimpleName(),
                        new Gson().toJson(userLoginResponse));

                MainActivity.instance().getLoginDetails();

                showHideEditContentData(false);

            }

            @Override
            public void onError(String error) {
                CommonMethods.showMessage(mActivity,""+error);
                tvSave.setEnabled(true);

            }
        });

    }



}
