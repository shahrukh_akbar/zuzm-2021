package com.darwingtechnologies.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.darwingtechnologies.app.R;
import com.darwingtechnologies.app.business.handlers.AppHandler;
import com.darwingtechnologies.app.business.models.forgotPassword.ForgotPasswordResponse;
import com.darwingtechnologies.app.utilities.CommonMethods;


public class ResetPasswordFragment extends BaseFragment  {

    private EditText etEmail;
    private TextView tvGetALink;
    private ImageButton ibBack;

    @Override
    protected void initView() {

        etEmail = mView.findViewById(R.id.etEmail);
        tvGetALink = mView.findViewById(R.id.tvGetALink);
        ibBack = mView.findViewById(R.id.ibBack);
        ibBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.getSupportFragmentManager().popBackStackImmediate();
            }
        });
        tvGetALink.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if(checkFieldsAreFill()){
                    AppHandler.getResetPasswordLink(etEmail.getText().toString(), new AppHandler.ForgotPasswordListener() {
                        @Override
                        public void onSuccess(ForgotPasswordResponse verificationResponse) {
                            String message = "Password reset link has been sent to your registered email.!";
                            if (verificationResponse.getMsg() != null && !verificationResponse.getMsg().isEmpty()) {
                                message = verificationResponse.getMsg();
                            }
                            CommonMethods.showMessage(mActivity,message);

                            ibBack.performClick();
                        }

                        @Override
                        public void onError(String error) {
                            CommonMethods.showMessage(mActivity, "" + error);
                        }

                    });
                }

            }});

    }

    @Override
    protected void loadData() {
    }

    public static ResetPasswordFragment newInstance() {
        ResetPasswordFragment fragment = new ResetPasswordFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.fragment_reset_password, null);

    }


    @Override
    public String getFragmentTag() {
        return ResetPasswordFragment.class.getSimpleName();
    }

    public boolean checkFieldsAreFill() {

        if (etEmail.getText().toString().equals("")||etEmail.getText().toString().isEmpty()) {
            CommonMethods.showMessage(mActivity,"please enter email.");
            return false;
        }
        else  if (!CommonMethods.isEmailValid(etEmail.getText().toString())) {
            CommonMethods.showMessage(mActivity,"please enter valid email.");
            return false;
        }else{
            return true;
        }
    }
}
