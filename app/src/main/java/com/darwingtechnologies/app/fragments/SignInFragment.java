package com.darwingtechnologies.app.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.darwingtechnologies.app.BuildConfig;
import com.darwingtechnologies.app.R;
import com.darwingtechnologies.app.activities.MainActivity;
import com.darwingtechnologies.app.business.handlers.AppHandler;
import com.darwingtechnologies.app.business.models.userLogin.UserLoginInput;
import com.darwingtechnologies.app.business.models.userLogin.UserLoginResponse;
import com.darwingtechnologies.app.utilities.CommonMethods;
import com.darwingtechnologies.app.utilities.CommonObjects;
import com.google.gson.Gson;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;


/**
 * Created by Rana Zeshan on 27-Jun-19.
 */

public class SignInFragment extends BaseFragment{
    private EditText etEmail,etPassword;
    private CircularProgressButton btnSubmit;
    private String email,password;
    private TextView tvForgotPassword,tvSignIn,tvRegister;


    @Override
    protected void initView() {

        tvSignIn=mView.findViewById(R.id.tvSignIn);
        tvForgotPassword=mView.findViewById(R.id.tvForgotPassword);
        tvRegister=mView.findViewById(R.id.tvRegister);
        etEmail=mView.findViewById(R.id.etEmail);
        etPassword=mView.findViewById(R.id.etPassword);

        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethods.callFragment(SignupAsFragment.newInstance(), R.id.flFragmentContainer, R.anim.fade_in, R.anim.fade_out, mActivity, true);
            }
        });

        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethods.callFragment(ResetPasswordFragment.newInstance(), R.id.flFragmentContainer, R.anim.fade_in, R.anim.fade_out, mActivity, true);
            }
        });
        tvSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkAvailability()) {
                    goNextScreen();
                }
            }
        });
        if(BuildConfig.DEBUG){

            //franchise testing login
            etEmail.setText("ranazeshi420@gmail.com");
            etPassword.setText("1234");

        }

        CommonMethods.setupUI(mView,mActivity);
    }

    @Override
    protected void loadData() {

    }

    public static SignInFragment newInstance() {
        SignInFragment fragment = new SignInFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.sign_in_fragment, null);
    }

    @Override
    public String getFragmentTag() {
        return SignInFragment.class.getSimpleName();
    }


    boolean checkAvailability(){
        if(etEmail.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please Enter Email or Phone Number.");
            return false;
        }else if(etEmail.getText().toString().contains("@") && !CommonMethods.isEmailValid(etEmail.getText().toString())){
            CommonMethods.showMessage(mActivity,"Please enter Valid Email.");
            return false;
        } else if(etPassword.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please Enter Password.");
            return false;
        } else{
            return true;
        }
    }

    public void goNextScreen(){
        UserLoginInput userLoginInput=new UserLoginInput();
        userLoginInput.setEmail(etEmail.getText().toString());
        userLoginInput.setPassword(etPassword.getText().toString());

        AppHandler.userLogin(userLoginInput, new AppHandler.UserLoginListener() {
            @Override
            public void onSuccess(UserLoginResponse userLoginResponse) {
                CommonMethods.hideProgressDialog();
                if (userLoginResponse!=null&&userLoginResponse.getId()!=null){
                    CommonMethods.setStringPreference(
                            mActivity,
                            mActivity.getString(R.string.app_name),
                            UserLoginResponse.class.getSimpleName(),
                            new Gson().toJson(userLoginResponse));

                    startActivity(new Intent(CommonObjects.getContext(), MainActivity.class));
                    ((FragmentActivity) CommonObjects.getContext()).finishAffinity();

//                    CommonMethods.callFragment(HomeScreenFragment.newInstance(), R.id.flFragmentContainer, R.anim.fade_in, R.anim.fade_out, mActivity, false);
                }else {
                    CommonMethods.callFragment(SplashFragment.newInstance(), R.id.flFragmentContainer, R.anim.fade_in, R.anim.fade_out, mActivity, false);

                }
            }

            @Override
            public void onError(String error) {
                CommonMethods.hideProgressDialog();
                CommonMethods.showMessage(mActivity,error);
            }
        });

    }

}
