package com.darwingtechnologies.app.fragments;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.core.app.ActivityCompat;

import com.darwingtechnologies.app.R;
import com.darwingtechnologies.app.activities.MainActivity;
import com.darwingtechnologies.app.business.handlers.AppHandler;
import com.darwingtechnologies.app.business.models.updateUserImage.UpdateUserImageInput;
import com.darwingtechnologies.app.business.models.updateUserImage.UpdateUserImageResponse;
import com.darwingtechnologies.app.business.models.updateUserProfile.UpdateUserProfileInput;
import com.darwingtechnologies.app.business.models.updateUserProfile.UpdateUserProfileResponse;
import com.darwingtechnologies.app.business.models.userLogin.UserLoginResponse;
import com.darwingtechnologies.app.utilities.CommonMethods;
import com.darwingtechnologies.app.utilities.CommonObjects;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;


/**
 * Created by Rana Zeshan on 27-Jun-19.
 */

public class ProfileFragment extends BaseFragment{
    private CircularImageView civAvatar;
    ImageView ivUpdateAvatar;
    private Uri imagesList ;
    private ImageView ivCover;

    private boolean createPostEnable;
    private EditText etFirstName, etLastName, etPhone, etEmail,etPassword,etAddress;
    private TextView tvSave,tvName,tvAddress;
    private UserLoginResponse userLoginResponse;

    private TextView tvBecomeBroker;

    private Boolean isCover;
    private Boolean isProfile;


    @Override
    protected void initView() {
        ivUpdateAvatar = mView.findViewById(R.id.ivUpdateAvatar);
        civAvatar = mView.findViewById(R.id.civAvatar);
        ivCover=mView.findViewById(R.id.ivCover);
        etFirstName = mView.findViewById(R.id.etFirstName);
        etLastName = mView.findViewById(R.id.etLastName);
        etPhone = mView.findViewById(R.id.etPhone);
        etEmail = mView.findViewById(R.id.etEmail);
        tvSave = mView.findViewById(R.id.tvSave);
        tvName = mView.findViewById(R.id.tvName);
        tvAddress=mView.findViewById(R.id.tvAddress);
        etPassword = mView.findViewById(R.id.etPassword);
        etAddress = mView.findViewById(R.id.etAddress);


        etFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tvSave.setEnabled(true);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tvSave.setEnabled(true);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tvSave.setEnabled(true);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tvSave.setEnabled(true);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tvSave.setEnabled(true);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tvSave.setEnabled(true);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkAvailability()){
                    userDataUpdate();
                }
            }
        });


        ivUpdateAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkForStoragePermission();
                isProfile = true;
                isCover = false;

            }
        });


        tvBecomeBroker=mView.findViewById(R.id.tvBecomeBroker);
        tvBecomeBroker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(MainActivity.userLoginResponse==null){
                    CommonMethods.displayMessagesDialog("Please login to continue",mActivity).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            getIbBack().performClick();
                            dialogInterface.dismiss();
                        }
                    }).setCancelable(false).create().show();
                }else{
                    CommonMethods.callFragment(BecomeBrokerFragment.newInstance(), R.id.flFragmentOverlay, R.anim.fade_in, R.anim.fade_out, CommonObjects.getContext(), true);
                }
            }
        });

        CommonMethods.setupUI(mView,mActivity);
    }

    @Override
    protected void loadData() {
        userLoginResponse=MainActivity.userLoginResponse;
        displayuserData();
        MainActivity.instance().showBottomBar();

    }

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.profile_fragment, null);
    }

    @Override
    public String getFragmentTag() {
        return ProfileFragment.class.getSimpleName();
    }


    void updateImage() {
        ArrayList<File> images = new ArrayList<>();
        if (imagesList!=null){
            Uri uri = imagesList;
            String path =
                    CommonMethods.compressImage(
                            uri.toString(),
                            "Media/Images/Upload"); // uri.getPath();

            if (path.contains("file://")) {
                path = path.split("file:///", 2)[1];
            } else if (path.contains("content://")) {
                path =
                        CommonMethods.getFilePath(
                                CommonObjects.getContext(),
                                Uri.parse(path));
            }
            if (path != null && path.contains("%20")) {
                path = path.replace("%20", "-");
            }

            File file = new File(path);
            images.add(file);



            if (userLoginResponse==null){
                return;
            }
            if(isProfile){
                AppHandler.updateProfileImage(images,String.valueOf(userLoginResponse.getId()), new AppHandler.UpdateUserImageListener() {
                    @Override
                    public void onSuccess(UpdateUserImageResponse updateUserImageResponse) {
                        CommonMethods.showMessage(mActivity,"Image uploaded Successfully");
//                            profileData();

                        userLoginResponse.getData().getUserInfo().setAvatar(updateUserImageResponse.getAvatar());

                        CommonMethods.setStringPreference(
                                mActivity,
                                mActivity.getString(R.string.app_name),
                                UserLoginResponse.class.getSimpleName(),
                                new Gson().toJson(userLoginResponse));

                        Picasso.with(mActivity).load(updateUserImageResponse.getAvatar()).placeholder(R.drawable.avatar_sqr).error(R.drawable.avatar_sqr).into(civAvatar);

                        MainActivity.instance().getLoginDetails();

                        CommonMethods.hideProgressDialog();
                    }

                    @Override
                    public void onError(String error) {
                        CommonMethods.showMessage(mActivity,""+error);
                        CommonMethods.hideProgressDialog();
                    }
                });

            }else if(isCover){

            }
        }
    }

    void updateProfileImage(Uri uri) {

        String path = CommonMethods.compressImage(uri.toString(), "Media/Images/Upload"); // uri.getPath();
        if (path.contains("file://")) {
            path = path.split("file:///", 2)[1];
        } else if (path.contains("content://")) {
            path =
                    CommonMethods.getFilePath(
                            CommonObjects.getContext(),
                            Uri.parse(path));
        }
        if (path != null && path.contains("%20")) {
            path = path.replace("%20", "-");
        }



        imagesList=uri;
        if(isCover){
            ivCover.setImageURI(imagesList);
        }else if(isProfile){
            civAvatar.setImageURI(imagesList);
        }

        if (imagesList!=null){
            updateImage();
        }
    }

    void userDataUpdate(){
        UpdateUserProfileInput updateUserProfileInput =new UpdateUserProfileInput();
        userLoginResponse = new Gson().fromJson(CommonMethods.getStringPreference(mActivity, getString(R.string.app_name), UserLoginResponse.class.getSimpleName(), ""), UserLoginResponse.class);

        if (userLoginResponse!=null){
            updateUserProfileInput.setID(String.valueOf(userLoginResponse.getId()));
        }
        updateUserProfileInput.setFirstName(etFirstName.getText().toString());
        updateUserProfileInput.setLastName(etLastName.getText().toString());
        updateUserProfileInput.setPhone(etPhone.getText().toString());
        updateUserProfileInput.setEmail(etEmail.getText().toString());
        updateUserProfileInput.setPassword(etPassword.getText().toString());
        updateUserProfileInput.setAddress(etAddress.getText().toString());

        AppHandler.updateUserProfile(updateUserProfileInput,new AppHandler.UpdateUserProfileListener() {
            @Override
            public void onSuccess(UserLoginResponse updateUserProfileResponse) {
//                profileData();
                CommonMethods.showMessage(mActivity,"Profile update Successfully");
                CommonMethods.hideProgressDialog();
                userLoginResponse.getData().getUserInfo().setFirstName(etFirstName.getText().toString());
                userLoginResponse.getData().getUserInfo().setLastName(etLastName.getText().toString());
                userLoginResponse.getData().getUserInfo().setPhone(etPhone.getText().toString());
                userLoginResponse.getData().getUserInfo().setAddress(etAddress.getText().toString());
                userLoginResponse.getData().setDisplayName(userLoginResponse.getData().getDisplayName());

                CommonMethods.setStringPreference(
                        CommonObjects.getContext(),
                        CommonObjects.getContext()
                                .getString(R.string.app_name),
                        UserLoginResponse.class.getSimpleName(),
                        new Gson().toJson(userLoginResponse));

                MainActivity.instance().getLoginDetails();
            }

            @Override
            public void onError(String error) {
                CommonMethods.showMessage(mActivity,error);
                CommonMethods.hideProgressDialog();
            }
        });
    }
    boolean checkAvailability(){
        if(etFirstName.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please Enter First Name.");
            return false;
        }else if(etLastName.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please Enter Last Name.");
            return false;
        }else if(etPhone.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please Enter Phone.");
            return false;
        }else if(etPassword.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please Enter Password.");
            return false;
        }else if(etAddress.getText().toString().isEmpty()){
            CommonMethods.showMessage(mActivity,"Please Enter Address.");
            return false;
        } else{
            return true;
        }


    }


    void displayuserData(){
        if(userLoginResponse!=null && userLoginResponse.getData()!=null && userLoginResponse.getData().getUserInfo()!=null){

            String avatar=(userLoginResponse.getData().getUserInfo().getAvatar()!=null && !userLoginResponse.getData().getUserInfo().getAvatar().isEmpty())?userLoginResponse.getData().getUserInfo().getAvatar():"";
            String cover=(userLoginResponse.getData().getUserInfo().getCover()!=null && !userLoginResponse.getData().getUserInfo().getCover().isEmpty())?userLoginResponse.getData().getUserInfo().getCover():"";

            etFirstName.setText(""+userLoginResponse.getData().getUserInfo().getFirstName());
            etLastName.setText(""+userLoginResponse.getData().getUserInfo().getLastName());
            etPhone.setText(""+userLoginResponse.getData().getUserInfo().getPhone());
            etEmail.setText(""+userLoginResponse.getData().getUserEmail());
            etPassword.setText("");
            etAddress.setText(""+userLoginResponse.getData().getUserInfo().getAddress());

            tvName.setText(userLoginResponse.getData().getDisplayName());
            tvAddress.setText(""+userLoginResponse.getData().getUserInfo().getAddress());

            if(avatar!=null && !avatar.isEmpty()){
                Picasso.with(CommonObjects.getContext()).load(avatar).placeholder(R.drawable.broker_avatar).error(R.drawable.broker_avatar).into(civAvatar);
            }else{
                Picasso.with(CommonObjects.getContext()).load(R.drawable.broker_avatar).into(civAvatar);
            }

            if(cover!=null && !cover.isEmpty()){
                Picasso.with(CommonObjects.getContext()).load(cover).placeholder(R.drawable.placeholder_image_sqr_new).error(R.drawable.placeholder_image_sqr_new).into(ivCover);
            }else{
                Picasso.with(CommonObjects.getContext()).load(R.drawable.placeholder_image_sqr_new).into(ivCover);
            }

        }else{
            etFirstName.setText("");
            etLastName.setText("");
            etPhone.setText("");
            etEmail.setText("");
            etPassword.setText("");
            etAddress.setText("");

        }

        tvSave.setEnabled(false);

    }


    private void checkForStoragePermission() {
        if(ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){

            String[] permissionsToask=new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA};

            CommonMethods.showPermissionMessage(mActivity,"To Upload Profile Image, allow "+getString(R.string.app_name)+" to access your device Storage & Camera.",permissionsToask);

        }else {
            openGalleryToShare();
        }
    }

    void openGalleryToShare(){
        CropImage.activity().start(mActivity, ProfileFragment.this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                if (isCover) {
                    updateProfileImage(resultUri);
                }

                if (isProfile) {
                    updateProfileImage(resultUri);
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

}
