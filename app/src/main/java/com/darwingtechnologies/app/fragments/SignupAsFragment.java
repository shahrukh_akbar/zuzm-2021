package com.darwingtechnologies.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.darwingtechnologies.app.R;
import com.darwingtechnologies.app.utilities.CommonMethods;


/**
 * Created by Rana Zeshan on 27-Jun-19.
 */

public class SignupAsFragment extends BaseFragment{
    private Spinner spnType;
    private int typePosition = 0;
    String[] roles = {"Customer","Broker"};
    private TextView tvContinue;

    @Override
    protected void initView() {
        setIbBack(mView.findViewById(R.id.ibBack),null);

        spnType = mView.findViewById(R.id.spnType);
        spnType.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(
                            AdapterView<?> adapterView, View view, int i, long l) {
                        typePosition = i;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {}
                });

        roles[0]=getString(R.string.customer);
        roles[1]=getString(R.string.broker);
        setUpSpinner(spnType,roles);

        tvContinue=mView.findViewById(R.id.tvContinue);
        tvContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommonMethods.callFragment(SignUpFragment.newInstance(typePosition==1), R.id.flFragmentContainer, R.anim.fade_in, R.anim.fade_out, mActivity, true);
            }
        });

        CommonMethods.setupUI(mView,mActivity);
    }

    @Override
    protected void loadData() {

    }

    public static SignupAsFragment newInstance() {
        SignupAsFragment fragment = new SignupAsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.sign_up_as_fragment, null);
    }

    @Override
    public String getFragmentTag() {
        return SignupAsFragment.class.getSimpleName();
    }


    private void setUpSpinner(Spinner spinner, String[] items) {
        ArrayAdapter<String> dataAdapter =
                new ArrayAdapter<>(mActivity, R.layout.item_spinner_detailing, items);
        dataAdapter.setDropDownViewResource(R.layout.item_spinner_dropdown_detailing);
        spinner.setAdapter(dataAdapter);
    }

}
