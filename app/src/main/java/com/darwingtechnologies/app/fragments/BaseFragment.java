package com.darwingtechnologies.app.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.darwingtechnologies.app.R;
import com.darwingtechnologies.app.activities.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 *
 * Created by Rana Zeshan on 27-Jun-19.
 */


@SuppressLint("ValidFragment")
public abstract class BaseFragment extends Fragment {
    protected FragmentActivity mActivity;
    protected View mView;
    private static Button btnBack;
    private static ImageButton ibBack;

    public void setBtnBack(Button btnBack) {
        this.btnBack = btnBack;
        ibBack = null;
    }

//    public static void setIbBack(ImageButton ibBack) {
//        BaseFragment.ibBack = ibBack;
//        btnBack = null;
//    }
public void setIbBack(ImageButton ibBack, String pageName) {
    BaseFragment.ibBack = ibBack;

    if (pageName != null) {
        TextView tvPageName = mActivity.findViewById(R.id.tvPageName);

        if (tvPageName != null) {
            tvPageName.setText(pageName);
        }
    }

    ibBack.setOnClickListener(
            new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MainActivity.instance().isShowSpinKit(false);
//
//                        if (doubleBackToExitPressedOnce) {
//
//                        } else {
//                            doubleBackToExitPressedOnce = true;
//                            new Handler()
//                                    .postDelayed(
//                                            new Runnable() {
//
//                                                @Override
//                                                public void run() {
//                                                    doubleBackToExitPressedOnce = false;
//                    android.util.Log.e("currentFragmentBefor:",MainActivity.instance().getCurrentFragment().name());
//                                                    mActivity.getSupportFragmentManager().popBackStackImmediate();
                    MainActivity.instance().popBackStackMytm();
//                    android.util.Log.e("currentFragmentafetr:",MainActivity.instance().getCurrentFragment().name());
//
//                                                }
//                                            },
//                                            100);
//                        }

                }
            });
}

    public static Button getBtnBack() {
        return btnBack;
    }

    public static ImageButton getIbBack() {
        return ibBack;
    }

    public BaseFragment() {

    }

    abstract protected void initView();

    abstract protected void loadData();

    abstract public String getFragmentTag();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mView = view;
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        if (mActivity.getSupportFragmentManager().getBackStackEntryCount() == 0) {
                            return false;
                        }
                        if (btnBack == null && ibBack == null) {
                            mActivity.getSupportFragmentManager().popBackStack();
                        } else if (btnBack != null) {
                            btnBack.performClick();
                            btnBack = null;
                        } else {
                            ibBack.performClick();
                            ibBack = null;
                        }
                        return true;
                    }
                }
                return false;
            }
        });
        initView();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUserVisibleHint(false);
        mActivity = getActivity();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }
}
