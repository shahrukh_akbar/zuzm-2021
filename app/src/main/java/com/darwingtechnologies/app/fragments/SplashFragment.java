package com.darwingtechnologies.app.fragments;

import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import com.darwingtechnologies.app.ButtonAnimations.MyBounceInterpolator;
import com.darwingtechnologies.app.R;
import com.darwingtechnologies.app.activities.MainActivity;
import com.darwingtechnologies.app.business.models.AgentLoginResponse;
import com.darwingtechnologies.app.business.models.userLogin.UserLoginResponse;
import com.darwingtechnologies.app.utilities.CommonMethods;
import com.darwingtechnologies.app.utilities.CommonObjects;
import com.google.gson.Gson;


/**
 * Created by Rana Zeshan on 27-Jun-19.
 */
public class SplashFragment extends BaseFragment  {
    private static int SPLASH_TIME_OUT = 1000;
    private Handler handler=new Handler();
    private Runnable runnable;
    private AgentLoginResponse agentLoginResponse;
    private static final int REQUEST_CODE_GPS_PERMISSION = 50;
    private LocationManager locationManager;
    private UserLoginResponse userLoginResponse;

    private RelativeLayout llAppName;

    @Override
    protected void initView() {


        final Animation animation= AnimationUtils.loadAnimation(mActivity,R.anim.bounceb);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        animation.setInterpolator(interpolator);

        runnable=new Runnable() {
            @Override
            public void run() {
                callNextFragment();
            }
        };


        CommonObjects.setContext(mActivity);
        handler.postDelayed(runnable, SPLASH_TIME_OUT);

    }

    @Override
    protected void loadData() {
        MainActivity.instance().hideTopBar();
        MainActivity.instance().hideBottomBar();

    }

    public static SplashFragment newInstance() {
        SplashFragment fragment = new SplashFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.splash_fragment, null);
    }

    @Override
    public String getFragmentTag() {
        return SplashFragment.class.getSimpleName();
    }

    public void callNextFragment(){
         userLoginResponse = new Gson().fromJson(CommonMethods.getStringPreference(mActivity, getString(R.string.app_name), UserLoginResponse.class.getSimpleName(), ""), UserLoginResponse.class);
         if (userLoginResponse!=null && userLoginResponse.getId()!=null){
             CommonMethods.callFragment(HomeScreenFragment.newInstance(), R.id.flFragmentContainer, R.anim.fade_in, R.anim.fade_out, mActivity, false);
         }else {
             CommonMethods.callFragment(SignInFragment.newInstance(), R.id.flFragmentContainer, R.anim.fade_in, R.anim.fade_out, mActivity, false);
         }
            }

}
