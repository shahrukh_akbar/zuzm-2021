package com.darwingtechnologies.app.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.darwingtechnologies.app.R;
import com.darwingtechnologies.app.activities.MainActivity;
import com.darwingtechnologies.app.adapters.BrokersListAdapter;
import com.darwingtechnologies.app.business.models.searchBroker.BrokerResponse;
import com.darwingtechnologies.app.business.models.userLogin.UserLoginResponse;
import com.darwingtechnologies.app.utilities.CommonMethods;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Rana Zeshan on 27-Jun-19.
 */

public class ListOfBrokersFragment extends BaseFragment{
    private  ArrayList<BrokerResponse> listItems;
    private  ArrayList<BrokerResponse> listItemsFiltered=new ArrayList<>();
    private RecyclerView rvListItems;

    private EditText etSearch;
    private CheckBox cbSearchIcon;

    private UserLoginResponse userLoginResponse;


    @Override
    protected void initView() {
//        setIbBack((ImageButton) mView.findViewById(R.id.ibBack), "Brokers");

        rvListItems=mView.findViewById(R.id.rvListItems);
        rvListItems.setLayoutManager(new LinearLayoutManager(mActivity,LinearLayoutManager.VERTICAL,false));

        etSearch =  mView.findViewById(R.id.etSearch);

        cbSearchIcon=mView.findViewById(R.id.cbSearchIconCustomer);
        cbSearchIcon.setChecked(true);
        cbSearchIcon.setEnabled(false);
        cbSearchIcon.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    etSearch.setText("");
                    cbSearchIcon.setEnabled(false);
                }
            }
        });

        etSearch.addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(
                            CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        if(!etSearch.getText().toString().isEmpty()){
                            cbSearchIcon.setChecked(false);
                            cbSearchIcon.setEnabled(true);
                        }else{
                            cbSearchIcon.setChecked(true);
                            cbSearchIcon.setEnabled(false);
                        }

                        performFiltering(etSearch.getText().toString());
                    }
                });


        CommonMethods.setupUI(mView,mActivity);
    }

    @Override
    protected void loadData() {

        rvListItems.setAdapter(new BrokersListAdapter(listItemsFiltered));
    }

    public static ListOfBrokersFragment newInstance(ArrayList<BrokerResponse> listItems) {
        ListOfBrokersFragment fragment = new ListOfBrokersFragment();
        fragment.listItems=listItems;
        if(fragment.listItems==null){
            fragment.listItems=new ArrayList<>();
        }
        fragment.listItemsFiltered.addAll(listItems);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.brokers_list_fragment, null);
    }

    @Override
    public String getFragmentTag() {
        return ListOfBrokersFragment.class.getSimpleName();
    }


    private void performFiltering(String charString) {

        if (charString.isEmpty()) {
            listItemsFiltered.clear();
            listItemsFiltered.addAll(listItems);
        } else {
            List<BrokerResponse> filteredList = new ArrayList<>();
            for (BrokerResponse brokerResponse : listItems) {

                // name match condition. this might differ depending on your requirement
                // here we are looking for name or phone number match
                if (brokerResponse!=null){
                    final String name=brokerResponse.getPostTitle()!=null?brokerResponse.getPostTitle():"";
                    final String shortDetail=brokerResponse.getPostContent()!=null?brokerResponse.getPostContent():"";
                    final String email=brokerResponse.getSearchBrokerListInfo()!=null && brokerResponse.getSearchBrokerListInfo().getJobEmail()!=null ?brokerResponse.getSearchBrokerListInfo().getJobEmail():"";
                    final String number=brokerResponse.getSearchBrokerListInfo()!=null && brokerResponse.getSearchBrokerListInfo().getJobPhone()!=null ?brokerResponse.getSearchBrokerListInfo().getJobPhone():"";

                        if (name.toLowerCase().contains(charString.toLowerCase()) || shortDetail.toLowerCase().contains(charString.toLowerCase()) || email.toLowerCase().contains(charString.toLowerCase()) || number.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(brokerResponse);
                        }
                }

            }
            listItemsFiltered.clear();
            listItemsFiltered.addAll(filteredList);
//            Collections.reverse(data_listFiltered);
        }

        loadData();
    }

}
