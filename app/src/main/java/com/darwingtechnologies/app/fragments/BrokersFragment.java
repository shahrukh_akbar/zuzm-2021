package com.darwingtechnologies.app.fragments;

/*
EmptyFragment.java
Copyright (C) 2017  Belledonne Communications, Grenoble, France

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.airbnb.lottie.model.Marker;
import com.darwingtechnologies.app.R;
import com.darwingtechnologies.app.activities.MainActivity;
import com.darwingtechnologies.app.adapters.ViewPageAdapter;
import com.darwingtechnologies.app.business.handlers.AppHandler;
import com.darwingtechnologies.app.business.models.searchBroker.BrokerResponse;
import com.darwingtechnologies.app.utilities.CommonMethods;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;


import java.util.ArrayList;

public class BrokersFragment extends BaseFragment {

    private ImageButton ibBack;
    private TabLayout tlBrokers;
    private ViewPager vpBrokers;
    private ViewPageAdapter viewPageAdapter;

    private LinearLayout llLoader;
    ArrayList<BrokerResponse> listItems;
    ArrayList<BrokerResponse> listItemsFilteredForMarker=new ArrayList<>();
    ArrayList<LatLng> latLngs=new ArrayList<>();
    private Boolean isMainScreen=true;

    private LinearLayout llShowHideTop;




    public static BrokersFragment newInstance(ArrayList<BrokerResponse> listItems, boolean isMainScreen) {
        BrokersFragment fragment = new BrokersFragment();
        fragment.listItems=listItems;
        fragment.isMainScreen=isMainScreen;
        return fragment;
    }


    @Override
    protected void initView() {
        ibBack = (ImageButton) mView.findViewById(R.id.ibBack);

        setIbBack(ibBack, ""+getString(R.string.broker));
        llShowHideTop=mView.findViewById(R.id.llShowHideTop);

        llLoader = mView.findViewById(R.id.llLoader);



        tlBrokers = mView.findViewById(R.id.tlTabs);
        vpBrokers = mView.findViewById(R.id.vpTabs);

        tlBrokers.setSelectedTabIndicatorColor(Color.WHITE);
        tlBrokers.setTabTextColors(Color.GRAY,Color.WHITE);
        viewPageAdapter = new ViewPageAdapter(getChildFragmentManager());

    }

    @Override
    protected void loadData() {
        if(isMainScreen){
            llShowHideTop.setVisibility(View.GONE);
        }else{
            llShowHideTop.setVisibility(View.VISIBLE);
        }

        updateUI();
    }

    @Override
    public String getFragmentTag() {
        return BrokersFragment.class.getSimpleName();
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.brokers_fragment, container, false);
        return view;
    }

    void addTabsandPages(){

        viewPageAdapter.addFragment(ListOfBrokersFragment.newInstance(listItems), ""+getString(R.string.broker_list));
        viewPageAdapter.addFragment(MapViewFragment.newInstance(latLngs, listItemsFilteredForMarker,true, false), ""+getString(R.string.map_view));




        vpBrokers.setAdapter(viewPageAdapter);
        tlBrokers.setupWithViewPager(vpBrokers);
        tlBrokers.getTabAt(0).setIcon(R.drawable.person_list);
        tlBrokers.getTabAt(1).setIcon(R.drawable.personal_location);

        int tabIconColor = ContextCompat.getColor(mActivity, R.color.white);
        tlBrokers.getTabAt(0).getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);

        int tabIconColor2 = ContextCompat.getColor(mActivity, R.color.Color_Gray);
        tlBrokers.getTabAt(1).getIcon().setColorFilter(tabIconColor2, PorterDuff.Mode.SRC_IN);

        tlBrokers.addOnTabSelectedListener(
                new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        try {
                            int tabIconColor = ContextCompat.getColor(mActivity, R.color.white);
                            tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {
                        try {
                            int tabIconColor = ContextCompat.getColor(mActivity, R.color.Color_Gray);
                            tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {}
                });

    }

    void loadBrokersList(){
        AppHandler.getAllBroker(new AppHandler.GetAllBrokerListener() {
            @Override
            public void onSuccess(ArrayList<BrokerResponse> getAllBrokerResponse) {
                if(getAllBrokerResponse!=null && getAllBrokerResponse.size()>0) {
                    listItems = new ArrayList<>();
                    listItems.clear();
                    for (int i = 0; i <getAllBrokerResponse.size() ; i++) {

                        if(MainActivity.userLoginResponse!=null){
                            if(!MainActivity.userLoginResponse.getId().equals(Integer.parseInt(getAllBrokerResponse.get(i).getPostAuthor()))){
                                listItems.add(getAllBrokerResponse.get(i));
                            }else{
                                android.util.Log.e("userFoundAT["+i+"]",new Gson().toJson(getAllBrokerResponse));
                            }
                        }else{
                            listItems.addAll(getAllBrokerResponse);
                            break;
                        }
                    }
                    updateUI();
                }else{
                    CommonMethods.showMessage(mActivity,"No brokers found");
                }
            }

            @Override
            public void onError(String error) {
                CommonMethods.showMessage(mActivity,""+error);

            }
        });
    }

    void updateUI(){
        if(listItems!=null){
            filterListForMarkers();
        }else{
            if(isMainScreen){
                loadBrokersList();
            }else{
                CommonMethods.displayMessagesDialog("No Search found...!!!",mActivity).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create().show();
            }
        }
    }

    void filterListForMarkers(){
        if(listItemsFilteredForMarker!=null && listItemsFilteredForMarker.size()>0 && viewPageAdapter!=null && viewPageAdapter.getCount()>0){
            return;
        }
        for (int i = 0; i <listItems.size() ; i++) {
            if(listItems.get(i)!=null && listItems.get(i).getSearchBrokerListInfo()!=null){
                if(listItems.get(i).getSearchBrokerListInfo().getGeolocationLat()!=null && listItems.get(i).getSearchBrokerListInfo().getGeolocationLong()!=null && !listItems.get(i).getSearchBrokerListInfo().getGeolocationLat().isEmpty() && !listItems.get(i).getSearchBrokerListInfo().getGeolocationLong().isEmpty()){
                    LatLng latLng=new LatLng(Double.parseDouble(listItems.get(i).getSearchBrokerListInfo().getGeolocationLat()), Double.parseDouble(listItems.get(i).getSearchBrokerListInfo().getGeolocationLong()));
                    latLngs.add(latLng);
                    listItemsFilteredForMarker.add(listItems.get(i));
                }
            }
        }

        addTabsandPages();
    }



}
