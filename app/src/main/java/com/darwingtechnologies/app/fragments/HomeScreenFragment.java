package com.darwingtechnologies.app.fragments;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.darwingtechnologies.app.R;
import com.darwingtechnologies.app.activities.MainActivity;
import com.darwingtechnologies.app.business.handlers.AppHandler;
import com.darwingtechnologies.app.business.models.getBrokerByCategory.GetBrokerByCategoryResponse;
import com.darwingtechnologies.app.business.models.getBrokerType.BrokerTypeResponse;
import com.darwingtechnologies.app.business.models.searchBroker.SearchBrokerInput;
import com.darwingtechnologies.app.business.models.searchBroker.BrokerResponse;
import com.darwingtechnologies.app.business.models.userLogin.UserLoginResponse;
import com.darwingtechnologies.app.utilities.CommonMethods;
import com.darwingtechnologies.app.utilities.CommonObjects;
import com.darwingtechnologies.app.utilities.GeoLocator;
import com.darwingtechnologies.app.utilities.LocaleHelper;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;

import static com.darwingtechnologies.app.BuildConfig.MINIMUM_RADIUS_IN_MILES;


/**
 * Created by Rana Zeshan on 27-Jun-19.
 */

public class HomeScreenFragment extends BaseFragment{

    private MapViewFragment mapViewFragment;
    private TextView tvPermissionRequiredMessage;
    private ArrayList<BrokerResponse> listItems;
    ArrayList<BrokerResponse> listItemsFilteredForMarker=new ArrayList<>();
    ArrayList<LatLng> latLngs=new ArrayList<>();

    private Spinner spnCategory, spnType;
    private EditText etKeyWord;
    private DrawerLayout drawer;
    private NavigationView nvLeft;
    private LinearLayout llCategory,llSearch,llType;
    private TextView tvLogout;
    private ArrayList<BrokerTypeResponse> getBrokerTypeList = new ArrayList<>();
    private int typePosition = 0;
    private int categoryPosition = 0;

    private ArrayList<GetBrokerByCategoryResponse> getBrokerByCategoryList = new ArrayList<>();

    private TextView tvBecomeBroker;
    private UserLoginResponse userLoginResponse;

    @Override
    protected void initView() {
//        ibBack = (ImageButton) mView.findViewById(R.id.ibBack);
        spnType = mView.findViewById(R.id.spnType);
        drawer = mView.findViewById(R.id.drawer);
        nvLeft = mView.findViewById(R.id.nvLeft);
        tvLogout = mView.findViewById(R.id.tvLogout);
        spnCategory = mView.findViewById(R.id.spnCategory);
        llCategory = mView.findViewById(R.id.llCategory);
        etKeyWord=mView.findViewById(R.id.etKeyWord);
        llSearch = mView.findViewById(R.id.llSearch);
        llType = mView.findViewById(R.id.llType);

//        String[] getType ={"Commercial"}  ;
//        setUpSpinnerType(spnType, getType);

        llSearch.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        searchBrokerData();
                    }
                });

        llType.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        spnType.performClick();
                    }
                });
        spnType.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(
                            AdapterView<?> adapterView, View view, int i, long l) {
                        typePosition = i;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {}
                });
        llCategory.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        spnCategory.performClick();
                    }
                });
        spnCategory.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(
                            AdapterView<?> adapterView, View view, int i, long l) {
                        categoryPosition = i;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {}
                });


        tvBecomeBroker=mView.findViewById(R.id.tvBecomeBroker);
        tvBecomeBroker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(MainActivity.userLoginResponse==null){
                    CommonMethods.displayMessagesDialog("Please login to continue",mActivity).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            getIbBack().performClick();
                            dialogInterface.dismiss();
                        }
                    }).setCancelable(false).create().show();
                }else{
                    CommonMethods.callFragment(BecomeBrokerFragment.newInstance(), R.id.flFragmentOverlay, R.anim.fade_in, R.anim.fade_out, CommonObjects.getContext(), true);
                }
            }
        });

        tvPermissionRequiredMessage=mView.findViewById(R.id.tvPermissionRequiredMessage);




        getBrokerTypeData();
        CommonMethods.setupUI(mView,mActivity);
    }

    @Override
    protected void loadData() {

        MainActivity.instance().showTopBar();
        MainActivity.instance().showBottomBar();

        userLoginResponse=CommonMethods.getUserDetail(mActivity);
        if(userLoginResponse!=null && userLoginResponse.getData()!=null && userLoginResponse.getData().getBrokerInfo()!=null && userLoginResponse.getData().getBrokerInfo().size()>0){
            tvBecomeBroker.setVisibility(View.GONE);
        }else{
            tvBecomeBroker.setVisibility(View.VISIBLE);
        }

        checkForLocationPermission();

    }

    public static HomeScreenFragment newInstance() {
        HomeScreenFragment fragment = new HomeScreenFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.home_screen_drawer_fragment, null);
    }

    @Override
    public String getFragmentTag() {
        return HomeScreenFragment.class.getSimpleName();
    }



    private void setUpSpinnerType(Spinner spinner, String[] items) {
        ArrayAdapter<String> dataAdapter =
                new ArrayAdapter<>(mActivity, R.layout.item_spinner_detailing, items);
        dataAdapter.setDropDownViewResource(R.layout.item_spinner_dropdown_detailing);
        spinner.setAdapter(dataAdapter);
    }
    private void setUpSpinnerCategory(Spinner spinner, String[] items) {
        ArrayAdapter<String> dataAdapter =
                new ArrayAdapter<>(mActivity, R.layout.item_spinner_detailing, items);
        dataAdapter.setDropDownViewResource(R.layout.item_spinner_dropdown_detailing);
        spinner.setAdapter(dataAdapter);
    }
    void getBrokerTypeData(){

        AppHandler.getBrokerType(true,new AppHandler.GetBrokerTypeListener() {
            @Override
            public void onSuccess(ArrayList<BrokerTypeResponse> getBrokerTypeResponse) {
                getBrokerTypeList.clear();
                getBrokerTypeList.addAll(getBrokerTypeResponse);
                setUpSpinnerType(spnType, getBrokerTypeList(getBrokerTypeList));
                spnType.setSelection(typePosition);
                getBrokerCategoryData();
            }

            @Override
            public void onError(String error) {
                CommonMethods.showMessage(mActivity,error);
                CommonMethods.hideProgressDialog();
            }
        });
    }
    void getBrokerCategoryData(){

        AppHandler.getBrokerByCategory(new AppHandler.GetBrokerByCategoryListener() {
            @Override
            public void onSuccess(ArrayList<GetBrokerByCategoryResponse> getBrokerByCategoryResponses) {
                getBrokerByCategoryList.clear();
                getBrokerByCategoryList.addAll(getBrokerByCategoryResponses);
                setUpSpinnerCategory(spnCategory, getBrokerCatList(getBrokerByCategoryList));
                spnCategory.setSelection(categoryPosition);
            }

            @Override
            public void onError(String error) {
                CommonMethods.showMessage(mActivity,error);
                CommonMethods.hideProgressDialog();
            }
        });
    }

    private String[] getBrokerTypeList( ArrayList<BrokerTypeResponse> transportServicesArrival){
        int size =
                transportServicesArrival.size();
        String[] serviceList = new String[size];

        for (int i = 0; i < transportServicesArrival.size(); i++) {

            String en=transportServicesArrival.get(i).getPostTitle();
            String ar=transportServicesArrival.get(i).getPostTitle();
            if(transportServicesArrival.get(i).getPostTitle()!=null && !transportServicesArrival.get(i).getPostTitle().isEmpty() && transportServicesArrival.get(i).getPostTitle().contains("/#")){

                try {
                    en = transportServicesArrival.get(i).getPostTitle().split("/#")[0];
                    ar = transportServicesArrival.get(i).getPostTitle().split("/#")[1];
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            android.util.Log.e("BrokerType(" + i+")", "en:" + en + "|| ar:" + ar);
            serviceList[i] =LocaleHelper.isDefaultLanguage(MainActivity.instance())? ""+en:""+ar;
        }
        return serviceList;
    }

    private String[] getBrokerCatList( ArrayList<GetBrokerByCategoryResponse> transportServicesArrival){
        int size =
                transportServicesArrival.size();
        String[] serviceList = new String[size];

        for (int i = 0; i < transportServicesArrival.size(); i++) {
            String en=transportServicesArrival.get(i).getName();
            String ar=transportServicesArrival.get(i).getName();
            if(transportServicesArrival.get(i).getName()!=null && !transportServicesArrival.get(i).getName().isEmpty() && transportServicesArrival.get(i).getName().contains("/#")){

                try {
                    en = transportServicesArrival.get(i).getName().split("/#")[0];
                    ar = transportServicesArrival.get(i).getName().split("/#")[1];
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            android.util.Log.e("BrokerCategory(" + i+")", "en:" + en + "|| ar:" + ar);
                serviceList[i] =LocaleHelper.isDefaultLanguage(MainActivity.instance())? ""+en:""+ar;
        }
        return serviceList;
    }

    void searchBrokerData(){

        SearchBrokerInput searchBrokerInput = new SearchBrokerInput();
        searchBrokerInput.setListingType(getBrokerTypeList.get(typePosition).getPostName());
        searchBrokerInput.setKeyword(etKeyWord.getText().toString());
        searchBrokerInput.setTermId(getBrokerByCategoryList.get(categoryPosition).getTermId()+"");
        AppHandler.searchBroker(searchBrokerInput,new AppHandler.SearchBrokerListener() {
            @Override
            public void onSuccess(ArrayList<BrokerResponse> updateUserProfileResponse) {
                CommonMethods.hideProgressDialog();
                CommonMethods.callFragment(BrokersFragment.newInstance(updateUserProfileResponse,false), R.id.flFragmentOverlay, R.anim.fade_in, R.anim.fade_out, CommonObjects.getContext(), true);
            }

            @Override
            public void onError(String error) {
                CommonMethods.showMessage(mActivity,error);
                CommonMethods.hideProgressDialog();
            }
        });
    }

    private void checkForLocationPermission() {
        if(ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            tvPermissionRequiredMessage.setVisibility(View.VISIBLE);

            loadMapView(true);

            String[] permissionsToask=new String[]{Manifest.permission.ACCESS_FINE_LOCATION};

            CommonMethods.showPermissionMessage(mActivity,"To view Nearby Brokers, allow "+getString(R.string.app_name)+" to access your Location.",permissionsToask);

        }else {
            tvPermissionRequiredMessage.setVisibility(View.GONE);
            loadBrokersList();
        }
    }

    private void loadMapView(boolean isSimple){
        if(isSimple || latLngs==null || listItemsFilteredForMarker==null) {
            mapViewFragment = MapViewFragment.newInstance(null, false, false);
            CommonMethods.callFragment(mapViewFragment, R.id.flMapView, -1, -1, mActivity, false);
            tvPermissionRequiredMessage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    checkForLocationPermission();
                }
            });

        }else{
            mapViewFragment = MapViewFragment.newInstance(latLngs, listItemsFilteredForMarker,true, false);
            CommonMethods.callFragment(mapViewFragment, R.id.flMapView, -1, -1, mActivity, false);

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            // permission was granted, yay! Do the
            // location-related task you need to do.
//                    if (ContextCompat.checkSelfPermission(mActivity,
//                            Manifest.permission.ACCESS_FINE_LOCATION)
//                            == PackageManager.PERMISSION_GRANTED) {
//                        buildGoogleApiClient();
//                        mGoogleMap.setMyLocationEnabled(true);
//                        addMyMarker();
//                    }
//            loadMapView(false);
            loadBrokersList();
            tvPermissionRequiredMessage.setVisibility(View.GONE);


        } else {

            // permission denied, boo! Disable the
            // functionality that depends on this permission.
            CommonMethods.showToast("permission denied", Toast.LENGTH_SHORT);
            tvPermissionRequiredMessage.setVisibility(View.VISIBLE);

        }
    }


    void loadBrokersList(){
        AppHandler.getAllBroker(new AppHandler.GetAllBrokerListener() {
            @Override
            public void onSuccess(ArrayList<BrokerResponse> getAllBrokerResponse) {
                if(getAllBrokerResponse!=null && getAllBrokerResponse.size()>0) {
                    listItems = new ArrayList<>();
                    listItems.clear();
                    listItems.addAll(getAllBrokerResponse);
                    updateUI();
                }else{
                    CommonMethods.showToast("No brokers found",Toast.LENGTH_LONG);
                }
            }

            @Override
            public void onError(String error) {
                CommonMethods.showMessage(mActivity,""+error);

            }
        });
    }

    void updateUI(){
        GeoLocator geoLocator = new GeoLocator(mActivity);
        LatLng myLocation = new LatLng(geoLocator.getLattitude(), geoLocator.getLongitude());

        if(listItems!=null){
            for (int i = 0; i <listItems.size() ; i++) {
                if(listItems.get(i)!=null && listItems.get(i).getSearchBrokerListInfo()!=null){
                    if(listItems.get(i).getSearchBrokerListInfo().getGeolocationLat()!=null && listItems.get(i).getSearchBrokerListInfo().getGeolocationLong()!=null && !listItems.get(i).getSearchBrokerListInfo().getGeolocationLat().isEmpty() && !listItems.get(i).getSearchBrokerListInfo().getGeolocationLong().isEmpty()){
                        LatLng latLng=new LatLng(Double.parseDouble(listItems.get(i).getSearchBrokerListInfo().getGeolocationLat()), Double.parseDouble(listItems.get(i).getSearchBrokerListInfo().getGeolocationLong()));
                       if(CommonMethods.distanceLatLongInMeter(myLocation.latitude,myLocation.longitude,latLng.latitude,latLng.longitude)<= MINIMUM_RADIUS_IN_MILES){
                           latLngs.add(latLng);
                           listItemsFilteredForMarker.add(listItems.get(i));
                       }
                    }
                }
            }

            loadMapView(false);
        }
    }

}
