package com.darwingtechnologies.app.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;


import com.darwingtechnologies.app.R;
import com.darwingtechnologies.app.activities.MainActivity;
import com.darwingtechnologies.app.utilities.CommonMethods;
import com.darwingtechnologies.app.utilities.PolyUtil;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import static com.darwingtechnologies.app.activities.MainActivity.latLng;

public class HomeMapFragment extends BaseFragment {
    private MapViewFragment mapViewFragment;

    @Override
    protected void initView() {
        setIbBack(mView.findViewById(R.id.ibBack),"Pick Location");
        mapViewFragment=MapViewFragment.newInstance(latLng,true,true);
        CommonMethods.callFragment(mapViewFragment, R.id.flMapView,-1, -1, mActivity, false);


//        mView.findViewById(R.id.btnDone).setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (((MainActivity) mActivity).checkForGPSOn()) {
//                    if (mapViewFragment.mCurrLocationMarker != null) {
//
//                        latLng = new LatLng(mapViewFragment.mCurrLocationMarker.getPosition().latitude, mapViewFragment.mCurrLocationMarker.getPosition().longitude);
//                        getIbBack().performClick();
//                    }
//                }
//            }
//        });
//        mView.findViewById(R.id.btnGoToCurrentLocation).setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mapViewFragment=MapViewFragment.newInstance(latLng,true,true);
//                CommonMethods.callFragment(mapViewFragment, R.id.flMapView,-1, -1, mActivity, false);
//            }
//        });
        mView.findViewById(R.id.ibBack).setVisibility(View.VISIBLE);
    }

    @Override
    protected void loadData() {

    }

    public static HomeMapFragment newInstance() {
        HomeMapFragment fragment = new HomeMapFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.fragment_home_map,null);
    }

    @Override
    public String getFragmentTag() {
        return HomeMapFragment.class.getSimpleName();
    }


}
