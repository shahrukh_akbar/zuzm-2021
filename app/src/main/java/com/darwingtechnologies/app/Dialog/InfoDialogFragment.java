package com.darwingtechnologies.app.Dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.darwingtechnologies.app.R;
import com.darwingtechnologies.app.utilities.CommonMethods;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

@SuppressLint("ValidFragment")
public class InfoDialogFragment extends BottomSheetDialogFragment {
    private View customView;
    private Context context;
    private TextView tvOk,tvNo;

    //    public void setCommentData(CommentData commentData) {
    //        this.commentData = commentData;
    //    }

    public InfoDialogFragment() {}

    @NonNull
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        context = getContext();
        customView = CommonMethods.createView(context, R.layout.fragment_dialog_info, null);

        final Dialog dialog = new Dialog(context, R.style.DialogFragmentThemes);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        tvOk = customView.findViewById(R.id.tvOk);
        tvNo = customView.findViewById(R.id.tvNo);

        customView.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dismiss();
                    }
                });

        //        customView
        //                .findViewById(R.id.tvCancel)
        //                .setOnClickListener(
        //                        new View.OnClickListener() {
        //                            @Override
        //                            public void onClick(View view) {
        //                                dismiss();
        //                            }
        //                        });

        tvOk.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dismiss();
                    }
                });
        tvNo.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dismiss();
                    }
                });
        customView.setFocusableInTouchMode(true);
        customView.requestFocus();
        customView.setOnKeyListener(
                new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN) {
                            if (keyCode == KeyEvent.KEYCODE_BACK) {
                                dismiss();
                                return true;
                            }
                        }
                        return false;
                    }
                });

        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(customView);
        return dialog;
    }

    private void updateImageLayoutHeight(LinearLayout linearLayout) {
        ViewGroup.LayoutParams params = linearLayout.getLayoutParams();
        params.height = CommonMethods.getDeviceWidth(context);
        linearLayout.setLayoutParams(params);
    }
}
