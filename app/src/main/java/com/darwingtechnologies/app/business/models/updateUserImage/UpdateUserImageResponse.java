
package com.darwingtechnologies.app.business.models.updateUserImage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateUserImageResponse {

    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("avatar")
    @Expose
    private String avatar;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

}
