
package com.darwingtechnologies.app.business.models.updateUserProfile;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateUserProfileResponse {

    @SerializedName("data")
    @Expose
    private UpdateUserProfileData data;
    @SerializedName("ID")
    @Expose
    private Long id;
    @SerializedName("caps")
    @Expose
    private UpdateUserProfileCaps caps;
    @SerializedName("cap_key")
    @Expose
    private String capKey;
    @SerializedName("roles")
    @Expose
    private List<String> roles = null;
    @SerializedName("allcaps")
    @Expose
    private UpdateUserProfileAllcaps allcaps;
    @SerializedName("filter")
    @Expose
    private Object filter;

    public UpdateUserProfileData getData() {
        return data;
    }

    public void setData(UpdateUserProfileData data) {
        this.data = data;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UpdateUserProfileCaps getCaps() {
        return caps;
    }

    public void setCaps(UpdateUserProfileCaps caps) {
        this.caps = caps;
    }

    public String getCapKey() {
        return capKey;
    }

    public void setCapKey(String capKey) {
        this.capKey = capKey;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public UpdateUserProfileAllcaps getAllcaps() {
        return allcaps;
    }

    public void setAllcaps(UpdateUserProfileAllcaps allcaps) {
        this.allcaps = allcaps;
    }

    public Object getFilter() {
        return filter;
    }

    public void setFilter(Object filter) {
        this.filter = filter;
    }

}
