
package com.darwingtechnologies.app.business.models.register;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterData {

    @SerializedName("ID")
    @Expose
    private String id;
    @SerializedName("user_login")
    @Expose
    private String userLogin;
    @SerializedName("user_pass")
    @Expose
    private String userPass;
    @SerializedName("user_nicename")
    @Expose
    private String userNicename;
    @SerializedName("user_email")
    @Expose
    private String userEmail;
    @SerializedName("user_url")
    @Expose
    private String userUrl;
    @SerializedName("user_registered")
    @Expose
    private String userRegistered;
    @SerializedName("user_activation_key")
    @Expose
    private String userActivationKey;
    @SerializedName("user_status")
    @Expose
    private String userStatus;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    @SerializedName("user_info")
    @Expose
    private RegisterUserInfo userInfo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public RegisterData withId(String id) {
        this.id = id;
        return this;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public RegisterData withUserLogin(String userLogin) {
        this.userLogin = userLogin;
        return this;
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }

    public RegisterData withUserPass(String userPass) {
        this.userPass = userPass;
        return this;
    }

    public String getUserNicename() {
        return userNicename;
    }

    public void setUserNicename(String userNicename) {
        this.userNicename = userNicename;
    }

    public RegisterData withUserNicename(String userNicename) {
        this.userNicename = userNicename;
        return this;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public RegisterData withUserEmail(String userEmail) {
        this.userEmail = userEmail;
        return this;
    }

    public String getUserUrl() {
        return userUrl;
    }

    public void setUserUrl(String userUrl) {
        this.userUrl = userUrl;
    }

    public RegisterData withUserUrl(String userUrl) {
        this.userUrl = userUrl;
        return this;
    }

    public String getUserRegistered() {
        return userRegistered;
    }

    public void setUserRegistered(String userRegistered) {
        this.userRegistered = userRegistered;
    }

    public RegisterData withUserRegistered(String userRegistered) {
        this.userRegistered = userRegistered;
        return this;
    }

    public String getUserActivationKey() {
        return userActivationKey;
    }

    public void setUserActivationKey(String userActivationKey) {
        this.userActivationKey = userActivationKey;
    }

    public RegisterData withUserActivationKey(String userActivationKey) {
        this.userActivationKey = userActivationKey;
        return this;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public RegisterData withUserStatus(String userStatus) {
        this.userStatus = userStatus;
        return this;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public RegisterData withDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public RegisterUserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(RegisterUserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public RegisterData withUserInfo(RegisterUserInfo userInfo) {
        this.userInfo = userInfo;
        return this;
    }

}
