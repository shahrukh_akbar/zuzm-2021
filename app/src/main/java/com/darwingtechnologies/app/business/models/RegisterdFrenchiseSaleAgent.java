
package com.darwingtechnologies.app.business.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class RegisterdFrenchiseSaleAgent {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("designation_id")
    @Expose
    private Integer designationId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("email_verified_at")
    @Expose
    private Object emailVerifiedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("contact")
    @Expose
    private Object contact;
    @SerializedName("user_id")
    @Expose
    private Object userId;
    @SerializedName("otp_code")
    @Expose
    private Object otpCode;
    @SerializedName("corporate_user_id")
    @Expose
    private Object corporateUserId;
    @SerializedName("franchise_id")
    @Expose
    private Object franchiseId;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("central_token")
    @Expose
    private String centralToken;
    @SerializedName("central_user")
    @Expose
    private Integer centralUser;
    @SerializedName("cnic")
    @Expose
    private Object cnic;
    @SerializedName("permissions")
    @Expose
    private List<RegisterdFrenchisePermission> permissions = new ArrayList<>();
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("roles")
    @Expose
    private List<RegisterdFrenchiseRole> roles = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDesignationId() {
        return designationId;
    }

    public void setDesignationId(Integer designationId) {
        this.designationId = designationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getEmailVerifiedAt() {
        return emailVerifiedAt;
    }

    public void setEmailVerifiedAt(Object emailVerifiedAt) {
        this.emailVerifiedAt = emailVerifiedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getContact() {
        return contact;
    }

    public void setContact(Object contact) {
        this.contact = contact;
    }

    public Object getUserId() {
        return userId;
    }

    public void setUserId(Object userId) {
        this.userId = userId;
    }

    public Object getOtpCode() {
        return otpCode;
    }

    public void setOtpCode(Object otpCode) {
        this.otpCode = otpCode;
    }

    public Object getCorporateUserId() {
        return corporateUserId;
    }

    public void setCorporateUserId(Object corporateUserId) {
        this.corporateUserId = corporateUserId;
    }

    public Object getFranchiseId() {
        return franchiseId;
    }

    public void setFranchiseId(Object franchiseId) {
        this.franchiseId = franchiseId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCentralToken() {
        return centralToken;
    }

    public void setCentralToken(String centralToken) {
        this.centralToken = centralToken;
    }

    public Integer getCentralUser() {
        return centralUser;
    }

    public void setCentralUser(Integer centralUser) {
        this.centralUser = centralUser;
    }

    public Object getCnic() {
        return cnic;
    }

    public void setCnic(Object cnic) {
        this.cnic = cnic;
    }

    public List<RegisterdFrenchisePermission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<RegisterdFrenchisePermission> permissions) {
        this.permissions = permissions;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public List<RegisterdFrenchiseRole> getRoles() {
        return roles;
    }

    public void setRoles(List<RegisterdFrenchiseRole> roles) {
        this.roles = roles;
    }

}
