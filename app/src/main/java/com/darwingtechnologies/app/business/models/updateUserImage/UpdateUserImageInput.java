
package com.darwingtechnologies.app.business.models.updateUserImage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateUserImageInput {

    @SerializedName("user_id")
    @Expose
    private String userID;

    @SerializedName("profile_id")
    @Expose
    private String profileId;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }
}
