
package com.darwingtechnologies.app.business.models.searchBroker;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchBrokerInput {

    @SerializedName("listing_type")
    @Expose
    private String listingType;
    @SerializedName("term_id")
    @Expose
    private String termId;
    @SerializedName("keyword")
    @Expose
    private String keyword;

    public String getListingType() {
        return listingType;
    }

    public void setListingType(String listingType) {
        this.listingType = listingType;
    }

    public String getTermId() {
        return termId;
    }

    public void setTermId(String termId) {
        this.termId = termId;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
