
package com.darwingtechnologies.app.business.models.register;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterCaps {

    @SerializedName("subscriber")
    @Expose
    private Boolean subscriber;

    public Boolean getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(Boolean subscriber) {
        this.subscriber = subscriber;
    }

    public RegisterCaps withSubscriber(Boolean subscriber) {
        this.subscriber = subscriber;
        return this;
    }

}
