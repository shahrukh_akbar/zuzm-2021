
package com.darwingtechnologies.app.business.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AgentLoginCorporate {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("branch")
    @Expose
    private String branch;
    @SerializedName("sales_agent")
    @Expose
    private Object salesAgent;
    @SerializedName("contact")
    @Expose
    private String contact;
    @SerializedName("poc_account")
    @Expose
    private String pocAccount;
    @SerializedName("poc_admin")
    @Expose
    private String pocAdmin;
    @SerializedName("days")
    @Expose
    private Integer days;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("credit_limit")
    @Expose
    private String creditLimit;
    @SerializedName("balance")
    @Expose
    private String balance;
    @SerializedName("complete_info")
    @Expose
    private String completeInfo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public Object getSalesAgent() {
        return salesAgent;
    }

    public void setSalesAgent(Object salesAgent) {
        this.salesAgent = salesAgent;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getPocAccount() {
        return pocAccount;
    }

    public void setPocAccount(String pocAccount) {
        this.pocAccount = pocAccount;
    }

    public String getPocAdmin() {
        return pocAdmin;
    }

    public void setPocAdmin(String pocAdmin) {
        this.pocAdmin = pocAdmin;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getCompleteInfo() {
        return completeInfo;
    }

    public void setCompleteInfo(String completeInfo) {
        this.completeInfo = completeInfo;
    }

}
