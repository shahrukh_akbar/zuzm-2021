
package com.darwingtechnologies.app.business.models.getAllBroker;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetAllBrokerListInfo {

    @SerializedName("_job_tagline")
    @Expose
    private String jobTagline;
    @SerializedName("_job_cover")
    @Expose
    private List<String> jobCover = null;
    @SerializedName("_job_email")
    @Expose
    private String jobEmail;
    @SerializedName("_job_phone")
    @Expose
    private String jobPhone;
    @SerializedName("_whatsapp-number")
    @Expose
    private Object whatsappNumber;
    @SerializedName("_case27_listing_type")
    @Expose
    private String case27ListingType;
    @SerializedName("_job_location")
    @Expose
    private String jobLocation;
    @SerializedName("geolocation_lat")
    @Expose
    private String geolocationLat;
    @SerializedName("geolocation_long")
    @Expose
    private String geolocationLong;

    public String getJobTagline() {
        return jobTagline;
    }

    public void setJobTagline(String jobTagline) {
        this.jobTagline = jobTagline;
    }

    public List<String> getJobCover() {
        return jobCover;
    }

    public void setJobCover(List<String> jobCover) {
        this.jobCover = jobCover;
    }

    public String getJobEmail() {
        return jobEmail;
    }

    public void setJobEmail(String jobEmail) {
        this.jobEmail = jobEmail;
    }

    public String getJobPhone() {
        return jobPhone;
    }

    public void setJobPhone(String jobPhone) {
        this.jobPhone = jobPhone;
    }

    public Object getWhatsappNumber() {
        return whatsappNumber;
    }

    public void setWhatsappNumber(Object whatsappNumber) {
        this.whatsappNumber = whatsappNumber;
    }

    public String getCase27ListingType() {
        return case27ListingType;
    }

    public void setCase27ListingType(String case27ListingType) {
        this.case27ListingType = case27ListingType;
    }

    public String getJobLocation() {
        return jobLocation;
    }

    public void setJobLocation(String jobLocation) {
        this.jobLocation = jobLocation;
    }

    public String getGeolocationLat() {
        return geolocationLat;
    }

    public void setGeolocationLat(String geolocationLat) {
        this.geolocationLat = geolocationLat;
    }

    public String getGeolocationLong() {
        return geolocationLong;
    }

    public void setGeolocationLong(String geolocationLong) {
        this.geolocationLong = geolocationLong;
    }

}
