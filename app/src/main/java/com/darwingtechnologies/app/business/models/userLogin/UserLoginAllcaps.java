
package com.darwingtechnologies.app.business.models.userLogin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserLoginAllcaps {

    @SerializedName("read")
    @Expose
    private Boolean read;
    @SerializedName("level_0")
    @Expose
    private Boolean level0;
    @SerializedName("mylisting_can_add_listings")
    @Expose
    private Boolean mylistingCanAddListings;
    @SerializedName("mylisting_can_switch_role")
    @Expose
    private Boolean mylistingCanSwitchRole;
    @SerializedName("subscriber")
    @Expose
    private Boolean subscriber;

    public Boolean getRead() {
        return read;
    }

    public void setRead(Boolean read) {
        this.read = read;
    }

    public Boolean getLevel0() {
        return level0;
    }

    public void setLevel0(Boolean level0) {
        this.level0 = level0;
    }

    public Boolean getMylistingCanAddListings() {
        return mylistingCanAddListings;
    }

    public void setMylistingCanAddListings(Boolean mylistingCanAddListings) {
        this.mylistingCanAddListings = mylistingCanAddListings;
    }

    public Boolean getMylistingCanSwitchRole() {
        return mylistingCanSwitchRole;
    }

    public void setMylistingCanSwitchRole(Boolean mylistingCanSwitchRole) {
        this.mylistingCanSwitchRole = mylistingCanSwitchRole;
    }

    public Boolean getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(Boolean subscriber) {
        this.subscriber = subscriber;
    }

}
