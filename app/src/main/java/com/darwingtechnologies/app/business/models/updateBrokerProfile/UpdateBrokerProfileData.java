
package com.darwingtechnologies.app.business.models.updateBrokerProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class UpdateBrokerProfileData {

    @SerializedName("profile_id")
    @Expose
    private String profileId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("post_title")
    @Expose
    private String postTitle;
    @SerializedName("post_content")
    @Expose
    private String postContent;
    @SerializedName("job_tagline")
    @Expose
    private String jobTagline;
    @SerializedName("job_email")
    @Expose
    private String jobEmail;
    @SerializedName("job_phone")
    @Expose
    private String jobPhone;
    @SerializedName("job_cover")
    @Expose
    private String jobCover;
    @SerializedName("job_logo")
    @Expose
    private String jobLogo;
    @SerializedName("whatsapp-number")
    @Expose
    private String whatsappNumber;
    @SerializedName("case27_listing_type")
    @Expose
    private String case27ListingType;
    @SerializedName("job_location")
    @Expose
    private String jobLocation;
    @SerializedName("geolocation_lat")
    @Expose
    private String geolocationLat;
    @SerializedName("geolocation_long")
    @Expose
    private String geolocationLong;

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public String getJobTagline() {
        return jobTagline;
    }

    public void setJobTagline(String jobTagline) {
        this.jobTagline = jobTagline;
    }

    public String getJobEmail() {
        return jobEmail;
    }

    public void setJobEmail(String jobEmail) {
        this.jobEmail = jobEmail;
    }

    public String getJobPhone() {
        return jobPhone;
    }

    public void setJobPhone(String jobPhone) {
        this.jobPhone = jobPhone;
    }

    public String getJobCover() {
        return jobCover;
    }

    public void setJobCover(String jobCover) {
        this.jobCover = jobCover;
    }

    public String getJobLogo() {
        return jobLogo;
    }

    public void setJobLogo(String jobLogo) {
        this.jobLogo = jobLogo;
    }

    public String getWhatsappNumber() {
        return whatsappNumber;
    }

    public void setWhatsappNumber(String whatsappNumber) {
        this.whatsappNumber = whatsappNumber;
    }

    public String getCase27ListingType() {
        return case27ListingType;
    }

    public void setCase27ListingType(String case27ListingType) {
        this.case27ListingType = case27ListingType;
    }

    public String getJobLocation() {
        return jobLocation;
    }

    public void setJobLocation(String jobLocation) {
        this.jobLocation = jobLocation;
    }

    public String getGeolocationLat() {
        return geolocationLat;
    }

    public void setGeolocationLat(String geolocationLat) {
        this.geolocationLat = geolocationLat;
    }

    public String getGeolocationLong() {
        return geolocationLong;
    }

    public void setGeolocationLong(String geolocationLong) {
        this.geolocationLong = geolocationLong;
    }

}
