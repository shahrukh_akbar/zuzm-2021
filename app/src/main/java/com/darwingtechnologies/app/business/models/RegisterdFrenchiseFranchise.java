
package com.darwingtechnologies.app.business.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterdFrenchiseFranchise {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("franchise_id")
    @Expose
    private Integer franchiseId;
    @SerializedName("added_by")
    @Expose
    private Integer addedBy;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("sale_agent")
    @Expose
    private RegisterdFrenchiseSaleAgent saleAgent;
    @SerializedName("franchise")
    @Expose
    private RegisterdFrenchiseSubFranchise franchise;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFranchiseId() {
        return franchiseId;
    }

    public void setFranchiseId(Integer franchiseId) {
        this.franchiseId = franchiseId;
    }

    public Integer getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(Integer addedBy) {
        this.addedBy = addedBy;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public RegisterdFrenchiseSaleAgent getSaleAgent() {
        return saleAgent;
    }

    public void setSaleAgent(RegisterdFrenchiseSaleAgent saleAgent) {
        this.saleAgent = saleAgent;
    }

    public RegisterdFrenchiseSubFranchise getFranchise() {
        return franchise;
    }

    public void setFranchise(RegisterdFrenchiseSubFranchise franchise) {
        this.franchise = franchise;
    }

}
