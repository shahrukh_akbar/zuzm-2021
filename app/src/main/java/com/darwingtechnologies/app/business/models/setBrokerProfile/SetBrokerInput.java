package com.darwingtechnologies.app.business.models.setBrokerProfile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SetBrokerInput {


    @SerializedName("post_title")
    @Expose
    private String post_title;//: Aslam

    @SerializedName("post_content")
    @Expose
    private String post_content;//:tour Guide

    @SerializedName("cat")
    @Expose
    private String cat;//:33

    @SerializedName("tagline")
    @Expose
    private String tagline;//:work eith us

    @SerializedName("email")
    @Expose
    private String email;//:ranaumair455@gmail.com

    @SerializedName("phone")
    @Expose
    private String phone;//:03038505028

    @SerializedName("whatsapp_number")
    @Expose
    private String whatsapp_number;//:03038505028

    @SerializedName("listing_type")
    @Expose
    private String listing_type;//:for-sale

    @SerializedName("broker_location")
    @Expose
    private String broker_location;//:

    @SerializedName("geolocation_lat")
    @Expose
    private String geolocation_lat;//:

    @SerializedName("geolocation_long")
    @Expose
    private String geolocation_long;//:


    @SerializedName("username")
    @Expose
    private String username;//:Hamza
    @SerializedName("password")
    @Expose
    private String password;//:123456789

    public String getPost_title() {
        return post_title;
    }

    public void setPost_title(String post_title) {
        this.post_title = post_title;
    }

    public String getPost_content() {
        return post_content;
    }

    public void setPost_content(String post_content) {
        this.post_content = post_content;
    }

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWhatsapp_number() {
        return whatsapp_number;
    }

    public void setWhatsapp_number(String whatsapp_number) {
        this.whatsapp_number = whatsapp_number;
    }

    public String getListing_type() {
        return listing_type;
    }

    public void setListing_type(String listing_type) {
        this.listing_type = listing_type;
    }

    public String getBroker_location() {
        return broker_location;
    }

    public void setBroker_location(String broker_location) {
        this.broker_location = broker_location;
    }

    public String getGeolocation_lat() {
        return geolocation_lat;
    }

    public void setGeolocation_lat(String geolocation_lat) {
        this.geolocation_lat = geolocation_lat;
    }

    public String getGeolocation_long() {
        return geolocation_long;
    }

    public void setGeolocation_long(String geolocation_long) {
        this.geolocation_long = geolocation_long;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
