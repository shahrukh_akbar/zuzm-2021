
package com.darwingtechnologies.app.business.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AgentLoginData {

    @SerializedName("user-info")
    @Expose
    private AgentLoginUserInfo userInfo;
    @SerializedName("corporates")
    @Expose
    private ArrayList<AgentLoginCorporate> corporates = new ArrayList<>();

    public AgentLoginUserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(AgentLoginUserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public ArrayList<AgentLoginCorporate> getCorporates() {
        return corporates;
    }

    public void setCorporates(ArrayList<AgentLoginCorporate> corporates) {
        this.corporates = corporates;
    }

}
