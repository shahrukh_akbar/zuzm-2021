
package com.darwingtechnologies.app.business.models.register;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterResponse {

    @SerializedName("data")
    @Expose
    private RegisterData data;
    @SerializedName("ID")
    @Expose
    private Long id;
    @SerializedName("caps")
    @Expose
    private RegisterCaps caps;
    @SerializedName("cap_key")
    @Expose
    private String capKey;
    @SerializedName("roles")
    @Expose
    private List<String> roles = null;
    @SerializedName("allcaps")
    @Expose
    private RegisterAllcaps allcaps;
    @SerializedName("filter")
    @Expose
    private Object filter;

    @SerializedName("error")
    @Expose
    private String error;

    public RegisterData getData() {
        return data;
    }

    public void setData(RegisterData data) {
        this.data = data;
    }

    public RegisterResponse withData(RegisterData data) {
        this.data = data;
        return this;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RegisterResponse withId(Long id) {
        this.id = id;
        return this;
    }

    public RegisterCaps getCaps() {
        return caps;
    }

    public void setCaps(RegisterCaps caps) {
        this.caps = caps;
    }

    public RegisterResponse withCaps(RegisterCaps caps) {
        this.caps = caps;
        return this;
    }

    public String getCapKey() {
        return capKey;
    }

    public void setCapKey(String capKey) {
        this.capKey = capKey;
    }

    public RegisterResponse withCapKey(String capKey) {
        this.capKey = capKey;
        return this;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public RegisterResponse withRoles(List<String> roles) {
        this.roles = roles;
        return this;
    }

    public RegisterAllcaps getAllcaps() {
        return allcaps;
    }

    public void setAllcaps(RegisterAllcaps allcaps) {
        this.allcaps = allcaps;
    }

    public RegisterResponse withAllcaps(RegisterAllcaps allcaps) {
        this.allcaps = allcaps;
        return this;
    }

    public Object getFilter() {
        return filter;
    }

    public void setFilter(Object filter) {
        this.filter = filter;
    }

    public RegisterResponse withFilter(Object filter) {
        this.filter = filter;
        return this;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
