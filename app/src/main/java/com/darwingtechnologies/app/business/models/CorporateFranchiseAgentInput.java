
package com.darwingtechnologies.app.business.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CorporateFranchiseAgentInput {

    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("collect_amount")
    @Expose
    private String collectAmount ;
    @SerializedName("franchise_address")
    @Expose
    private String franchiseAddress ;
    @SerializedName("franchise_user_name")
    @Expose
    private String franchiseUserName ;
    @SerializedName("password")
    @Expose
    private String password ;

    @SerializedName("cnic")
    @Expose
    private String cnic ;
    @SerializedName("phone")
    @Expose
    private String phone ;
    @SerializedName("lng")
    @Expose
    private String lng ;
    @SerializedName("lat")
    @Expose
    private String lat ;
    @SerializedName("franchise_name")
    @Expose
    private String franchiseName ;
    @SerializedName("franchise_id")
    @Expose
    private String franchiseId ;
    @SerializedName("corporate_id")
    @Expose
    private String corporateId ;
    @SerializedName("subscription_fee")
    @Expose
    private String subscriptionFee ;

    public String getSubscriptionFee() {
        return subscriptionFee;
    }

    public void setSubscriptionFee(String subscriptionFee) {
        this.subscriptionFee = subscriptionFee;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCollectAmount() {
        return collectAmount;
    }

    public void setCollectAmount(String collectAmount) {
        this.collectAmount = collectAmount;
    }

    public String getFranchiseAddress() {
        return franchiseAddress;
    }

    public void setFranchiseAddress(String franchiseAddress) {
        this.franchiseAddress = franchiseAddress;
    }

    public String getFranchiseUserName() {
        return franchiseUserName;
    }

    public void setFranchiseUserName(String franchiseUserName) {
        this.franchiseUserName = franchiseUserName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getFranchiseName() {
        return franchiseName;
    }

    public void setFranchiseName(String franchiseName) {
        this.franchiseName = franchiseName;
    }

    public String getFranchiseId() {
        return franchiseId;
    }

    public void setFranchiseId(String franchiseId) {
        this.franchiseId = franchiseId;
    }

    public String getCorporateId() {
        return corporateId;
    }

    public void setCorporateId(String corporateId) {
        this.corporateId = corporateId;
    }
}
