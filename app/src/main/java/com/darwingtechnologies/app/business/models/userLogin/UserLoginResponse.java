
package com.darwingtechnologies.app.business.models.userLogin;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserLoginResponse {

    @SerializedName("data")
    @Expose
    private UserLoginData data;
    @SerializedName("ID")
    @Expose
    private Integer id;
    @SerializedName("caps")
    @Expose
    private UserLoginCaps caps;
    @SerializedName("cap_key")
    @Expose
    private String capKey;
    @SerializedName("roles")
    @Expose
    private List<String> roles = null;
    @SerializedName("allcaps")
    @Expose
    private UserLoginAllcaps allcaps;
    @SerializedName("filter")
    @Expose
    private Object filter;

    public UserLoginData getData() {
        return data;
    }

    public void setData(UserLoginData data) {
        this.data = data;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserLoginCaps getCaps() {
        return caps;
    }

    public void setCaps(UserLoginCaps caps) {
        this.caps = caps;
    }

    public String getCapKey() {
        return capKey;
    }

    public void setCapKey(String capKey) {
        this.capKey = capKey;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public UserLoginAllcaps getAllcaps() {
        return allcaps;
    }

    public void setAllcaps(UserLoginAllcaps allcaps) {
        this.allcaps = allcaps;
    }

    public Object getFilter() {
        return filter;
    }

    public void setFilter(Object filter) {
        this.filter = filter;
    }

}
