
package com.darwingtechnologies.app.business.models.userLogin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserLoginCaps {

    @SerializedName("subscriber")
    @Expose
    private Boolean subscriber;
    @SerializedName("customer")
    @Expose
    private Boolean customer;
    @SerializedName("broker")
    @Expose
    private Boolean broker;

    public Boolean getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(Boolean subscriber) {
        this.subscriber = subscriber;
    }

    public Boolean getBroker() {
        return broker;
    }

    public void setBroker(Boolean broker) {
        this.broker = broker;
    }

    public Boolean getCustomer() {
        return customer;
    }

    public void setCustomer(Boolean customer) {
        this.customer = customer;
    }
}
