
package com.darwingtechnologies.app.business.models.register;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterAllcaps {

    @SerializedName("read")
    @Expose
    private Boolean read;
    @SerializedName("level_0")
    @Expose
    private Boolean level0;
    @SerializedName("mylisting_can_add_listings")
    @Expose
    private Boolean mylistingCanAddListings;
    @SerializedName("mylisting_can_switch_role")
    @Expose
    private Boolean mylistingCanSwitchRole;
    @SerializedName("subscriber")
    @Expose
    private Boolean subscriber;

    public Boolean getRead() {
        return read;
    }

    public void setRead(Boolean read) {
        this.read = read;
    }

    public RegisterAllcaps withRead(Boolean read) {
        this.read = read;
        return this;
    }

    public Boolean getLevel0() {
        return level0;
    }

    public void setLevel0(Boolean level0) {
        this.level0 = level0;
    }

    public RegisterAllcaps withLevel0(Boolean level0) {
        this.level0 = level0;
        return this;
    }

    public Boolean getMylistingCanAddListings() {
        return mylistingCanAddListings;
    }

    public void setMylistingCanAddListings(Boolean mylistingCanAddListings) {
        this.mylistingCanAddListings = mylistingCanAddListings;
    }

    public RegisterAllcaps withMylistingCanAddListings(Boolean mylistingCanAddListings) {
        this.mylistingCanAddListings = mylistingCanAddListings;
        return this;
    }

    public Boolean getMylistingCanSwitchRole() {
        return mylistingCanSwitchRole;
    }

    public void setMylistingCanSwitchRole(Boolean mylistingCanSwitchRole) {
        this.mylistingCanSwitchRole = mylistingCanSwitchRole;
    }

    public RegisterAllcaps withMylistingCanSwitchRole(Boolean mylistingCanSwitchRole) {
        this.mylistingCanSwitchRole = mylistingCanSwitchRole;
        return this;
    }

    public Boolean getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(Boolean subscriber) {
        this.subscriber = subscriber;
    }

    public RegisterAllcaps withSubscriber(Boolean subscriber) {
        this.subscriber = subscriber;
        return this;
    }

}
