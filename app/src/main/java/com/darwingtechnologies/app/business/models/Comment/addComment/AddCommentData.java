
package com.darwingtechnologies.app.business.models.Comment.addComment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class AddCommentData {

    @SerializedName("comment_post_ID")
    @Expose
    private String commentPostID;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("comment_author")
    @Expose
    private String commentAuthor;
    @SerializedName("comment_author_email")
    @Expose
    private String commentAuthorEmail;
    @SerializedName("comment_author_url")
    @Expose
    private String commentAuthorUrl;
    @SerializedName("comment_content")
    @Expose
    private String commentContent;
    @SerializedName("comment_author_IP")
    @Expose
    private String commentAuthorIP;
    @SerializedName("comment_agent")
    @Expose
    private String commentAgent;
    @SerializedName("comment_date")
    @Expose
    private String commentDate;
    @SerializedName("comment_approved")
    @Expose
    private Integer commentApproved;

    public String getCommentPostID() {
        return commentPostID;
    }

    public void setCommentPostID(String commentPostID) {
        this.commentPostID = commentPostID;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getCommentAuthor() {
        return commentAuthor;
    }

    public void setCommentAuthor(String commentAuthor) {
        this.commentAuthor = commentAuthor;
    }

    public String getCommentAuthorEmail() {
        return commentAuthorEmail;
    }

    public void setCommentAuthorEmail(String commentAuthorEmail) {
        this.commentAuthorEmail = commentAuthorEmail;
    }

    public String getCommentAuthorUrl() {
        return commentAuthorUrl;
    }

    public void setCommentAuthorUrl(String commentAuthorUrl) {
        this.commentAuthorUrl = commentAuthorUrl;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public String getCommentAuthorIP() {
        return commentAuthorIP;
    }

    public void setCommentAuthorIP(String commentAuthorIP) {
        this.commentAuthorIP = commentAuthorIP;
    }

    public String getCommentAgent() {
        return commentAgent;
    }

    public void setCommentAgent(String commentAgent) {
        this.commentAgent = commentAgent;
    }

    public String getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(String commentDate) {
        this.commentDate = commentDate;
    }

    public Integer getCommentApproved() {
        return commentApproved;
    }

    public void setCommentApproved(Integer commentApproved) {
        this.commentApproved = commentApproved;
    }

}
