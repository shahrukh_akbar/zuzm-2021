package com.darwingtechnologies.app.business.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImageFrenchiesInput {

    @SerializedName("franchise_id")
    @Expose
    private String franchiseId = "";

//    @SerializedName("image")
//    @Expose
//    private List<String> image = new ArrayList<>();

    public String getFranchiseId() {
        return franchiseId;
    }

    public void setFranchiseId(String franchiseId) {
        this.franchiseId = franchiseId;
    }

//    public List<String> getImage() {
//        return image;
//    }
//
//    public void setImage(List<String> image) {
//        this.image = image;
//    }
}
