package com.darwingtechnologies.app.business.handlers;

import android.content.Context;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.darwingtechnologies.app.BuildConfig;
import com.darwingtechnologies.app.R;
import com.darwingtechnologies.app.activities.MainActivity;
import com.darwingtechnologies.app.business.models.AppConfig;
import com.darwingtechnologies.app.business.models.ParamFile;
import com.darwingtechnologies.app.business.models.StatusCodeResponse;
import com.darwingtechnologies.app.utilities.CommonMethods;
import com.darwingtechnologies.app.utilities.CommonObjects;
import com.darwingtechnologies.app.utilities.Constants;
import com.google.gson.Gson;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * Created by Rana Zeshan on 27-Jun-19.
 */

public class CallForServer {

    private String url;
    private OnServerResultNotifier onServerResultNotifier;
    private Map<String, String> paramStrings = new HashMap<>();
    private String NO_INTERNET = "No internet connection";
    private static String BASE_URL="";
    private static String API_BASE_URL = BASE_URL;//+"api/";
    private ArrayList<ParamFile> paramFiles = new ArrayList<>();
    private static OkHttpClient client;
    private static boolean isShown;



    public CallForServer(String url, OnServerResultNotifier onServerResultNotifier, Map<String, String> paramStrings) {
        this.url = url;
        this.paramStrings=paramStrings;
        this.onServerResultNotifier = onServerResultNotifier;
    }


    public CallForServer(String url, OnServerResultNotifier onServerResultNotifier) {
        this.url = url;
        this.onServerResultNotifier = onServerResultNotifier;
    }
    public CallForServer(
            String url,
            OnServerResultNotifier onServerResultNotifier,
            Map<String, String> paramStrings,
            ArrayList<ParamFile> paramFiles) {
        this.url = url;
        this.onServerResultNotifier = onServerResultNotifier;
        this.paramStrings = paramStrings;
        this.paramFiles = paramFiles;
    }

    public interface OnServerResultNotifier {
        public void onServerResultNotifier(boolean isError, String response);
    }

    //Load data from server
    public void callForServerGet() {
        if (CommonMethods.isNetworkAvailable(CommonObjects.getContext())) {
            AndroidNetworking.initialize(CommonObjects.getContext());
            OkHttpClient client = new OkHttpClient()
                    .newBuilder()
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .readTimeout(20, TimeUnit.SECONDS)
                    .build();
            AndroidNetworking.initialize(CommonObjects.getContext(),client);
            ANRequest.GetRequestBuilder getRequestBuilder = AndroidNetworking.get(API_BASE_URL+url);
            Log.d("Service_Response", url);
//            if(CommonObjects.getLogInResponse()!=null)
//            {
//                getRequestBuilder.addHeaders(Constants.Keys.AUTHORIZATION, Constants.Keys.BEARER+CommonObjects.getLogInResponse().getData().getToken());
//                getRequestBuilder.addHeaders(Constants.Keys.UDID, CommonObjects.getLogInResponse().getData().getCode().getUdid());
//
//                Log.e("Auth",""+Constants.Keys.BEARER+CommonObjects.getLogInResponse().getData().getToken() +"\n UDID" +CommonObjects.getLogInResponse().getData().getCode().getUdid());
//            }
//            else
//            {
//                getRequestBuilder.addHeaders(Constants.Keys.UDID, CommonMethods.getStringPreference(CommonObjects.getContext(),CommonObjects.getContext().getString(R.string.app_name), Constants.Keys.UDID,""));
//            }
            getRequestBuilder.addHeaders("Accept", "application/json");
            getRequestBuilder.build().getAsString(new StringRequestListener() {
                @Override
                public void onResponse(String jsonStr) {
                        onServerResultNotifier.onServerResultNotifier(false, jsonStr);
                    Log.e("JSON",""+jsonStr);
                }

                @Override
                public void onError(ANError error) {
                    String err="Unable to get data from server";
                    if(error.getErrorBody()!=null)
                    {
                        err=error.getErrorBody();
                    }
                    onServerResultNotifier.onServerResultNotifier(true,err);
                    Log.e("Erorr ",""+err.toString());

                }
            });
        } else {
            onServerResultNotifier.onServerResultNotifier(true, NO_INTERNET);
            Log.e("No Internet ",""+NO_INTERNET.toString());

        }
    }

    //Load data from server
    public void callForServerGetScrapper() {
        if (CommonMethods.isNetworkAvailable(CommonObjects.getContext())) {

            AndroidNetworking.initialize(CommonObjects.getContext());
            OkHttpClient client = new OkHttpClient()
                    .newBuilder()
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .readTimeout(20, TimeUnit.SECONDS)
                    .build();

            AndroidNetworking.initialize(CommonObjects.getContext(),client);

            ANRequest.GetRequestBuilder getRequestBuilder = AndroidNetworking.get(url);


            Log.d("Service_Response", url);

            getRequestBuilder.build().getAsString(new StringRequestListener() {

                @Override
                public void onResponse(String jsonStr) {
                    onServerResultNotifier.onServerResultNotifier(false, jsonStr);
                    Log.e("WebResponse",""+jsonStr);
                }

                @Override
                public void onError(ANError error) {
                    String err="Unable to get data from server";
                    if(error.getErrorBody()!=null)
                    {
                        err=error.getErrorBody();
                    }
                    onServerResultNotifier.onServerResultNotifier(true,err);
                    Log.e("Erorr ",""+err.toString());

                }
            });

        } else {
            onServerResultNotifier.onServerResultNotifier(true, NO_INTERNET);
            Log.e("No Internet ",""+NO_INTERNET.toString());

        }
    }
    // Load data from server
    public void callForServerPost() {
        if (CommonMethods.isNetworkAvailable(CommonObjects.getContext()))
        {
            AndroidNetworking.initialize(CommonObjects.getContext());
            AndroidNetworking.initialize(CommonObjects.getContext(), getSingleClient());
            ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(url);
            //            Log.d("Service_Response", url);
            paramStrings.put("device_type", Constants.DEVICE_TYPE);

            postRequestBuilder.addHeaders("Accept", "application/json");
                postRequestBuilder.addHeaders(getHeaders());
            postRequestBuilder.addBodyParameter(paramStrings);

            postRequestBuilder
                    .build()
                    .getAsString(
                            new StringRequestListener() {
                                @Override
                                public void onResponse(String jsonStr) {
                                    Log.d("Service_Response", jsonStr);

                                    try {
                                        StatusCodeResponse statusCodeResponse =
                                                new Gson()
                                                        .fromJson(
                                                                jsonStr,
                                                                StatusCodeResponse.class);

                                        if (statusCodeResponse.getSuccess() && statusCodeResponse.getStatus() == 200 && statusCodeResponse.getAppConfig() == null) {
                                            onServerResultNotifier.onServerResultNotifier(false, jsonStr);
                                        } else if(statusCodeResponse.getStatus() != 200) {
                                            if (statusCodeResponse.getStatus() == 498) {
                                                logOut();
                                            } else if(statusCodeResponse.getStatus() == 419){
//                                                SocialUserInfoData socialiteLoginData = CommonMethods.getUserData();
//                                                socialiteLoginData.setToken(statusCodeResponse.getToken());
//                                                CommonMethods.setStringPreference(
//                                                        CommonObjects.getContext(),
//                                                        CommonObjects.getContext()
//                                                                .getResources()
//                                                                .getString(R.string.file_name),
//                                                        CommonObjects.getContext()
//                                                                .getResources()
//                                                                .getString(
//                                                                        R.string.socialite_login_data),
//                                                        new Gson().toJson(socialiteLoginData));
                                                CommonMethods.setStringPreference(
                                                        CommonObjects.getContext(),
                                                        CommonObjects.getContext()
                                                                .getResources()
                                                                .getString(R.string.file_name),
                                                        CommonObjects.getContext()
                                                                .getResources()
                                                                .getString(R.string.socialite_token),
                                                        statusCodeResponse.getToken());
                                                callForServerPost();
                                            }
                                        }
                                        else
                                        {
                                            if(statusCodeResponse.getAppConfig() == null) {
                                                onServerResultNotifier.onServerResultNotifier(false, jsonStr);
                                            }
                                            else
                                            {
                                                if( getAppVersion() < statusCodeResponse.getAppConfig().getCurrentVersion() &&  getAppVersion() >= statusCodeResponse.getAppConfig().getMinVersion())
                                                {
                                                    showMessageUpdateMessage(CommonObjects.getContext(),true);
                                                }
                                                else if(getAppVersion() < statusCodeResponse.getAppConfig().getMinVersion())
                                                {
                                                    showMessageUpdateMessage(CommonObjects.getContext(),false);
                                                }
                                                onServerResultNotifier.onServerResultNotifier(false, jsonStr);
                                            }
                                        }

                                    }
                                    catch (Exception e)
                                    {
                                        onServerResultNotifier.onServerResultNotifier(false, jsonStr);
                                    }
                                }

                                @Override
                                public void onError(ANError error) {
                                    String err = "Something went wrong, please try again.";
                                    if (error.getErrorBody() != null) {
                                        err = error.getErrorBody();
                                    }else if(!CommonMethods.isNetworkAvailable(CommonObjects.getContext())){
                                        err="Please check your internet connection & try again.";
                                    }
                                    onServerResultNotifier.onServerResultNotifier(true, err);
                                }
                            });
        }

        else {
            onServerResultNotifier.onServerResultNotifier(true, NO_INTERNET);
        }
    }

    public void callForServerFilePost() {
        if (CommonMethods.isNetworkAvailable(CommonObjects.getContext())) {
            AndroidNetworking.initialize(CommonObjects.getContext());
            AndroidNetworking.initialize(CommonObjects.getContext(),getSingleClient());
            ANRequest.MultiPartBuilder multiPartBuilder = AndroidNetworking.upload(url);
            multiPartBuilder.addHeaders("Accept", "application/json");

            multiPartBuilder.addHeaders(getHeaders());

            //            Log.d("Service_Response", url);
            for (ParamFile paramFile : paramFiles) {
                Log.d(
                        "Service_Response",
                        paramFile.getKey() + " " + paramFile.getFile().getAbsolutePath());
                multiPartBuilder.addMultipartFile(paramFile.getKey(), paramFile.getFile());
            }
            multiPartBuilder.addMultipartParameter(paramStrings);
            multiPartBuilder
                    .build()
                    .getAsString(
                            new StringRequestListener() {
                                @Override
                                public void onResponse(String jsonStr) {
                                    Log.d("Service_Response", jsonStr);
                                    try {
                                        StatusCodeResponse statusCodeResponse =
                                                new Gson()
                                                        .fromJson(
                                                                jsonStr,
                                                                StatusCodeResponse.class);

                                        if (statusCodeResponse.getSuccess() && statusCodeResponse.getStatus() == 200 && statusCodeResponse.getAppConfig() == null) {
                                            onServerResultNotifier.onServerResultNotifier(false, jsonStr);
                                        } else if(statusCodeResponse.getStatus() != 200) {
                                            if (statusCodeResponse.getStatus() == 498) {
                                                logOut();
                                            } else if(statusCodeResponse.getStatus() == 419){
//                                                SocialUserInfoData socialiteLoginData = CommonMethods.getUserData();
//                                                socialiteLoginData.setToken(statusCodeResponse.getToken());
//                                                CommonMethods.setStringPreference(
//                                                        CommonObjects.getContext(),
//                                                        CommonObjects.getContext()
//                                                                .getResources()
//                                                                .getString(R.string.file_name),
//                                                        CommonObjects.getContext()
//                                                                .getResources()
//                                                                .getString(
//                                                                        R.string.socialite_login_data),
//                                                        new Gson().toJson(socialiteLoginData));
                                                CommonMethods.setStringPreference(
                                                        CommonObjects.getContext(),
                                                        CommonObjects.getContext()
                                                                .getResources()
                                                                .getString(R.string.file_name),
                                                        CommonObjects.getContext()
                                                                .getResources()
                                                                .getString(R.string.socialite_token),
                                                        statusCodeResponse.getToken());
                                                callForServerFilePost();
                                            }
                                        }
                                        else
                                        {
                                            if(statusCodeResponse.getAppConfig() == null) {
                                                onServerResultNotifier.onServerResultNotifier(false, jsonStr);
                                            }
                                            else
                                            {
                                                if( getAppVersion() < statusCodeResponse.getAppConfig().getCurrentVersion() &&  getAppVersion() >= statusCodeResponse.getAppConfig().getMinVersion())
                                                {
                                                    showMessageUpdateMessage(CommonObjects.getContext(),true);
                                                }
                                                else if(getAppVersion() < statusCodeResponse.getAppConfig().getMinVersion())
                                                {
                                                    showMessageUpdateMessage(CommonObjects.getContext(),false);
                                                }
                                                onServerResultNotifier.onServerResultNotifier(false, jsonStr);
                                            }
                                        }

                                    }
                                    catch (Exception e)
                                    {
                                        onServerResultNotifier.onServerResultNotifier(false, jsonStr);
                                    }

                                }

                                @Override
                                public void onError(ANError error) {
                                    String err = "Something went wrong, please try again.";
                                    if (error.getErrorBody() != null) {
                                        err = error.getErrorBody();
                                    }else if(!CommonMethods.isNetworkAvailable(CommonObjects.getContext())){
                                        err="Please check your internet connection & try again.";
                                    }
                                    onServerResultNotifier.onServerResultNotifier(true, err);
                                }
                            });
        } else {
            onServerResultNotifier.onServerResultNotifier(true, NO_INTERNET);
        }
    }

    private void logOut()
    {

        CommonMethods.setBooleanPreference(
                CommonObjects.getContext(),
                CommonObjects.getContext().getResources().getString(R.string.file_name),
                "isGuest",
                false);

        CommonMethods.setBooleanPreference(
                CommonObjects.getContext(),
                CommonObjects.getContext().getResources().getString(R.string.file_name),
                "isUserFromLogin",
                false);

        CommonMethods.setBooleanPreference(
                CommonObjects.getContext(),
                CommonObjects.getContext().getResources().getString(R.string.file_name),
                "isUserFromBasicProfile",
                false);
//        LinphonePreferences.instance().setFirstLaunchDefault(true);
//        CommonObjects.getContext().startActivity(new Intent().setClass(CommonObjects.getContext(), AccountCreationAssistantActivityNew.class));
        ((MainActivity)CommonObjects.getContext()).finish();
    }

    private OkHttpClient getSingleClient()
    {
        if(client == null) {
            client =
                    new OkHttpClient()
                            .newBuilder()
                            .connectTimeout(1, TimeUnit.MINUTES)
                            .readTimeout(1, TimeUnit.MINUTES)
                            .build();
        }
        return client;
    }

    private HashMap<String,String> getHeaders()
    {

        HashMap<String,String> headers=new HashMap<>();
//        if(CommonMethods.getUserData() != null)
//        {
//            headers.put("Authorization","Bearer "+CommonMethods.getUserData().getToken());
//        }
        if(CommonMethods.getUserCentralToken((FragmentActivity) CommonObjects.getContext()) != null)
        {

            headers.put("token",CommonMethods.getUserCentralToken((FragmentActivity) CommonObjects.getContext()));
        }
        headers.put("device_type","Android");
        headers.put("devicetype","android");
        headers.put("version", getAppVersion()+"");
        headers.put("phone","");//"CommonMethods.getUserName()");
        headers.put("password","");//CommonMethods.getPassword());
        return headers;
    }

    private void showMessageUpdateMessage(Context context, boolean canCancel) {
        if(canCancel)
        {
            isShown = CommonMethods.getBooleanPreference(context,context.getString(R.string.app_name), AppConfig.class.getSimpleName()+" "+CommonMethods.getTodaysDate(),false);
        }
        if(!isShown) {
            isShown = true;
            AlertDialog.Builder alertDialogBuilder =
                    new AlertDialog.Builder(context, R.style.Base_Theme_AppCompat_Light_Dialog_Alert);
            String message = "This app is outdated. Please, update it to the latest version";
            if (canCancel) {
                CommonMethods.setBooleanPreference(context,context.getString(R.string.app_name), AppConfig.class.getSimpleName()+" "+CommonMethods.getTodaysDate(),true);
                message = "A new update of app is available. Please, update it to the latest version";
            }
//            alertDialogBuilder.setMessage(message);
//            alertDialogBuilder.setCancelable(canCancel);
//            alertDialogBuilder.setPositiveButton(
//                    "Update Now", (dialogInterface, i) -> {
//                        final String appPackageName = context.getPackageName(); // getPackageName() from Context or Activity object
//                        try {
//                            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
//                        } catch (android.content.ActivityNotFoundException anfe) {
//                            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
//                        }
//                    });
//            if (canCancel) {
//                alertDialogBuilder.setNegativeButton(
//                        "Later", (dialogInterface, i) -> dialogInterface.cancel());
//            }


            final AlertDialog alertDialog = alertDialogBuilder.create();

            alertDialog.show();
        }
    }

    private Integer getAppVersion()
    {
        return Integer.valueOf(BuildConfig.VERSION_NAME.replace(".",""));
    }
}
