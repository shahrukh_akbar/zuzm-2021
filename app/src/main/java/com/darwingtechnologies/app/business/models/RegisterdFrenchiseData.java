
package com.darwingtechnologies.app.business.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class RegisterdFrenchiseData {

    @SerializedName("franchise")
    @Expose
    private List<RegisterdFrenchiseFranchise> franchise = new ArrayList<>();

    public List<RegisterdFrenchiseFranchise> getFranchise() {
        return franchise;
    }

    public void setFranchise(List<RegisterdFrenchiseFranchise> franchise) {
        this.franchise = franchise;
    }

}
