
package com.darwingtechnologies.app.business.models.updateBrokerProfileImage;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class UpdateBrokerProfileImageData {

    @SerializedName("profile_id")
    @Expose
    private String profileId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("job_logo")
    @Expose
    private String jobLogo;

    @SerializedName("job_cover")
    @Expose
    private String jobCover;

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getJobLogo() {
        return jobLogo;
    }

    public void setJobLogo(String jobLogo) {
        this.jobLogo = jobLogo;
    }

    public String getJobCover() {
        return jobCover;
    }

    public void setJobCover(String jobCover) {
        this.jobCover = jobCover;
    }
}
