
package com.darwingtechnologies.app.business.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterdFrenchiseSubFranchise {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("corporate_user_id")
    @Expose
    private Integer corporateUserId;
    @SerializedName("franchise_id")
    @Expose
    private String franchiseId;
    @SerializedName("franchise_name")
    @Expose
    private String franchiseName;
    @SerializedName("franchise_address")
    @Expose
    private String franchiseAddress;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("balance")
    @Expose
    private String balance;
    @SerializedName("lat")
    @Expose
    private Double lat;
    @SerializedName("lng")
    @Expose
    private Double lng;
    @SerializedName("first_subscription")
    @Expose
    private Object firstSubscription;
    @SerializedName("complete_info")
    @Expose
    private String completeInfo;
    @SerializedName("admin")
    @Expose
    private RegisterdFrenchiseAdmin admin;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCorporateUserId() {
        return corporateUserId;
    }

    public void setCorporateUserId(Integer corporateUserId) {
        this.corporateUserId = corporateUserId;
    }

    public String getFranchiseId() {
        return franchiseId;
    }

    public void setFranchiseId(String franchiseId) {
        this.franchiseId = franchiseId;
    }

    public String getFranchiseName() {
        return franchiseName;
    }

    public void setFranchiseName(String franchiseName) {
        this.franchiseName = franchiseName;
    }

    public String getFranchiseAddress() {
        return franchiseAddress;
    }

    public void setFranchiseAddress(String franchiseAddress) {
        this.franchiseAddress = franchiseAddress;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Object getFirstSubscription() {
        return firstSubscription;
    }

    public void setFirstSubscription(Object firstSubscription) {
        this.firstSubscription = firstSubscription;
    }

    public String getCompleteInfo() {
        return completeInfo;
    }

    public void setCompleteInfo(String completeInfo) {
        this.completeInfo = completeInfo;
    }

    public RegisterdFrenchiseAdmin getAdmin() {
        return admin;
    }

    public void setAdmin(RegisterdFrenchiseAdmin admin) {
        this.admin = admin;
    }

}
