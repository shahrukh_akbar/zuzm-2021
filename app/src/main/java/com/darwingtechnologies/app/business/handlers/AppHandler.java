package com.darwingtechnologies.app.business.handlers;

import android.util.Log;

import com.darwingtechnologies.app.business.models.AgentLoginResponse;
import com.darwingtechnologies.app.business.models.Comment.addComment.AddCommentData;
import com.darwingtechnologies.app.business.models.Comment.addComment.AddCommentResponse;
import com.darwingtechnologies.app.business.models.CreateFranchiesResponse;
import com.darwingtechnologies.app.business.models.Comment.CommentData;
import com.darwingtechnologies.app.business.models.Comment.CommentResponse;
import com.darwingtechnologies.app.business.models.ImageFranchiesResponse;
import com.darwingtechnologies.app.business.models.ParamFile;
import com.darwingtechnologies.app.business.models.forgotPassword.ForgotPasswordResponse;
import com.darwingtechnologies.app.business.models.getBrokerByCategory.GetBrokerByCategoryResponse;
import com.darwingtechnologies.app.business.models.getBrokerType.BrokerTypeResponse;
import com.darwingtechnologies.app.business.models.register.RegisterInput;
import com.darwingtechnologies.app.business.models.register.RegisterResponse;
import com.darwingtechnologies.app.business.models.register.newR.RegisterAsBrokerResponse;
import com.darwingtechnologies.app.business.models.searchBroker.BrokerResponse;
import com.darwingtechnologies.app.business.models.searchBroker.SearchBrokerInput;
import com.darwingtechnologies.app.business.models.setBrokerProfile.SetBrokerInput;
import com.darwingtechnologies.app.business.models.setBrokerProfile.SetBrokerProfileResponse;
import com.darwingtechnologies.app.business.models.updateBrokerProfile.UpdateBrokerProfileInput;
import com.darwingtechnologies.app.business.models.updateBrokerProfile.UpdateBrokerProfileResponse;
import com.darwingtechnologies.app.business.models.updateBrokerProfileImage.UpdateBrokerProfileImageResponse;
import com.darwingtechnologies.app.business.models.updateUserImage.UpdateUserImageInput;
import com.darwingtechnologies.app.business.models.updateUserImage.UpdateUserImageResponse;
import com.darwingtechnologies.app.business.models.updateUserProfile.UpdateUserProfileInput;
import com.darwingtechnologies.app.business.models.updateUserProfile.UpdateUserProfileResponse;
import com.darwingtechnologies.app.business.models.userLogin.UserLoginInput;
import com.darwingtechnologies.app.business.models.userLogin.UserLoginResponse;
import com.darwingtechnologies.app.utilities.CommonMethods;
import com.darwingtechnologies.app.utilities.CommonObjects;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Rana Zeshan on 27-Jun-19.
 */


public class AppHandler {
    private static String BASE_URL = "http://zozm.sa/api.php";



    public interface GetJobsFromWebScrapperListener {
        public void onSuccess(String htmlData);
        public void onError(String error);
    }



    public static void getJobsFromWebsiteScrapper(String urlofWebsiteForScrapping,final GetJobsFromWebScrapperListener getJobsFromWebScrapperListener ){
//        CommonMethods.showProgressDialog(CommonObjects.getContext());

        new CallForServer(urlofWebsiteForScrapping, new CallForServer.OnServerResultNotifier() {
            @Override
            public void onServerResultNotifier(boolean isError, String response) {
//                CommonMethods.hideProgressDialog();

                if(!isError){
                    getJobsFromWebScrapperListener.onSuccess(response);
                }else{
                    getJobsFromWebScrapperListener.onError("Something went wrong");
                }
            }
        }).callForServerGetScrapper();
    }


    public interface AgentLoginListener {
        public void onSuccess(AgentLoginResponse agentLoginResponse);
        public void onError(String error);
    }
    public interface CreateFranchiesAgentListener {
        public void onSuccess(CreateFranchiesResponse createFranchiesResponse);
        public void onError(String error);
    }

    public interface ImageFranchiesListener {
        public void onSuccess(ImageFranchiesResponse imageFranchiesResponse);

        public void onError(String error);
    }
    public interface UserLoginListener {
        public void onSuccess(UserLoginResponse userLoginResponse);
        public void onError(String error);
    }

    public interface ForgotPasswordListener {
        public void onSuccess(ForgotPasswordResponse forgotPasswordResponse);
        public void onError(String error);
    }

    public interface RegisterUserListener {
        public void onSuccess(RegisterResponse userLoginResponse);
        public void onError(String error);
    }
    public interface UpdateUserImageListener {
        public void onSuccess(UpdateUserImageResponse updateUserImageResponse);
        public void onError(String error);
    }
    public interface UpdateBrokerImageListener {
        public void onSuccess(UpdateBrokerProfileImageResponse updateBrokerProfileImageResponse);
        public void onError(String error);
    }
    public interface UpdateBrokerProfileListener {
        public void onSuccess(UpdateBrokerProfileResponse updateBrokerProfileResponse);
        public void onError(String error);
    }


    public interface UpdateUserProfileListener {
        public void onSuccess(UserLoginResponse updateUserProfileResponse);
        public void onError(String error);
    }
    public interface GetBrokerByCategoryListener {
        public void onSuccess(ArrayList<GetBrokerByCategoryResponse> getBrokerByCategoryResponse);
        public void onError(String error);
    }
    public interface GetAllBrokerListener {
        public void onSuccess(ArrayList<BrokerResponse> getAllBrokerResponse);
        public void onError(String error);
    }
    public interface SearchBrokerListener {
        public void onSuccess(ArrayList<BrokerResponse> searchBrokerResponse);
        public void onError(String error);
    }
    public interface SetBrokerProfileListener {
        public void onSuccess(SetBrokerProfileResponse setBrokerProfileResponse);
        public void onError(String error);
    }

    public interface RegisterAsBrokerListener {
        public void onSuccess(RegisterAsBrokerResponse registerAsBrokerResponse);
        public void onError(String error);
    }
    public interface CommentListener {
        public void onSuccess(List<CommentData> commentData);
        public void onError(String error);
    }
    public interface AddCommentListener {
        public void onSuccess(AddCommentData addcommentData);
        public void onError(String error);
    }

    public interface GetBrokerTypeListener {
        public void onSuccess(ArrayList<BrokerTypeResponse> getBrokerTypeResponse);
        public void onError(String error);
    }

    public static void userLogin(UserLoginInput userLoginInput, final UserLoginListener userLoginListener) {
        CommonMethods.showProgressDialog(CommonObjects.getContext());
        android.util.Log.e("loginInput",new Gson().toJson(userLoginInput));

        Map<String, String> paramStrings = new Gson().fromJson(new Gson().toJson(userLoginInput), new TypeToken<Map<String, String>>() {}.getType());

        new CallForServer(BASE_URL + "?action=login", new CallForServer.OnServerResultNotifier() {
            @Override
            public void onServerResultNotifier(boolean isError, String response) {
                Log.e("loginResponse", "" + response);

//                        CommonMethods.hideProgressDialog();
                if (!isError) {
                    try {
                        UserLoginResponse userLoginResponse = new Gson().fromJson(response, UserLoginResponse.class);
                        if ( userLoginResponse!=null && userLoginResponse.getData()!=null) {
                            userLoginListener.onSuccess(userLoginResponse);
                        } else {
                            if ( response.contains("incorrect_password")) {
                                userLoginListener.onError("Please enter valid password and try again.");
                            }else{
                                userLoginListener.onError("Something went wrong, please try again...!!!");
                            }

                        }
                    } catch (Exception e){
                        e.printStackTrace();
                        userLoginListener.onError(e.getMessage());
                    }
                } else {
                    userLoginListener.onError(response);
                }
            }
        }, paramStrings).callForServerPost();
    }

    public static void getResetPasswordLink(String email, final ForgotPasswordListener forgotPasswordListener) {
        CommonMethods.showProgressDialog(CommonObjects.getContext());

        new CallForServer(BASE_URL + "?action=forgetpassLink&email="+email, new CallForServer.OnServerResultNotifier() {
            @Override
            public void onServerResultNotifier(boolean isError, String response) {
                Log.e("resetPasswordResponse", "" + response);

                        CommonMethods.hideProgressDialog();
                if (!isError) {
                    try {
                        ForgotPasswordResponse forgotPasswordResponse = new Gson().fromJson(response, ForgotPasswordResponse.class);
                        if ( forgotPasswordResponse!=null && forgotPasswordResponse.getCode().equalsIgnoreCase("200")) {
                            forgotPasswordListener.onSuccess(forgotPasswordResponse);
                        } else {
                            if(forgotPasswordResponse!=null && forgotPasswordResponse.getMsg()!=null && !forgotPasswordResponse.getMsg().isEmpty()){
                                forgotPasswordListener.onError(forgotPasswordResponse.getMsg());
                            }else{
                                forgotPasswordListener.onError("Something went wrong, please try again...!!!");
                            }
                        }
                    } catch (Exception e){
                        e.printStackTrace();
                        forgotPasswordListener.onError(e.getMessage());
                    }
                } else {
                    forgotPasswordListener.onError(response);
                }
            }
        }).callForServerGet();
    }

    public static void registerUser(RegisterInput registerInput, final RegisterUserListener registerUserListener) {
        CommonMethods.showProgressDialog(CommonObjects.getContext());
        android.util.Log.e("registerInput",new Gson().toJson(registerInput));

        Map<String, String> paramStrings = new Gson().fromJson(new Gson().toJson(registerInput), new TypeToken<Map<String, String>>() {}.getType());

        new CallForServer(BASE_URL + "?action=reg", new CallForServer.OnServerResultNotifier() {
            @Override
            public void onServerResultNotifier(boolean isError, String response) {
                Log.e("registerResponse", "" + response);

//                        CommonMethods.hideProgressDialog();
                if (!isError) {
                    try {
                        RegisterResponse registerResponse =
                                new Gson()
                                        .fromJson(
                                                response,
                                                RegisterResponse.class);
                        if ( registerResponse!=null) {
                            if(registerResponse.getError()!=null && !registerResponse.getError().isEmpty()){
                                registerUserListener.onError(registerResponse.getError());
                            }else{
                                registerUserListener.onSuccess(registerResponse);

                            }
                        } else {
                                registerUserListener.onError("Some thing went wrong" + "");
                        }
                    }  catch (Exception e) {
                        e.printStackTrace();
                        registerUserListener.onError(e.getMessage());
                    }
                } else {
                    registerUserListener.onError(response);
                }
            }
        }, paramStrings).callForServerPost();
    }
    public static void updateBrokerProfileCoverImage(ArrayList<File> files, UpdateUserImageInput updateProfileImageInput, final UpdateBrokerImageListener updateUserImageListener) {
        CommonMethods.showProgressDialog(CommonObjects.getContext());
        Map<String, String> paramStrings = new Gson().fromJson(new Gson().toJson(updateProfileImageInput), new TypeToken<Map<String, String>>() {}.getType());
        ArrayList<ParamFile> paramFiles = new ArrayList<>();
        for (int i = 0; i < files.size(); i++) {
            ParamFile paramFile = new ParamFile("job_cover", files.get(i));
            paramFiles.add(paramFile);
        }

        new CallForServer(
                BASE_URL +"?action=updatebrokercoverImg",
                new CallForServer.OnServerResultNotifier() {
                    @Override
                    public void onServerResultNotifier(boolean isError, String response) {
                        Log.e("updatebrokerImgResponse", "" + response);
                        CommonMethods.hideProgressDialog();
                        if (!isError) {
                            try {
                                UpdateBrokerProfileImageResponse updateUserImageResponse = new Gson().fromJson(response, UpdateBrokerProfileImageResponse.class);
                                if ( updateUserImageResponse!=null && updateUserImageResponse.getSuccess()) {
                                    updateUserImageListener.onSuccess(updateUserImageResponse);
                                } else {
                                    if ( updateUserImageResponse!=null && updateUserImageResponse.getErrors()!=null && updateUserImageResponse.getErrors().size()>0 ){
                                        updateUserImageListener.onError(""+updateUserImageResponse.getErrors().toString());
                                    }else{
                                        updateUserImageListener.onError("Something went wrong, please try again.");
                                    }
                                }
                            }  catch (Exception e) {
                                updateUserImageListener.onError(e.getMessage());
                            }
                        } else {
                            updateUserImageListener.onError(response);
                        }
                    }
                }, paramStrings, paramFiles)
                .callForServerFilePost();
    }

    public static void updateBrokerProfile(UpdateBrokerProfileInput updateProfileImageInput, final UpdateBrokerProfileListener updateUserImageListener) {
        CommonMethods.showProgressDialog(CommonObjects.getContext());
        Map<String, String> paramStrings = new Gson().fromJson(new Gson().toJson(updateProfileImageInput), new TypeToken<Map<String, String>>() {}.getType());


        new CallForServer(
                BASE_URL +"?action=updateProfile",
                new CallForServer.OnServerResultNotifier() {
                    @Override
                    public void onServerResultNotifier(boolean isError, String response) {
                        Log.e("updatebrokerImgResponse", "" + response);
                        CommonMethods.hideProgressDialog();
                        if (!isError) {
                            try {
                                UpdateBrokerProfileResponse updateUserImageResponse = new Gson().fromJson(response, UpdateBrokerProfileResponse.class);
                                if ( updateUserImageResponse!=null && updateUserImageResponse.getSuccess()) {
                                    updateUserImageListener.onSuccess(updateUserImageResponse);
                                } else {
                                    if ( updateUserImageResponse!=null && updateUserImageResponse.getErrors()!=null && updateUserImageResponse.getErrors().size()>0 ){
                                        updateUserImageListener.onError(""+updateUserImageResponse.getErrors().toString());
                                    }else{
                                        updateUserImageListener.onError("Something went wrong, please try again.");
                                    }
                                }
                            }  catch (Exception e) {
                                updateUserImageListener.onError(e.getMessage());
                            }
                        } else {
                            updateUserImageListener.onError(response);
                        }
                    }
                }, paramStrings)
                .callForServerFilePost();
    }

    public static void updateBrokerProfileImage(ArrayList<File> files, UpdateUserImageInput updateProfileImageInput, final UpdateBrokerImageListener updateUserImageListener) {
        CommonMethods.showProgressDialog(CommonObjects.getContext());
        Map<String, String> paramStrings = new Gson().fromJson(new Gson().toJson(updateProfileImageInput), new TypeToken<Map<String, String>>() {}.getType());
        ArrayList<ParamFile> paramFiles = new ArrayList<>();
        for (int i = 0; i < files.size(); i++) {
            ParamFile paramFile = new ParamFile("job_logo", files.get(i));
            paramFiles.add(paramFile);
        }

        new CallForServer(
                BASE_URL +"?action=updatebrokerprofileImg",
                new CallForServer.OnServerResultNotifier() {
                    @Override
                    public void onServerResultNotifier(boolean isError, String response) {
                        Log.e("updatebrokerImgResponse", "" + response);
                        CommonMethods.hideProgressDialog();
                        if (!isError) {
                            try {
                                UpdateBrokerProfileImageResponse updateUserImageResponse = new Gson().fromJson(response, UpdateBrokerProfileImageResponse.class);
                                if ( updateUserImageResponse!=null && updateUserImageResponse.getSuccess()) {
                                    updateUserImageListener.onSuccess(updateUserImageResponse);
                                } else {
                                    if ( updateUserImageResponse!=null && updateUserImageResponse.getErrors()!=null && updateUserImageResponse.getErrors().size()>0 ){
                                        updateUserImageListener.onError(""+updateUserImageResponse.getErrors().toString());
                                    }else{
                                        updateUserImageListener.onError("Something went wrong, please try again.");
                                    }
                                }
                            }  catch (Exception e) {
                                updateUserImageListener.onError(e.getMessage());
                            }
                        } else {
                            updateUserImageListener.onError(response);
                        }
                    }
                }, paramStrings, paramFiles)
                .callForServerFilePost();
    }

    public static void updateProfileImage(ArrayList<File> files, String id, final UpdateUserImageListener updateUserImageListener) {
        CommonMethods.showProgressDialog(CommonObjects.getContext());
        Map<String, String> paramStrings =new HashMap<>();
        ArrayList<ParamFile> paramFiles = new ArrayList<>();
        for (int i = 0; i < files.size(); i++) {
            ParamFile paramFile = new ParamFile("avatar", files.get(i));
            paramFiles.add(paramFile);
        }

        new CallForServer(
                BASE_URL +"?action=updateImg&ID="+id,
                new CallForServer.OnServerResultNotifier() {
                    @Override
                    public void onServerResultNotifier(boolean isError, String response) {
                        Log.e("uploadBalanceResponse", "" + response);
                        CommonMethods.hideProgressDialog();
                        if (!isError) {
                            try {
                                UpdateUserImageResponse updateUserImageResponse = new Gson().fromJson(response, UpdateUserImageResponse.class);
                                if ( updateUserImageResponse!=null && updateUserImageResponse.getAvatar()!=null && !updateUserImageResponse.getAvatar().isEmpty()) {
                                    updateUserImageListener.onSuccess(updateUserImageResponse);
                                } else {
                                    if ( updateUserImageResponse!=null && updateUserImageResponse.getMsg()!=null && !updateUserImageResponse.getMsg().isEmpty() ){
                                        updateUserImageListener.onError(""+updateUserImageResponse.getMsg());
                                    }else{
                                        updateUserImageListener.onError("Something went wrong, please try again.");
                                    }
                                }
                            }  catch (Exception e) {
                                updateUserImageListener.onError(e.getMessage());
                            }
                        } else {
                            updateUserImageListener.onError(response);
                        }
                    }
                }, paramStrings, paramFiles)
                .callForServerFilePost();
    }

    public static void updateUserProfile(UpdateUserProfileInput updateUserProfileInput, final UpdateUserProfileListener updateUserProfileListener) {
        CommonMethods.showProgressDialog(CommonObjects.getContext());
        Map<String, String> paramStrings = new Gson().fromJson(new Gson().toJson(updateUserProfileInput), new TypeToken<Map<String, String>>() {}.getType());

        new CallForServer(BASE_URL +"?action=updateUser&ID="+updateUserProfileInput.getID(), new CallForServer.OnServerResultNotifier() {
            @Override
            public void onServerResultNotifier(boolean isError, String response) {
                Log.e("updateProfile", "" + response);
                CommonMethods.hideProgressDialog();
                if (!isError) {
                    try {
                        UserLoginResponse updateUserProfileResponse = new Gson().fromJson(response, UserLoginResponse.class);
                        if(updateUserProfileResponse !=null) {
                                updateUserProfileListener.onSuccess(updateUserProfileResponse);
                        }else{
                            updateUserProfileListener.onError("Something went wrong, please try again.");
                        }
                    }  catch (Exception e) {
                        updateUserProfileListener.onError(e.getMessage());
                    }
                } else {
                    updateUserProfileListener.onError(response);
                }
            }
        },paramStrings).callForServerPost();
    }

    public static void getBrokerByCategory( final GetBrokerByCategoryListener getBrokerByCategoryListener) {
        CommonMethods.showProgressDialog(CommonObjects.getContext());
//        Map<String, String> paramStrings = new Gson().fromJson(new Gson().toJson(updateUserProfileInput), new TypeToken<Map<String, String>>() {}.getType());

        new CallForServer(BASE_URL +"?action=getbrokercat", new CallForServer.OnServerResultNotifier() {
            @Override
            public void onServerResultNotifier(boolean isError, String response) {
                Log.e("cancelAppointment", "" + response);
                CommonMethods.hideProgressDialog();
                if (!isError) {
                    try {
                        Type listType = new TypeToken<ArrayList<GetBrokerByCategoryResponse>>(){}.getType();
                        ArrayList<GetBrokerByCategoryResponse> getBrokerByCategoryResponse = new Gson().fromJson(response,listType);

                        if(getBrokerByCategoryResponse !=null) {
                            if (getBrokerByCategoryResponse!=null) {
                                getBrokerByCategoryListener.onSuccess(getBrokerByCategoryResponse);
                            } else {
                                getBrokerByCategoryListener.onError("Something went wrong, please try again.");
                            }
                        }else{
                            getBrokerByCategoryListener.onError("Something went wrong, please try again.");
                        }
                    }  catch (Exception e) {
                        getBrokerByCategoryListener.onError(e.getMessage());
                    }
                } else {
                    getBrokerByCategoryListener.onError(response);
                }
            }
        }).callForServerGet();
    }

    public static void getAllBroker(final GetAllBrokerListener getAllBrokerListener) {
        CommonMethods.showProgressDialog(CommonObjects.getContext());
//        Map<String, String> paramStrings = new Gson().fromJson(new Gson().toJson(updateUserProfileInput), new TypeToken<Map<String, String>>() {}.getType());

        new CallForServer(BASE_URL +"?action=getallbrokers", new CallForServer.OnServerResultNotifier() {
            @Override
            public void onServerResultNotifier(boolean isError, String response) {
                Log.e("getallbrokers", "" + response);
                CommonMethods.hideProgressDialog();
                if (!isError) {
                    try {
                        Type listType = new TypeToken<ArrayList<BrokerResponse>>(){}.getType();
                        ArrayList<BrokerResponse> brokerResponses = new Gson().fromJson(response,listType);

                        if(brokerResponses !=null) {
                            if (brokerResponses.size()>0) {
                                getAllBrokerListener.onSuccess(brokerResponses);
                            } else {
                                getAllBrokerListener.onError("No Broker Found");
                            }
                        }else{
                            getAllBrokerListener.onError("Something went wrong, please try again.");
                        }
                    }  catch (Exception e) {
                        getAllBrokerListener.onError(e.getMessage());
                    }
                } else {
                    getAllBrokerListener.onError(response);
                }
            }
        }).callForServerGet();
    }

    public static void searchBroker(SearchBrokerInput searchBrokerInput, final SearchBrokerListener searchBrokerListener) {
        CommonMethods.showProgressDialog(CommonObjects.getContext());
        Map<String, String> paramStrings = new Gson().fromJson(new Gson().toJson(searchBrokerInput), new TypeToken<Map<String, String>>() {}.getType());

        android.util.Log.e("searchBrokerInput",new Gson().toJson(searchBrokerInput));

        new CallForServer(BASE_URL +"?action=searchbroker", new CallForServer.OnServerResultNotifier() {
            @Override
            public void onServerResultNotifier(boolean isError, String response) {
                Log.e("searchBroker", "" + response);
                CommonMethods.hideProgressDialog();
                if (!isError) {
                    try {
                        Type listType = new TypeToken<ArrayList<BrokerResponse>>(){}.getType();
                        ArrayList<BrokerResponse> searchBrokerResponse = new Gson().fromJson(response,listType);

//                        BrokerResponse searchBrokerResponse = new Gson().fromJson(response, BrokerResponse.class);
                        if(searchBrokerResponse !=null) {
                            if (searchBrokerResponse.size()>0) {
                                searchBrokerListener.onSuccess(searchBrokerResponse);
                            } else {
                                searchBrokerListener.onError("No Search Found...!!!");
                            }
                        }else{
                            searchBrokerListener.onError("No Search Found...!!!");
                        }
                    }  catch (Exception e) {
                        searchBrokerListener.onError(e.getMessage());
                    }
                } else {
                    searchBrokerListener.onError(response);
                }
            }
        },paramStrings).callForServerPost();
    }

    public static void setBrokerProfile(SetBrokerInput setBrokerInput,final SetBrokerProfileListener setBrokerProfileListener) {
        CommonMethods.showProgressDialog(CommonObjects.getContext());
        Map<String, String> paramStrings = new Gson().fromJson(new Gson().toJson(setBrokerInput), new TypeToken<Map<String, String>>() {}.getType());

        new CallForServer(BASE_URL +"?action=setbrokerprofile", new CallForServer.OnServerResultNotifier() {
            @Override
            public void onServerResultNotifier(boolean isError, String response) {
                Log.e("setBrokerProfile", "" + response);
                CommonMethods.hideProgressDialog();
                if (!isError) {
                    try {
//                        Type listType = new TypeToken<ArrayList<SetBrokerProfileResponse>>(){}.getType();
//                        ArrayList<SetBrokerProfileResponse> setBrokerProfileResponse = new Gson().fromJson(response,listType);

                        SetBrokerProfileResponse setBrokerProfileResponse = new Gson().fromJson(response, SetBrokerProfileResponse.class);
                        if(setBrokerProfileResponse !=null) {
                                setBrokerProfileListener.onSuccess(setBrokerProfileResponse);
                        }else{
                            setBrokerProfileListener.onError("Something went wrong, please try again.");
                        }
                    }  catch (Exception e) {
                        setBrokerProfileListener.onError(e.getMessage());
                    }
                } else {
                    setBrokerProfileListener.onError(response);
                }
            }
        },paramStrings).callForServerPost();
    }

    public static void registerAsBroker(SetBrokerInput setBrokerInput,final RegisterAsBrokerListener registerAsBrokerListener) {
        CommonMethods.showProgressDialog(CommonObjects.getContext());
        Map<String, String> paramStrings = new Gson().fromJson(new Gson().toJson(setBrokerInput), new TypeToken<Map<String, String>>() {}.getType());

        new CallForServer(BASE_URL +"?action=setbrokerprofile", new CallForServer.OnServerResultNotifier() {
            @Override
            public void onServerResultNotifier(boolean isError, String response) {
                Log.e("setBrokerProfile", "" + response);
                CommonMethods.hideProgressDialog();
                if (!isError) {
                    try {
//                        Type listType = new TypeToken<ArrayList<SetBrokerProfileResponse>>(){}.getType();
//                        ArrayList<SetBrokerProfileResponse> setBrokerProfileResponse = new Gson().fromJson(response,listType);

                        RegisterAsBrokerResponse registerAsBrokerResponse = new Gson().fromJson(response, RegisterAsBrokerResponse.class);
                        if(registerAsBrokerResponse !=null && registerAsBrokerResponse.getSuccess()) {
                            registerAsBrokerListener.onSuccess(registerAsBrokerResponse);
                        }else{
                            if(registerAsBrokerResponse.getErrors()!=null && registerAsBrokerResponse.getErrors().size()>0){
                                registerAsBrokerListener.onError(registerAsBrokerResponse.getErrors().toString());
                            }else{
                                registerAsBrokerListener.onError("Something went wrong, please try again.");
                            }
                        }
                    }  catch (Exception e) {
                        registerAsBrokerListener.onError(e.getMessage());
                    }
                } else {
                    registerAsBrokerListener.onError(response);
                }
            }
        },paramStrings).callForServerPost();
    }

    public static void addComment(int profileID,int userIdWhoComment,String commentText,AddCommentListener addCommentListener){
        CommonMethods.showProgressDialog(CommonObjects.getContext());
        Map<String, String> paramStrings = new HashMap<>();
        paramStrings.put("profile_id",""+profileID);
        paramStrings.put("user_id",""+userIdWhoComment);
        paramStrings.put("comment",""+commentText);

        new CallForServer(BASE_URL + "?action=addcomments", new CallForServer.OnServerResultNotifier() {
            @Override
            public void onServerResultNotifier(boolean isError, String response) {
                CommonMethods.hideProgressDialog();
                if (!isError) {
                    try {
                        AddCommentResponse commentResponse=new Gson().fromJson(response,AddCommentResponse.class);

                        if(commentResponse !=null && commentResponse.getSuccess()) {
                            if (commentResponse.getData()!=null) {
                                addCommentListener.onSuccess(commentResponse.getData());
                            } else {
                                addCommentListener.onError("Something went wrong, Please try again.");
                            }
                        }else{
                            if(commentResponse!=null && commentResponse.getErrors()!=null && commentResponse.getErrors().size()>0){
                                addCommentListener.onError(commentResponse.getErrors().toString());
                            }else{
                                addCommentListener.onError("Something went wrong, Please try again.");
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        addCommentListener.onError(e.getMessage());
                    }
                }else {
                    addCommentListener.onError(response);
                }
            }
        },paramStrings).callForServerPost();

    }

    public static void getComments(int postID, final CommentListener getCommentListener) {
//        CommonMethods.showProgressDialog(CommonObjects.getContext());
        Map<String, String> paramStrings = new HashMap<>();//new Gson().fromJson(new Gson().toJson(searchBrokerInput), new TypeToken<Map<String, String>>() {}.getType());
        paramStrings.put("post_id",""+postID);

        new CallForServer(BASE_URL +"?action=getcomments", new CallForServer.OnServerResultNotifier() {
            @Override
            public void onServerResultNotifier(boolean isError, String response) {
                Log.e("cancelAppointment", "" + response);
//                CommonMethods.hideProgressDialog();
                if (!isError) {
                    try {
//                        Type listType = new TypeToken<ArrayList<CommentResponse>>(){}.getType();
//                        ArrayList<CommentResponse> commentResponses = new Gson().fromJson(response,listType);

                        CommentResponse commentResponse=new Gson().fromJson(response,CommentResponse.class);

                        if(commentResponse !=null && commentResponse.getSuccess()) {
                            if (commentResponse.getData()!=null && commentResponse.getData().size()>0) {
                                getCommentListener.onSuccess(commentResponse.getData());
                            } else {
                                getCommentListener.onError("No Comments.");
                            }
                        }else{
                            if(commentResponse!=null && commentResponse.getErrors()!=null && commentResponse.getErrors().size()>0){
                                getCommentListener.onError(commentResponse.getErrors().toString());
                            }else{
                                getCommentListener.onError("No Comments.");
                            }
                        }
                    }  catch (Exception e) {
                        getCommentListener.onError(e.getMessage());
                    }
                } else {
                    getCommentListener.onError(response);
                }
            }
        },paramStrings).callForServerPost();
    }

    public static void getBrokerType(boolean isShowProgress, final GetBrokerTypeListener getBrokerTypeListener) {
       if(isShowProgress) {
           CommonMethods.showProgressDialog(CommonObjects.getContext());
       }
//        Map<String, String> paramStrings = new Gson().fromJson(new Gson().toJson(searchBrokerInput), new TypeToken<Map<String, String>>() {}.getType());

        new CallForServer(BASE_URL +"?action=getbrokertype", new CallForServer.OnServerResultNotifier() {
            @Override
            public void onServerResultNotifier(boolean isError, String response) {
                Log.e("getbrokertype", "" + response);
                CommonMethods.hideProgressDialog();
                if (!isError) {
                    try {
                        Type listType = new TypeToken<ArrayList<BrokerTypeResponse>>(){}.getType();
                        ArrayList<BrokerTypeResponse> getBrokerTypeResponse = new Gson().fromJson(response,listType);

                        if(getBrokerTypeResponse !=null) {
                            if (getBrokerTypeResponse.size()>0) {
                                getBrokerTypeListener.onSuccess(getBrokerTypeResponse);
                            } else {
                                getBrokerTypeListener.onError("No Types Available.");
                            }
                        }else{
                            getBrokerTypeListener.onError("No Types Available.");
                        }
                    }  catch (Exception e) {
                        getBrokerTypeListener.onError(e.getMessage());
                    }
                } else {
                    getBrokerTypeListener.onError(response);
                }
            }
        }).callForServerGet();
    }

}