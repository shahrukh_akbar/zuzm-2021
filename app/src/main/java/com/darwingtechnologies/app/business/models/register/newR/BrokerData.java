
package com.darwingtechnologies.app.business.models.register.newR;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BrokerData {

    @SerializedName("profile_id")
    @Expose
    private Integer profileId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("post_title")
    @Expose
    private String postTitle;
    @SerializedName("post_content")
    @Expose
    private String postContent;
    @SerializedName("job_tagline")
    @Expose
    private String jobTagline;
    @SerializedName("job_cover")
    @Expose
    private String jobCover;
    @SerializedName("job_logo")
    @Expose
    private String jobLogo;
    @SerializedName("job_email")
    @Expose
    private String jobEmail;
    @SerializedName("job_phone")
    @Expose
    private String jobPhone;
    @SerializedName("whatsapp-number")
    @Expose
    private String whatsappNumber;
    @SerializedName("case27_listing_type")
    @Expose
    private String case27ListingType;
    @SerializedName("job_location")
    @Expose
    private String jobLocation;
    @SerializedName("geolocation_lat")
    @Expose
    private String geolocationLat;
    @SerializedName("geolocation_long")
    @Expose
    private String geolocationLong;

    public Integer getProfileId() {
        return profileId;
    }

    public void setProfileId(Integer profileId) {
        this.profileId = profileId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public String getJobTagline() {
        return jobTagline;
    }

    public void setJobTagline(String jobTagline) {
        this.jobTagline = jobTagline;
    }

    public String getJobCover() {
        return jobCover;
    }

    public void setJobCover(String jobCover) {
        this.jobCover = jobCover;
    }

    public String getJobLogo() {
        return jobLogo;
    }

    public void setJobLogo(String jobLogo) {
        this.jobLogo = jobLogo;
    }

    public String getJobEmail() {
        return jobEmail;
    }

    public void setJobEmail(String jobEmail) {
        this.jobEmail = jobEmail;
    }

    public String getJobPhone() {
        return jobPhone;
    }

    public void setJobPhone(String jobPhone) {
        this.jobPhone = jobPhone;
    }

    public String getWhatsappNumber() {
        return whatsappNumber;
    }

    public void setWhatsappNumber(String whatsappNumber) {
        this.whatsappNumber = whatsappNumber;
    }

    public String getCase27ListingType() {
        return case27ListingType;
    }

    public void setCase27ListingType(String case27ListingType) {
        this.case27ListingType = case27ListingType;
    }

    public String getJobLocation() {
        return jobLocation;
    }

    public void setJobLocation(String jobLocation) {
        this.jobLocation = jobLocation;
    }

    public String getGeolocationLat() {
        return geolocationLat;
    }

    public void setGeolocationLat(String geolocationLat) {
        this.geolocationLat = geolocationLat;
    }

    public String getGeolocationLong() {
        return geolocationLong;
    }

    public void setGeolocationLong(String geolocationLong) {
        this.geolocationLong = geolocationLong;
    }

}
